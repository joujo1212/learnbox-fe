import { LearnboxPage } from './app.po';

describe('learnbox App', function() {
  let page: LearnboxPage;

  beforeEach(() => {
    page = new LearnboxPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
