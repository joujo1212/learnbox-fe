import {Injectable} from '@angular/core';
import {CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild} from '@angular/router';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {
  constructor(private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const expiredTime = +localStorage.getItem('expire_token_time');
    const accessToken = localStorage.getItem('access_token');
    const refreshToken = localStorage.getItem('refresh_token');
    const role = localStorage.getItem('role');
    const uuid = localStorage.getItem('uuid');
    if (expiredTime === 0 || !accessToken || !refreshToken || !role || !uuid) {
      localStorage.removeItem('access_token');
      localStorage.removeItem('refresh_token');
      localStorage.removeItem('expire_token_time');
      localStorage.removeItem('role');
      localStorage.removeItem('uuid');
      this.router.navigate(['/login']);
    }

    // check authorization
    const url: string = state.url;

    if (url === '/privacy' || url === '/cookies' || url === '/legal') {
      return true;
    }
    switch (role) {
      case 'STUDENT':
        if (url.startsWith('/student')) {
          return true;
        }
        break;
      case 'TUTOR':
        if (url.startsWith('/tutor')) {
          return true;
        }
        break;
      case 'SUPER_USER':
        if (url.startsWith('/super-user')) {
          return true;
        }
        break;
      case 'EXAMINER':
        if (url.startsWith('/examiner')) {
          return true;
        }
        break;
      default:
        localStorage.removeItem('access_token');
        localStorage.removeItem('refresh_token');
        localStorage.removeItem('expire_token_time');
        localStorage.removeItem('role');
        localStorage.removeItem('uuid');
    }

    this.router.navigateByUrl('/login');
    return false;
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.canActivate(childRoute, state);
  }
}
