import {ConnectionBackend, Http, RequestOptions, RequestOptionsArgs, Response, Headers} from '@angular/http';
import {AsyncSubject} from 'rxjs/AsyncSubject';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import {Endpoints} from '../endpoints';

export class AuthInterceptor extends Http {

  private requestingNewToken = false;
  private accessTokenSubject: AsyncSubject<string>;


  constructor(backend: ConnectionBackend, defaultOptions: RequestOptions, private router: Router) {
    super(backend, defaultOptions);
  }

  post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
    return Observable.create((observer: Observer<Response>) => {
      this.getRequestOptionArgs(options)
        .toPromise()
        .then((requestOptions: RequestOptionsArgs) => {
          super.post(url, body, requestOptions)
            .subscribe(result => {
                observer.next(result);
                observer.complete();
              },
              err => {
                observer.error(err);
              });
        });
    });
  }

  get(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return Observable.create((observer: Observer<Response>) => {
      this.getRequestOptionArgs(options)
        .toPromise()
        .then((requestOptions: RequestOptionsArgs) => {
          super.get(url, requestOptions)
            .subscribe(result => {
                observer.next(result);
                observer.complete();
              },
              err => {
                observer.error(err);
              });
        });
    });
  }

  put(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    return Observable.create((observer: Observer<Response>) => {
      this.getRequestOptionArgs(options)
        .toPromise()
        .then((requestOptions: RequestOptionsArgs) => {
          super.put(url, body, requestOptions)
            .subscribe(result => {
                observer.next(result);
                observer.complete();
              },
              err => {
                observer.error(err);
              });
        });
    });
  }

  delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return Observable.create((observer: Observer<Response>) => {
      this.getRequestOptionArgs(options)
        .toPromise()
        .then((requestOptions: RequestOptionsArgs) => {
          super.delete(url, requestOptions)
            .subscribe(result => {
                observer.next(result);
                observer.complete();
              },
              err => {
                observer.error(err);
              });
        });
    });
  }

  getRequestOptionArgs(options?: RequestOptionsArgs): Observable<RequestOptionsArgs> {
    if (options == null) {
      options = new RequestOptions();
    }
    if (options.headers == null) {
      options.headers = new Headers();
    }
    const isAuthorized = options.headers.get('Authorization');
    if (isAuthorized === null || isAuthorized) {
      return this.getAccessToken()
        .map((response: string) => {
          options.headers.set('Authorization', 'bearer ' + response);
          return options;
        });
    } else {
      return Observable.of(options);
    }
  }

  private getAccessToken(): Observable<string> {
    const expiredTime = +localStorage.getItem('expire_token_time');
    const accessToken = localStorage.getItem('access_token');
    const refreshToken = localStorage.getItem('refresh_token');
    if (expiredTime === 0 || !accessToken || !refreshToken) {
      this.router.navigateByUrl('/login');
      return Observable.throw('UNAUTHORIZED');
    }
    const actualTime = new Date().getTime();
    if (actualTime < expiredTime) {
      return Observable.of(accessToken);
    } else {
      if (!this.requestingNewToken) {
        this.requestingNewToken = true;
        this.accessTokenSubject = new AsyncSubject();
        super.post(Endpoints.refreshToken + refreshToken, null)
          .subscribe(
            (response: Response) => {
              const json = response.json();
              if (json) {
                localStorage.setItem('access_token', json.access_token);
                localStorage.setItem('refresh_token', json.refresh_token);
                const newExpiredTime = new Date().getTime() + (json.expires_in * 1000) - 3600;
                localStorage.setItem('expire_token_time', newExpiredTime.toString());
                this.accessTokenSubject.next(json.access_token);
                this.accessTokenSubject.complete();
                this.requestingNewToken = false;
              }
            },
            (error: Response) => {
              localStorage.removeItem('access_token');
              localStorage.removeItem('refresh_token');
              localStorage.removeItem('expire_token_time');
              localStorage.removeItem('role');
              this.router.navigateByUrl('/login');
              this.accessTokenSubject.error(error);
            }
          );
        return this.accessTokenSubject;
      } else {
        return this.accessTokenSubject;
      }
    }
  }
}
