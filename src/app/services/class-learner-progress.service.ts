import {Injectable} from '@angular/core';
import {ClassLearnerProgress} from '../model/class-learner-progress';
import {getClassLearnerProgress} from '../mocks/learner-admin.mocks';
import {Observable} from 'rxjs/Observable';
import {LearnerProgress} from '../model/learner-progress';
import {LearnerProgressPortfolio} from '../model/learner-progress-portfolio';
import {Http} from '@angular/http';
import {Endpoints} from '../endpoints';


@Injectable()
export class ClassLearnerProgressService {


  constructor(private http: Http) {}

  getClasses(): Promise<ClassLearnerProgress[]> {

    const data = getClassLearnerProgress();

    data.map(classLearner => {
      classLearner.students = [];
      classLearner.students.length = 0;
      classLearner.students = null;

    });

    return Observable.of(data).delay(1000).toPromise();

  }

  getClassStudents(uuidClassLearnerProgress: string): Promise<LearnerProgress[]> {

    const data = getClassLearnerProgress();

    let students: LearnerProgress[] = [];

    const classLearner = data.find(classLearnerItem => classLearnerItem.uuidClassLearnerProgress === uuidClassLearnerProgress);

    if (classLearner) {
      students = classLearner.students;
    }

    return Observable.of(students).delay(1000).toPromise();

  }

  getLearnerProfile(learnerUuid: string): Promise<LearnerProgressPortfolio> {
    return this.http.get(Endpoints.getLearnerProfile + '?uuid=' + learnerUuid)
      .toPromise()
      .then(response => response.json() as LearnerProgressPortfolio);
  }


}
