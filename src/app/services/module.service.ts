import {Injectable} from "@angular/core";
import {Http, Headers, Response} from "@angular/http";
import {OnBoardService} from "./on-board.service";
import {Endpoints} from "../endpoints";
import {MODULE_DETAILS} from "../mocks/module.mocks";
import {ModuleDetail} from "../model/module-detail";
import {useMock} from "../mocks/tutorial.mocks";
import {PageRequest} from "../model/page-request";

@Injectable()
export class ModuleService {

  private headers = new Headers({
    'Content-Type': 'application/json',
  });

  constructor(private http: Http) { }


  getModuleByUuid(enrollmentModuleUuid: string): Promise <ModuleDetail> {
    if (useMock) {
      ///start MOCK
      console.log('FAKE GET REQUEST FOR MODULE DETAIL DATA');
      const moduleDetail: ModuleDetail = MODULE_DETAILS.find(moduleDetail => moduleDetail.enrollmentUuid === enrollmentModuleUuid);
      return Promise.resolve(moduleDetail);
      ///end MOCK
    } else {
      return this.http.get(
        Endpoints.getModuleDetail(enrollmentModuleUuid),
        {headers: this.headers}
      ).toPromise()
        .then((response: Response) => {
          const module: ModuleDetail = response.json() as ModuleDetail;
          console.log(typeof module.tutorials[0].status);
          return module;
        });
    }
  }

  getModuleFromCourse(enrollmentUuid?: string): Promise<any> {
    const pageRequest: PageRequest = new PageRequest();
    pageRequest.page = 0;
    pageRequest.pageSize = 1000;
    if (enrollmentUuid) {
      pageRequest.enrollmentUuid = enrollmentUuid;
    }
    return this.http.post(
      Endpoints.getModulesFromCourse,
      JSON.stringify(pageRequest),
      {headers: this.headers}
    ).toPromise()
      .then(response => response.json());
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
