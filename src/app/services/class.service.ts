import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { OnBoardService } from './on-board.service';
import { Endpoints } from '../endpoints';

@Injectable()
export class ClassService {
  private headers: Headers;

  constructor(
    private http: Http,
  ) {
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
  }

  /**
   * Create class from list of students, body:
   * {
   *  name: string, - name of class
   *  studentUuids: [string], - array with student uuids
   *  tutorUuid: string - tutor uuid
   * }
   * @param body
   * @returns promise
     */
  createClass(body): Promise<any> {
    return this.http.post(Endpoints.createClass, body, {headers: this.headers})
      .toPromise()
      .then((res: Response) => res.json())
      .catch(error => console.log(error));
  };

}
