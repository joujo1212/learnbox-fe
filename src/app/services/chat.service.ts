/**
 * Created by pati on 05/05/2017.
 */
import {Injectable} from '@angular/core';
import {Response, Http, Headers} from '@angular/http';
import {Endpoints} from '../endpoints';
import {OnBoardService} from './on-board.service';
import {Observable} from 'rxjs';

@Injectable()
export class ChatService {

  private headers;

  constructor(private http: Http) {
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
  }

  getChatroomInfo(timestamp) {
    return this.http.get(Endpoints.chatroomInfo + '?timestamp=' + timestamp, {headers: this.headers})
      .map((res: any) => res.json())
      .catch((error: any) => Observable.throw(error));
  }
}
