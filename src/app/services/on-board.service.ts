import {Injectable} from '@angular/core';
import {Headers, Http, Response} from '@angular/http';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Rx';

import {Endpoints} from '../endpoints';
import {BasicInfo} from '../model/basic-info';

@Injectable()
export class OnBoardService {

  private headersNoAuthorization: Headers = new Headers({
    'Accept': 'application/json',
    'Authorization': false
  });
  private headers: Headers = new Headers({
    'Content-Type': 'application/json'
  });

  constructor(private http: Http,
              private router: Router) {
  }

  login(email, password): Observable<Response> {
    return this.http.post(Endpoints.login(email, password), {}, {headers: this.headersNoAuthorization})
      .map((res: Response) => {
        const json = res.json();
        if (json) {
          localStorage.setItem('access_token', json.access_token);
          localStorage.setItem('refresh_token', json.refresh_token);
          const expiredTime = new Date().getTime() + (json.expires_in * 1000) - 3600;
          localStorage.setItem('expire_token_time', expiredTime.toString());
        }
        return json;
      })
      .catch(e => {
        if (e.status === 401) {
          return Observable.throw('Unauthorized');
        }
        else {
          return Observable.throw('UserNotFound');
        }
      });
  }

  logout(): void {
    localStorage.removeItem('access_token');
    localStorage.removeItem('refresh_token');
    localStorage.removeItem('expire_token_time');
    localStorage.removeItem('role');
    localStorage.removeItem('uuid');
    this.router.navigateByUrl('/login');
  }

  finishRegistration(birthDate, lastName, gender, name): Observable<Response> {

    return this.http.post(Endpoints.finishRegistration, {
      birthDate: birthDate,
      familyName: lastName,
      genderType: gender,
      givenName: name
    }, {headers: this.headers})
      .map((response: Response) => response.json())
      .catch(e => {
        if (e.status === 401) {
          return Observable.throw('Unauthorized');
        } else {
          return Observable.throw('Error');
        }
      });
  }

  // todo zmazat
  inviteStudent(email): Observable<Response[]> {
    return this.http.post(Endpoints.inviteStudents, {'students': [email]}, {headers: this.headers})
      .map((response: Response) => response.json())
      .catch(e => Observable.throw('Error'));
  }

  // todo zmazat
  inviteStudentsByFile(file): Observable<any> {
    return this.http.post(Endpoints.invitedStudentsByFile, {'file': file}, {headers: this.headers})
      .map((response: Response) => response.json())
      .catch(e => Observable.throw('Error'));
  }

  changePassword(oldPswd, newPswd): Observable<Response> {
    return this.http.post(Endpoints.changePassword, {
      oldPassword: oldPswd,
      newPassword: newPswd
    }, {headers: this.headers})
      .map((response: Response) => {
        response.json();
        console.log('Json: ', response);
      })
      .catch(e => {
        if (e.status === 412) {
          console.log('Error: ', e);
          return Observable.throw('Error');
        }
      });
  }

  resetPassword(email): Observable<Response> {
    return this.http.post(Endpoints.resetPassword, {email, language: ''}, {headers: this.headersNoAuthorization})
      .catch(e => Observable.throw('Error'));
  }

  /**
   * Get basic info according to token
   * @returns Promise<BasicInfo>
   */
  getBasicInfo(): Promise<BasicInfo> {
    return this.http.get(Endpoints.getBasicInfo, {headers: this.headers})
      .toPromise()
      .then((res: Response) => res.json());
  };

  /**
   * Save user role after user login to display different html templates
   * in views when needed, or to change logic of components according to role
   * when needed
   * @param role: STUDENT, TUTOR, SUPER_USER, EXAMINER
   */
  setUserRole(role) {
    localStorage.setItem('role', role);
  }

  /**
   * Get the role of user
   * @returns {string} STUDENT, TUTOR, SUPER_USER, EXAMINER
   */
  getUserRole() {
    return localStorage.getItem('role');
  }

  /**
   * Save user uuid after user login
   * @param uuid
   */
  setUserUuid(uuid) {
    localStorage.setItem('uuid', uuid);
  }

  /**
   * Get user uuid
   * @returns {string}
   */
  getUserUuid() {
    return localStorage.getItem('uuid');
  }
}
