import {Injectable} from "@angular/core";
import {Http, Headers, Response, RequestOptions} from "@angular/http";
import {OnBoardService} from "./on-board.service";
import {Endpoints} from "../endpoints";
import {PortfolioCourse} from "../model/portfolioCourse";
import {PageRequest} from "../model/page-request";

import 'rxjs/add/operator/toPromise';

@Injectable()
export class TutorService {

  private headers = new Headers({
    'Content-Type': 'application/json'
  });

  constructor(private http: Http,
              private onBoardService: OnBoardService) {}

  /**
   * Creates users by uploading a file with list of users
   * @param file
   * @returns promise
     */
  inviteLearnersByFile(file): Promise<Response[]> {
    const formData: FormData = new FormData();
    formData.append('file', file);

    return this.http.post(Endpoints.inviteStudentsByFile, formData)
      .toPromise()
      .then((res: Response) => res.json())
      .catch(this.handleError);
  }

  /**
   * Parses data from file and returns list of students
   * @param file
   * @returns list of users
     */
  getUsersByFile(file): Promise<Response[]> {
    const formData: FormData = new FormData();
    formData.append('file', file);

    return this.http.post(Endpoints.getUsersByFile, formData)
      .toPromise()
      .then((res: Response) => res.json())
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
