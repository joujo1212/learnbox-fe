import {Injectable} from '@angular/core';
import {Modal, BSModalContext} from 'angular2-modal/plugins/bootstrap';
import {CustomModalContext, SuccessModal} from '../shared/modals/success-modal/success-modal.component';
import {Overlay, overlayConfigFactory} from 'angular2-modal';
import {ErrorModal} from '../shared/modals/error-modal/error-modal.component';
import {PromptModalComponent} from '../modules/super-user-module/course-setup/editor/modals/prompt.modal';

@Injectable()
export class ModalService {

  constructor(
    public modal: Modal,
    private overlay: Overlay
  ) {}

  /**
   * Open success or error modal
   * @param title of modal
   * @param body message of modal
   * @param type success | error
   * @param viewContainerRef where to display modal
   * @returns {Promise<DialogRef<any>>}
   */
  showSimpleModal(title, body, type, viewContainerRef) {
    this.overlay.defaultViewContainer = viewContainerRef;
    const context = new CustomModalContext();
    context.title = title;
    context.body = body;

    if (type === 'success') {
      return this.modal.open(SuccessModal, overlayConfigFactory(context, BSModalContext));
    } else if (type === 'error') {
      return this.modal.open(ErrorModal, overlayConfigFactory(context, BSModalContext));
    }
  }

  /**
   *
   * @param title
   * @param desc
   * @param input1Key key of input1 in response object
   * @param input1Placeholder placeholder text or empty string if input should not be visible
   * @param input2Key optional, key of input2 in response object
   * @param input2Placeholder optional, placeholder text or empty string if input should not be visible
   * @param canDismiss optional, if true, user can dismiss(close) modal, false for force hit 'OK' button. Default is true
   * @param input1Value optional, value for 1st input. Default is empty string
   * @returns {Promise<DialogRef<any>>}
   */
  showPromptModal(title, desc, input1Key, input1Placeholder, input2Key = '', input2Placeholder = '',
                  canDismiss = true, input1Value = '') {
    return this.modal
      .open(PromptModalComponent, overlayConfigFactory({ title, desc, input1Key, input1Placeholder, input2Key,
        input2Placeholder, canDismiss, input1Value}, BSModalContext))
      .then(promise => promise.result);
  }
}
