import {Injectable} from '@angular/core';
import {Headers, Http, Response} from '@angular/http';
import {OnBoardService} from './on-board.service';
import {Endpoints} from '../endpoints';
import {Submittable} from '../modules/student-module/util/submittable';
import {TutorialDetail} from '../model/tutorial-detail';
import {ModuleDetail} from '../model/module-detail';
import {getTutorialDetail, useMock} from '../mocks/tutorial.mocks';
import {TutorialInfo} from '../model/tutorial-info';
import {MODULE_DETAILS} from '../mocks/module.mocks';
import {Location} from '@angular/common';
import {Observable} from 'rxjs';


@Injectable()
export class TutorialService {

  private headers = new Headers({
    'Content-Type': 'application/json',
  });

  constructor(private http: Http,
              private _location: Location) {
  }

  /**
   * Get quiz by uuid
   * @param uuid
   * @returns {Promise<TResult>|Promise<T>|Promise<TResult2|TResult1>}
   */
  getQuiz(uuid: string): Promise<any> {
    return this.http.get(
      Endpoints.getQuiz + uuid,
      {headers: this.headers}
    ).toPromise()
      .then((response: Response) => response.json());
  }

  /**
   * Submit quiz
   * @param arrayOfQuestions
   * @returns {Observable<R|T>}
   */
  submitQuiz(arrayOfQuestions) {
    const splittedUrl = this._location.path().split('/');
    const enrolUuid = splittedUrl[splittedUrl.length - 1];

    const answerPojo = {
      enrollmentUuid: enrolUuid,
      questions: arrayOfQuestions
    };
    console.log(JSON.stringify(answerPojo));
    return this.http.post(Endpoints.submitQuizTutorial,
      answerPojo, {headers: this.headers})
      .map((res: any) => res.json())
      .catch((error: any) => Observable.throw(error));
  }

  /**
   * Get students' results from quiz
   * @param userUuid of student
   * @param enrollmentUuid of student
   * @returns Promise
   */
  viewResultsForTutorial(userUuid, enrollmentUuid): Promise<any> {
    return this.http.get(Endpoints.viewResults(userUuid, enrollmentUuid), {headers: this.headers})
      .toPromise()
      .then((res: Response) => res.json())
      .catch(this.handleError);
  }

  /**
   *
   * @param enrollmentTutorialUuid
   * @returns {any}
   */
  getTutorialByUuid(enrollmentTutorialUuid: string): Promise<TutorialDetail> {
    if (useMock) {
      // ///start MOCK
      console.log('FAKE GET REQUEST FOR TUTORIAL DETAIL DATA', enrollmentTutorialUuid);
      let tutorialInfo: TutorialInfo = null;
      for (const moduleDetail of MODULE_DETAILS) {
        tutorialInfo = moduleDetail.tutorials.find(tutorial => tutorial.enrollmentUuid === enrollmentTutorialUuid);
        if (tutorialInfo != null) {
          break;
        }

      }
      if (tutorialInfo == null) {
        return Promise.reject({error: 'Error not found'});
      }
      const tutorialDetail = getTutorialDetail(tutorialInfo);
      console.log(tutorialDetail);
      return Promise.resolve(tutorialDetail);
      // ///end MOCK
    } else {
      return this.http.get(
        Endpoints.getTutorialDetail(enrollmentTutorialUuid),
        {headers: this.headers}
      ).toPromise()
        .then((response: Response) => response.json() as TutorialDetail);
    }
  }

  getEnrollmentUuidOfActualTutorial(): Promise<string> {
    if (useMock) {
      // ///start MOCK
      const actualTutorial: TutorialInfo =
        MODULE_DETAILS[0].tutorials
          .find((tutorialInfo: TutorialInfo) => tutorialInfo.status === 'ACTIVE');
      if (actualTutorial) {
        return Promise.resolve(actualTutorial.enrollmentUuid);
      } else {
        return Promise.reject({error: 'Error not found'});
      }

      // ///end MOCK
    } else {
      return this.http.get(
        Endpoints.actualTutorial,
        {headers: this.headers}
      ).toPromise()
        .then((response: Response) => response.text());
    }
  }

  submitTutorial(submitableObject: Submittable): Promise<ModuleDetail> {
    if (useMock) {
      // //start MOCK
      console.log('FAKE SUBMIT TO ADDRESS: [' + submitableObject.getEndpoint() + ']');
      console.log('DATA TO FAKE SUBMIT: ');
      console.log(submitableObject.getSubmitData());
      //TODO: send submit for tutorial
      let moduleDetailByTutorialEnrollment: ModuleDetail;
      const tutorialEnrollmentUuid = submitableObject.getSubmitData().enrollmentUuid;

      for (const moduleDetail of MODULE_DETAILS) {
        const tutorialInfoIndex: number = moduleDetail.tutorials.findIndex(tutorial => tutorial.enrollmentUuid === tutorialEnrollmentUuid);
        if (tutorialInfoIndex !== -1) {
          const tutorialInfo = moduleDetail.tutorials[tutorialInfoIndex];
          if (Math.random() < .5) {
            tutorialInfo.status = 'PASS';
          } else {
            tutorialInfo.status = 'FAIL';
          }
          const nextTutorialIndex: number = tutorialInfoIndex + 1;
          if (moduleDetail.tutorials.length > nextTutorialIndex) {
            moduleDetail.tutorials[nextTutorialIndex].status = 'ACTIVE';
          }
          moduleDetailByTutorialEnrollment = moduleDetail;
          break;
        }
      }
      return Promise.resolve(moduleDetailByTutorialEnrollment);
      // //end MOCK
    } else {
      return this.http.post(
        submitableObject.getEndpoint(),
        submitableObject.getSubmitData(),
        {headers: this.headers}
      ).toPromise()
        .then((response: Response) => response.json() as ModuleDetail);
    }
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  public submitEvidenceTutorial(submitData: {enrollmentUuid: string; fileUuid: string}): Promise<Response> {
    return this.http.post(Endpoints.submitEvidence, submitData, {headers: this.headers})
      .toPromise();
  }

  public uploadEvidenceFile(file): Promise<string> {
    const formData: FormData = new FormData();
    formData.append('file', file);
    return this.http.post(Endpoints.uploadFile, formData)
      .toPromise()
      .then(response => response.text());
  }

  /**
   * Reset user's course. If uuid is not defined (empty string), 1st course will be reset
   * @param uuid. If UUID is empty, actual course will be reset
   * @returns {Promise<TResult>|Promise<T>|Promise<TResult2|TResult1>}
   */
  public resetCourseEnrollment(uuid: string = ''): Promise<any> {
    return this.http.post(
      Endpoints.resetCourseEnrollment + uuid,
      null,
      {headers: this.headers}
    ).toPromise();
  }
}

