/**
 * Created by pati on 21/04/2017.
 */
import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
import {OnBoardService} from './on-board.service';
import {Endpoints} from '../endpoints';
import {Observable} from 'rxjs';

@Injectable()
export class ActionsRequiredService {

  private headers = new Headers({
    'Content-Type': 'application/json'
  });

  constructor(private http: Http,
              private onBoardService: OnBoardService) {
  }

  /**
   * get notifications for tutor
   * @returns {Observable<R|T>}
   */
  getActionsRequired() {
    return this.http.get(Endpoints.actionsRequired, {headers: this.headers})
      .map((res: any) => res.json())
      .catch((error: any) => Observable.throw(error));
  }

  /**
   * get evidence file by id
   * @param fileID
   * @returns {Promise<T>}
   */
  getEvidence(fileID) {
    return this.http.get(Endpoints.getEvidenceByID + fileID, {headers: this.headers}).toPromise();
  }

  /**
   * get nofitication details which contains file id
   * @param notificationID
   * @returns {Observable<R|T>}
   */
  getNotificationInfo(notificationID) {
    return this.http.get(Endpoints.getOneNotificationInfo + notificationID, {headers: this.headers})
      .map((res: any) => res.json())
      .catch((error: any) => Observable.throw(error));
  }

  /**
   * submit evidence for student - PASS or FAIL
   * @param status - PASS, FAIL
   * @param enrollmentUuid
   * @param date
   * @returns {Promise<T>}
   */
  submitEvidence(status, enrollmentUuid, date) {
    return this.http.post(Endpoints.submitEvidenceTutor, {
      status: status,
      enrollmentUuid: enrollmentUuid,
      submitDate: date
    }, {headers: this.headers}).toPromise();
  }

  /**
   * retaking evidence
   * @param studentUuid
   * @param enrolmentUuid
   * @returns {Promise<T>}
   */
  retakeEvidence(studentUuid, enrolmentUuid) {
    return this.http.get(Endpoints.retakeTutorial(studentUuid, enrolmentUuid), {headers: this.headers}).toPromise();
  }
}
