import {Injectable} from "@angular/core";
import {Http, Headers} from "@angular/http";
import {OnBoardService} from "./on-board.service";
import {Endpoints} from "../endpoints";
import {Course} from "../model/course";

@Injectable()
export class HomeScreenService {

  private headers = new Headers({
    'Content-Type': 'application/json',
  });

  constructor(private http: Http) {
  }

  getHomeScreenData(): Promise<Course> {
    return this.http.get(Endpoints.getHomeScreenData, {headers: this.headers})
      .toPromise()
      .then(response => {
        const jsonData = response.json();
        console.log('receive homescreen data: ' + jsonData);
        return jsonData as Course;
      })
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred lala', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
