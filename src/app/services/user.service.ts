import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

import { Endpoints } from '../endpoints';

@Injectable()
export class UserService {
  private headers: Headers;

  constructor(
    private http: Http,
  ) {
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
  }

  getUsers(): Observable<any> {
    const options = new RequestOptions({headers: this.headers});

    return this.http.get(Endpoints.getBasicInfo, options)
      .map((response: Response) => response.json())
      .catch( e => {
        if (e.status === 409) {
          return Observable.throw('UserNotFinishedRegistration');
        } else {
          return Observable.throw('Error');
        }
      });
  }

  /**
   * Creates single student and sends invite email
   * @param pojo
   * @returns promise
   */
  inviteStudent(pojo): Promise<Response[]> {
    return this.http.post(Endpoints.inviteStudents, pojo, {headers: this.headers})
      .toPromise()
      .then((res: Response) => res.json())
      .catch(this.handleError);
  };

  /**
   * Get all users by type
   * @param type
   * @returns list of users
   */
  getUsersByFilter(type): Promise<any> {
    return this.http.post(Endpoints.getUsersByFilter, type, {headers: this.headers})
      .toPromise()
      .then((res: Response) => res.json())
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
