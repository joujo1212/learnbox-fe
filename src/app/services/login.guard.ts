import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {OnBoardService} from './on-board.service';

@Injectable()
export class LoginGuard implements CanActivate {

  constructor(private router: Router,
              private onBoardService: OnBoardService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const expiredTime = +localStorage.getItem('expire_token_time');
    const accessToken = localStorage.getItem('access_token');
    const refreshToken = localStorage.getItem('refresh_token');
    const role = localStorage.getItem('role');
    const uuid = localStorage.getItem('uuid');

    if (expiredTime === 0 || !accessToken || !refreshToken || !role || !uuid) {
      localStorage.removeItem('access_token');
      localStorage.removeItem('refresh_token');
      localStorage.removeItem('expire_token_time');
      localStorage.removeItem('role');
      localStorage.removeItem('uuid');
      return true;
    }

    switch (this.onBoardService.getUserRole()) {
      case 'TUTOR':
        this.router.navigateByUrl('/tutor');
        break;

      case 'STUDENT':
        this.router.navigateByUrl('/student/tutorial/active');
        break;

      case 'SUPER_USER':
        this.router.navigateByUrl('/super-user');
        break;

      case 'EXAMINER':
        this.router.navigateByUrl('/examiner');
        break;

      default:
        this.router.navigateByUrl('/page-not-found');
    }
    return false;
  }

}
