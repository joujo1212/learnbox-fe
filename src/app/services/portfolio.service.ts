import {Injectable} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';
import {OnBoardService} from './on-board.service';
import {Endpoints} from '../endpoints';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class PortfolioService {

  private headers = new Headers({
    'Content-Type': 'application/json',
  });

  constructor(private http: Http) {
  }

  getMyPortfolioCourses(): Promise<Response> {
    return this.http.get(Endpoints.portfolio, {headers: this.headers})
      .toPromise();
  }

}
