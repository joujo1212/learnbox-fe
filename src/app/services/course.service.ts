import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import {OnBoardService} from './on-board.service';
import {Endpoints} from '../endpoints';
import CourseBasic from '../model/course-basic';
import {CourseSetup} from '../model/course-setup';
import {PageRequest} from '../model/page-request';

import 'rxjs/add/operator/toPromise';
import {CourseLogo} from '../model/course-logo';
import {CourseStatistic} from '../model/course-statistic';

@Injectable()
export class CourseService {

  private headers = new Headers({
    'Content-Type': 'application/json',
  });


  constructor(private http: Http) {
  }

  getCourseLogo(): Promise<CourseLogo> {
    return this.http.get(Endpoints.getCourseLogo, {headers: this.headers})
      .toPromise()
      .then(response => response.json() as CourseLogo)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  createCourse(courseSetup: CourseSetup, files: {logo: null, document: null}): Promise<CourseSetup> {
    const formData: FormData = new FormData();
    formData.append('courseLogo', files.logo);
    formData.append('document', files.document);
    formData.append('coursePojo', JSON.stringify(courseSetup));

    return this.http.post(Endpoints.createCourse, formData)
      .toPromise()
      .then(response => response.json() as CourseSetup)
      .catch(this.handleError);
  }

  getCourse(courseUuid: string): Promise<CourseSetup> {
    return this.http.get(Endpoints.getCourse + courseUuid, {headers: this.headers})
      .toPromise()
      .then(response => response.json() as CourseSetup)
      .catch(this.handleError);
  }

  getCourseStatistic(enrollmentUuid?: string): Promise<CourseStatistic> {
    return this.http.get(Endpoints.actualCourseStatistics(enrollmentUuid),
      {headers: this.headers})
      .toPromise()
      .then(response => response.json() as CourseStatistic);
  }

  getCourses(pageRequest: PageRequest): Promise<Array<CourseBasic>> {
    return this.http.get(`${Endpoints.courses}?pgNum=${pageRequest.page}`, {headers: this.headers})
      .toPromise()
      .then(response => response.json().content as Array<CourseBasic>)
      .catch(this.handleError);
  }
}
