import {Injectable} from '@angular/core';
import {environment} from '../environments/environment';

@Injectable()
export class Endpoints {
  public static imageBase = 'http://learnbox-dev.localhost.company';
  public static serverBase = environment.serverBase;
  // public serverBase: string = 'http://192.168.0.74:8765';

  public static finishRegistration = Endpoints.serverBase + '/user-service/user/finishRegistration';
  public static getBasicInfo = Endpoints.serverBase + '/user-service/user/getBasicInfo';
  public static changePassword = Endpoints.serverBase + '/api-service/changePassword';
  public static refreshToken = Endpoints.serverBase + '/oauth2-service/oauth/token?grant_type=refresh_token&refresh_token=';

  // USERS
  public static inviteStudents = Endpoints.serverBase + '/api-service/inviteStudents';
  public static invitedStudentsByFile = Endpoints.serverBase + '/api-service/inviteStudentsByFile'; // TODO BORDEL
  public static inviteStudentsByFile = Endpoints.serverBase + '/api-service/inviteStudentsByFile';
  public static getUsersByFile = Endpoints.serverBase + '/api-service/getUsersByFile';
  public static getUsersByFilter = Endpoints.serverBase + '/api-service/getUsersByFilter';

  // CLASS & COURSES
  public static createClass = Endpoints.serverBase + '/course-service/class/create';
  public static getModulesFromCourse = Endpoints.serverBase + '/api-service/getModulesFromCourse';
  public static getCourseLogo = Endpoints.serverBase + '/api-service/course/getCourseLogo';
  public static getHomeScreenData = Endpoints.serverBase + '/api-service/homeScreen';
  public static portfolio = Endpoints.serverBase + '/api-service/learningPortfolio';
  public static resetPassword = Endpoints.serverBase + '/api-service/generateResetPasswordEmail';
  public static submitVideoTutorial = Endpoints.serverBase + '/api-service/tutorial/video';
  public static submitQuizTutorial = Endpoints.serverBase + '/api-service/tutorial/quiz';
  public static submitImageTutorial = Endpoints.serverBase + '/api-service/tutorial/image';
  public static submitTextTutorial = Endpoints.serverBase + '/api-service/tutorial/text';
  public static getQuiz = Endpoints.serverBase + '/api-service/tutorial/quiz?uuid=';
  public static actualTutorial = Endpoints.serverBase + '/api-service/tutorial/actualTutorial';
  public static submitEvidence = Endpoints.serverBase + '/enrollment-service/tutorialEnrollment/evidence/submit';
  public static actionsRequired = Endpoints.serverBase + '/api-service/tutorial/tutorNotifications';
  public static getOneNotificationInfo = Endpoints.actionsRequired + '/';
  public static getEvidenceByID = Endpoints.serverBase + '/enrollment-service/tutorialEnrollment/getEvidence/';
  public static submitEvidenceTutor = Endpoints.serverBase + '/enrollment-service/tutorialEnrollment/checkEvidence';
  public static getUnits = Endpoints.serverBase + '/course-service/quiz/answerUnits';
  public static resetCourseEnrollment = Endpoints.serverBase + '/enrollment-service/courseEnrollment/reset';
  public static getLearnerProfile = Endpoints.serverBase + '/api-service/class/user';

  // file
  public static uploadFile = Endpoints.serverBase + '/api-service/file';

  // chat
  public static chatroomInfo = Endpoints.serverBase + '/chat-service/chat/room/info';
  public static chatroomJoin = Endpoints.serverBase + '/chat-service/chat/room/join';
  public static chatWS = Endpoints.serverBase + '/chat-service/chat';

  public static createCourse = Endpoints.serverBase + '/course-service/course';
  public static getCourse = Endpoints.serverBase + '/course-service/course/';
  public static updateCourse = Endpoints.serverBase + '/course-service/course/update';
  public static courses = Endpoints.serverBase + '/course-service/course/getAllCourses?';

  public static addLearnersAndClass = Endpoints.serverBase + '/api-service/inviteStudents?course=uuid1000';

  public static viewResults = (userUuid: string, tutorialUuid: string) =>
  Endpoints.serverBase + '/api-service/tutorial/quiz/evaluatedQuiz?uuid=' + tutorialUuid + '&studentUuid=' + userUuid;

  public static getTutorialDetail = (enrollmentTutorialUuid: string) =>
  Endpoints.serverBase + '/api-service/tutorial?uuid=' + enrollmentTutorialUuid;

  public static getModuleDetail = (enrollmentModuleUuid: string) =>
  Endpoints.serverBase + '/api-service/module?uuid=' + enrollmentModuleUuid;

  public static login = (userName: string, userPswd: string) =>
  Endpoints.serverBase + '/oauth2-service/oauth/token?grant_type=password&username=' + userName + '&password=' + userPswd;

  public static retakeTutorial = (studentUuid: string, enrolmentUuid: string) =>
  Endpoints.serverBase + '/enrollment-service/tutorialEnrollment/' + studentUuid + '/' + enrolmentUuid + '/evidence/retakeTutorial';

  public static actualCourseStatistics = (enrollmentCourseUuid?: string) =>
    enrollmentCourseUuid ? Endpoints.serverBase + '/enrollment-service/courseEnrollment/actualCourseStatistics?uuid=' + enrollmentCourseUuid
      : Endpoints.serverBase + '/enrollment-service/courseEnrollment/actualCourseStatistics';

}
