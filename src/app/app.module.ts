import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {Http, HttpModule, RequestOptions, XHRBackend} from '@angular/http';
import {MaterialModule} from '@angular/material';

import {AppComponent} from './app.component';
import {TestComponentComponent} from './components/test-component/test-component.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

/**
 * Services
 */
import {OnBoardService} from './services/on-board.service';
import {UserService} from './services/user.service';
import {PortfolioService} from './services/portfolio.service';

/**
 * Libs
 */
import {Ng2Webstorage}  from 'ng2-webstorage';
import {FileSelectDirective, FileDropDirective, FileUploader} from 'ng2-file-upload/ng2-file-upload';
import 'hammerjs';
import {PerfectScrollbarModule} from 'angular2-perfect-scrollbar';
import {PERFECT_SCROLLBAR_CONFIG} from './libs-config/perfectscrollbar.config';

/**
 * Constants
 */

import {Endpoints} from './endpoints';
import {IndexComponent} from './components/index/index.component';
import {CourseService} from './services/course.service';
import {ModuleService} from './services/module.service';

import {HomeScreenService} from './services/home_screen.service';
import {AppRoutingModule} from './app-routing.module';
import {OnBoardModule} from './modules/on-board-module/on-board.module';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
import {StudentModule} from './modules/student-module/student.module';
import {TutorialService} from './services/tutorial.service';
import {LegalModule} from './modules/legal-module/legal.module';
import {TutorModule} from './modules/tutor-module/tutor.module';
import {ExaminerModule} from './modules/examiner-module/examiner.module';
import {SuperUserModule} from './modules/super-user-module/super-user.module';
import {FooterComponent} from './shared/footer/footer.component';
import {AuthInterceptor} from './services/auth-interceptor';
import {Router} from '@angular/router';
import {AuthGuard} from 'app/services/auth-guard';
import {LoginGuard} from './services/login.guard';
import {ClassLearnerProgressService} from './services/class-learner-progress.service';


export function authInterceptorFactory(backend: XHRBackend, options: RequestOptions, router: Router) {
  return new AuthInterceptor(backend, options, router);
}

@NgModule({
  declarations: [
    AppComponent,
    TestComponentComponent,
    IndexComponent,
    PageNotFoundComponent,

    // FileUploader
    FileSelectDirective,

    // pre vybranie suboru
    // FileDropDirective, //pre dropnutie suboru na urcitu plochu

    FooterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    Ng2Webstorage,
    OnBoardModule,
    StudentModule,
    LegalModule,
    TutorModule,
    ExaminerModule,
    SuperUserModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [
    {
      provide: Http,
      useFactory: authInterceptorFactory,
      deps: [XHRBackend, RequestOptions, Router]
    },
    OnBoardService,
    UserService,
    TutorialService,
    Endpoints,
    CourseService,
    ModuleService,
    HomeScreenService,
    PortfolioService,
    AuthGuard,
    LoginGuard,
    ClassLearnerProgressService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
