import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {OnBoardRoutingModule} from './on-board-routing.module';
import {FinishRegistrationComponent} from './components/finish-registration/finish-registration.component';
import {LoginComponent} from './components/login/login.component';
import {ResetPasswordComponent} from './components/reset-password/reset-password.component';
import {BorderedInputComponent} from './components/bordered-input/bordered-input.component';
import {MaterialModule} from '@angular/material';
import {SharedModule} from '../../shared/shared.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    OnBoardRoutingModule,
    SharedModule,
    MaterialModule.forRoot(),

  ],
  declarations: [
    BorderedInputComponent,
    FinishRegistrationComponent,
    LoginComponent,
    ResetPasswordComponent,
  ],
})
export class OnBoardModule {
}
