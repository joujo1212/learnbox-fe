import { Component, OnInit } from '@angular/core';
import { Router }             from '@angular/router';
import {OnBoardService} from "../../../../services/on-board.service";


@Component({
  selector: 'app-finish-registration',
  templateUrl: 'finish-registration.component.html',
  styleUrls: ['finish-registration.component.css']
})
export class FinishRegistrationComponent implements OnInit {

  constructor(
    private router: Router,
    private onBoardService: OnBoardService
  ) { }

  ngOnInit() {
  }

  continue(birthDate, familyName, genderType, givenName): void {
    this.onBoardService.finishRegistration(birthDate, familyName, genderType, givenName).subscribe(response => {
      this.router.navigate(['/index']);
    }, (err) => {
      if(err === 'Unauthorized') {
        console.log('Unauthorized');
      }
      else if (err === 'Error'){
        console.log('There is problem with authentication.');
      }
    })
  }

}
