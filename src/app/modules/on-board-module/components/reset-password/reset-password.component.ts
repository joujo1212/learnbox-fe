import {Component, OnInit} from '@angular/core';
import {Router}             from '@angular/router';
import {OnBoardService} from "../../../../services/on-board.service";

@Component({
  selector: 'app-reset-password',
  templateUrl: 'reset-password.component.html',
  styleUrls: ['reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  error = '';
  message = '';

  constructor(private onBoardService: OnBoardService,
              private router: Router) {
  }

  ngOnInit() {
  }

  reset(mail): void {
    this.onBoardService.resetPassword(mail).subscribe(response => {
      if (response.status === 200) {
        this.message = 'Email was sent successfully!';
        this.error = '';
      }
    }, (err) => {
      this.message = '';
      this.error = 'Incorrect email';
    });
  }

}
