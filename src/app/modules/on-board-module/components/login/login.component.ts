import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { OnBoardService } from '../../../../services/on-board.service';

@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css']
})
export class LoginComponent {
  user: any = {};
  error = '';
  isLogging = false;
  @ViewChild('password') password: any;
  @ViewChild('username') username: any;

  constructor(
    private onBoardService: OnBoardService,
    private router: Router) {
  }

  login(mail, password): void {
    if (!this.isLogging) {
      // TODO: zmazat po nasadeny na ostro
      if (typeof mail === 'undefined') {
        mail = 'falty@centrum.sk';
        password = 'kvetinac';
      }
      this.isLogging = true;
      this.onBoardService.login(mail, password).subscribe(
        response => {
          // get basic info about user and redirect accordingly to his role
          this.onBoardService.getBasicInfo().then(
            basicInfo => {
              this.onBoardService.setUserRole(basicInfo.roles['0']);
              this.onBoardService.setUserUuid(basicInfo.uuid);
              if (basicInfo.roles['0'] === 'STUDENT') {
                this.router.navigate(['/student/tutorial/active']);
              }
              if (basicInfo.roles['0'] === 'TUTOR') {
                this.router.navigate(['/tutor']);
              }
              if (basicInfo.roles['0'] === 'SUPER_USER') {
                this.router.navigate(['/super-user']);
              }
              if (basicInfo.roles['0'] === 'EXAMINER') {
                this.router.navigate(['/examiner']);
              }
            },
            err => {
              this.isLogging = false;
              console.log(err);
            }
          );
        }, (err) => {
          this.isLogging = false;
          console.log('Error: ', err);
          this.error = 'Incorrect email or password';
        });
    }
  }

  goToPassword() {
    this.password.input.nativeElement.focus();
  }

  goToUsername() {
    this.username.input.nativeElement.focus();
  }

}
