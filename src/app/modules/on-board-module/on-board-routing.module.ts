

import {Routes, RouterModule} from "@angular/router";
import {LoginComponent} from "./components/login/login.component";
import {FinishRegistrationComponent} from "./components/finish-registration/finish-registration.component";
import {ResetPasswordComponent} from "./components/reset-password/reset-password.component";
import {NgModule} from "@angular/core";
import {LoginGuard} from '../../services/login.guard';
const onBoardRoutes: Routes = [
  {
    path: 'login',
    canActivate: [LoginGuard],
    component: LoginComponent
  },
  {
    path: 'resetPassword',
    canActivate: [LoginGuard],
    component: ResetPasswordComponent
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(onBoardRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class OnBoardRoutingModule{}
