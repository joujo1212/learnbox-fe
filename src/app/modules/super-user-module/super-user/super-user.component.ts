import {Component, OnInit} from '@angular/core';
import {Router, NavigationEnd} from '@angular/router';

@Component({
  selector: 'super-user-module',
  templateUrl: 'super-user.component.html',
  styleUrls: ['super-user.component.css']
})
export class SuperUserComponent implements OnInit {
  constructor(public router: Router) {
  };

  ngOnInit() {
    this.scrollUpWhenUrlChanged();
  }

  /**
   * Scroll container up when url was changed
   */
  private scrollUpWhenUrlChanged() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      const container = document.getElementsByClassName('super-user-container').item(0);
      if (container) {
        container.scrollTop = 0;
      }
    });
  }
}
