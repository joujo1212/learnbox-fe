import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { CourseService } from '../../../services/course.service';
import { SuperUserStore } from '../service/super-user-store.service';

@Injectable()
export class GetCourseResolver implements Resolve<boolean> {

  constructor(private courseService: CourseService,
              private router: Router,
              private superUserStore: SuperUserStore) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
  const courseUuid: string = route.params['id'];
  return this.courseService.getCourse(courseUuid)
      .then(course => {
        this.superUserStore.storeCourseSetup(course);
        return true;
      })
      .catch(error => {
        // TODO Uncomment when all will work on BE
        // this.router.navigate(['/page-not-found']);
        return null;
      });
  }
}
