import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { ChartsModule } from 'ng2-charts';
import { SharedModule } from '../../shared/shared.module';
import { ModalModule } from 'angular2-modal';
import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';

import { DndModule } from 'ng2-dnd';
import { StompService } from 'ng2-stomp-service';
import { ActionsRequiredService } from '../../services/actionsRequired.service';
import { OnBoardService } from '../../services/on-board.service';

import { SuperUserRoutingModule } from './super-user-routing.module';
import { SuperUserComponent } from './super-user/super-user.component';
import { CourseSetupComponent } from './course-setup/course-setup.component';
import { UserSetupComponent } from './user-setup/user-setup.component';
import { GetCourseResolver } from './resolver/get-course-resolver.service';
import { SuperUserStore } from './service/super-user-store.service';
import { AddTutorExaminerComponent } from './user-setup/add-tutor-examiner/add-tutor-examiner.component';
import { CoursesListingComponent } from './components/coursesListing/courses-listing.component';
import { CourseModuleDetailComponent } from './components/module-detail/course-module-detail.component';
import { CourseTutorialDetailComponent } from './components/course-tutorial-detail/course-tutorial-detail.component';
import { ItemEditMenuComponent } from './course-setup/editor/item-edit-menu/item-edit-menu.component';
import { LoadingComponent } from './course-setup/loading/loading.component';
import { GroupsController } from './course-setup/editor/groups-controller/groups-controller';
import { RadioController } from './course-setup/editor/radio-controller/radio-controller';
import { CheckboxController } from './course-setup/editor/checkbox-controller/checkbox-controller';
import { ControlPanelCategories } from './course-setup/editor/dnd-control-panel-categories/control-panel-categories';
import { ProjectionComponent } from './course-setup/editor/projection/projection.component';
import { ControlPanel } from './course-setup/editor/dnd-control-panel/control-panel';
import { EditorComponent } from './course-setup/editor/editor.component';
import { EditorService } from './service/editor.service';
import { PromptModalComponent } from './course-setup/editor/modals/prompt.modal';
import {ImageUploadModule} from 'ng2-imageupload';

@NgModule({
  imports: [
    ImageUploadModule,
    CommonModule,
    MaterialModule.forRoot(),
    SuperUserRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    ChartsModule,
    SharedModule,
    ModalModule.forRoot(),
    BootstrapModalModule,
    DndModule.forRoot()
  ],
  declarations: [
    SuperUserComponent,
    CourseSetupComponent,
    UserSetupComponent,
    AddTutorExaminerComponent,
    CoursesListingComponent,
    CourseModuleDetailComponent,
    UserSetupComponent,
    EditorComponent,
    ControlPanel,
    ProjectionComponent,
    ControlPanelCategories,
    CheckboxController,
    RadioController,
    GroupsController,
    LoadingComponent,
    ItemEditMenuComponent,
    CourseTutorialDetailComponent,
    PromptModalComponent
  ],
  providers: [StompService, ActionsRequiredService, OnBoardService, GetCourseResolver, SuperUserStore, EditorService],
  entryComponents: [PromptModalComponent]
})
export class SuperUserModule {}
