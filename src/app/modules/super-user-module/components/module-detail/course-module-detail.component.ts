import { Component, Input } from '@angular/core';
import { ModuleDetailedView } from '../../../../model/module-detailed-view';

@Component({
  selector: 'app-course-module-detail',
  templateUrl: 'course-module-detail.component.html',
  styleUrls: ['course-module-detail.component.css']
})
export class CourseModuleDetailComponent {

  @Input()
  module: ModuleDetailedView;

  @Input()
  position: number;

  expanded: boolean = false;

  toggle() {
    this.expanded = !this.expanded;
  }

}
