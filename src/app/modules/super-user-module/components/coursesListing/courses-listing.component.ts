import {Component, OnInit} from '@angular/core';
import CourseBasic from '../../../../model/course-basic';
import {CourseService} from '../../../../services/course.service';
import {PageRequest} from '../../../../model/page-request';

@Component({
  selector: 'app-courses-listing',
  templateUrl: 'courses-listing.component.html',
  styleUrls: ['courses-listing.component.css']
})
export class CoursesListingComponent implements OnInit {

  courses: Array<CourseBasic>;
  pageRequest: PageRequest = new PageRequest();

  constructor(private courseService: CourseService) {}

  ngOnInit() {
    this.pageRequest.page = 0;

    this.courseService.getCourses(this.pageRequest)
      .then(courses => this.courses = courses);
  }

}
