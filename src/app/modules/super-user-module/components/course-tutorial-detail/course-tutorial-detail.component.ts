import { Component, Input } from '@angular/core';
import { TutorialDetailedView } from '../../../../model/tutorial-detailed-view';

@Component({
  selector: 'app-course-tutorial-detail',
  templateUrl: './course-tutorial-detail.component.html',
  styleUrls: ['./course-tutorial-detail.component.css']
})
export class CourseTutorialDetailComponent {

  @Input()
  tutorial: TutorialDetailedView;

  expanded: boolean = false;

  toggle() {
    this.expanded = !this.expanded;
  }
}
