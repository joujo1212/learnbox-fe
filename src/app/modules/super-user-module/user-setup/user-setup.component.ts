import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'user-setup',
  templateUrl: 'user-setup.component.html',
  styleUrls: ['user-setup.component.css']
})
export class UserSetupComponent {
  constructor(
    public router: Router
  ) {
  }
}
