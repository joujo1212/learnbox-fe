import {Component, ViewContainerRef, OnInit, OnDestroy} from '@angular/core';
import {AddUser} from '../../../../model/add-user';
import {Overlay, overlayConfigFactory} from 'angular2-modal';
import {Modal, BSModalContext} from 'angular2-modal/plugins/bootstrap';
import {CustomModalContext, SuccessModal} from '../../../../shared/modals/success-modal/success-modal.component';
import {UserService} from '../../../../services/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import CourseBasic from '../../../../model/course-basic';

@Component({
  selector: 'add-tutor',
  templateUrl: 'add-tutor-examiner.component.html',
  styleUrls: ['add-tutor-examiner.component.css']
})
export class AddTutorExaminerComponent implements OnInit, OnDestroy {
  public addTutorExaminer = new AddUser();
  public activeCoursesList: CourseBasic[] = []; // list with all courses at bottom of add tutor
  public existingUsers: any = []; // content of scrollable table
  public tableType = 'add-user'; // type of scrollable table
  public selectedRole; // used in right side list with all users
  private sub: any; // subscription for route data containing viewType
  private viewType; // 'tutor' or 'examiner' view

  constructor(
    public router: Router,
    overlay: Overlay,
    vcRef: ViewContainerRef,
    public modal: Modal,
    private _userService: UserService,
    private route: ActivatedRoute
  ) {
    overlay.defaultViewContainer = vcRef;
  }

  ngOnInit(): void {
    // check what view is displayed, add tutor or add examiner
    this.sub = this.route.data.subscribe((value: any) => {
      this.viewType = value.type;
      if (this.viewType === 'tutor') {
        this.selectedRole = 'TUTOR';
      } else {
        this.selectedRole = 'EXAMINER';
      }
      this.showUsers(this.selectedRole);
    });

    const activeCourse1 = new CourseBasic();
    activeCourse1.courseLogo = '../../../../assets/english-course.png';
    activeCourse1.since = '22 /02 / 2017';
    activeCourse1.courseTitle = 'English';
    const activeCourse2 = new CourseBasic();
    activeCourse2.courseLogo = '../../../../assets/maths-course.png';
    activeCourse2.since = '22 /02 / 2017';
    activeCourse2.courseTitle = 'Maths';
    this.activeCoursesList.push(activeCourse1);
    this.activeCoursesList.push(activeCourse2);
  }

  addNewTutor(event: Event) {
    event.preventDefault();

    const pojo = [{
      'email' : this.addTutorExaminer.email,
      'name' : this.addTutorExaminer.name,
      'company' : this.addTutorExaminer.company,
      'street' : this.addTutorExaminer.street,
      'city' : this.addTutorExaminer.city,
      'zipCode' : this.addTutorExaminer.zipCode,
      'phoneNumber' : this.addTutorExaminer.phoneNumber
    }];

    const context = new CustomModalContext();
    context.title = 'Success!';
    context.body = 'Learner was successfully added!';
    return this.modal.open(SuccessModal, overlayConfigFactory(context, BSModalContext));
  }

  showUsers(type) {
    const pojo = {
      'roles': [type],
      'pageRequestPojo': { 'page': 0}};
    this._userService.getUsersByFilter(pojo).then(
      response => {
        this.existingUsers = response.content;
      },
      error => {
        throw error;
      }
    );
  }

  onTabClick(event) {
    if (event.index === 0) {
      this.selectedRole = 'EXAMINER';
      this.showUsers(this.selectedRole);
    }
    if (event.index === 1) {
      this.selectedRole = 'TUTOR';
      this.showUsers(this.selectedRole);
    }
    if (event.index === 2) {
      this.selectedRole = 'STUDENT';
      this.showUsers(this.selectedRole);
    }
  };

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
