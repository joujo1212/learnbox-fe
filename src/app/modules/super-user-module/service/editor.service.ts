/**
 * Created by pati on 23/03/2017.
 */
import {Injectable} from '@angular/core';
import {Headers, Http, Response} from "@angular/http";
import {Observable} from "rxjs";
import {Endpoints} from '../../../endpoints';

@Injectable()
export class EditorService {
  private headers: Headers;
  private uuid: string;

  endpoint = Endpoints.serverBase + '/course-service/quiz/question';

  setEndpoint(value) {
    if (value === 'dev') {
      this.endpoint = 'http://server.localhostsro.sk:28765/course-service/quiz/question';
      console.log('dev');
    } else {
      this.endpoint = 'http://server.localhostsro.sk:18765/course-service/quiz/question';
      console.log('prod');

    }
  }

  setUUID(uuid) {
    this.uuid = uuid;
  }

  constructor(private http: Http,
              // private localSt: LocalStorageService
  ) {
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    // this.headers.append('token', this.localSt.retrieve('token'));
  }

  addSecretQuestion(html, secrets): Observable<Response> {
    let i = 0;
    let tmp = [];
    while (i < secrets.length) {
      tmp[i] = {'answerId': secrets[i].id, 'value': secrets[i].value};
      i++;
    }
    let obj = {
      html: html,
      type: 'SECRET',
      answers: tmp,
      quizUuid: this.uuid
    };
    console.log(obj);

    return this.http.post(this.endpoint,
      obj, {headers: this.headers})
      .map((res: any) => res.json().data)
      .catch((error: any) => Observable.throw(error));
  }

  addDNDQuestion(html, dnds): Observable<Response> {
    let i = 0;
    let tmp = [];
    while (i < dnds.length) {
      tmp[i] = {'answerId': dnds[i].id, 'value': dnds[i].value};
      i++;
    }
    let obj = {
      html: html,
      type: 'DROPTEXT',
      answers: tmp,
      quizUuid: this.uuid
    };
    console.log(obj);
    return this.http.post(this.endpoint,
      obj, {headers: this.headers})
      .map((res: any) => res.json().data)
      .catch((error: any) => Observable.throw('request berie za error ale fici - zahada'));
  }

  addCategoriesQuestion(html, categories): Observable<Response> {
    let i = 0, j = 0;
    let tmp = [];
    console.log(categories);
    while (i < categories.length) {
      tmp[i] = {
        name: categories[i].name,
        items: []
      };
      j = 0;
      while (j < categories[i].items.length) {
        tmp[i].items.push({
          'answerId': categories[i].items[j].id,
          'value': categories[i].items[j].name  // note here .. name is VALUE in this case
        });
        j++;
      }
      i++;
    }
    let obj = {
      html: html,
      type: 'CATEGORY',
      answers: tmp,
      quizUuid: this.uuid
    };
    console.log(obj);

    return this.http.post(this.endpoint,
      obj, {headers: this.headers})
      .map((res: any) => res.json().data)
      .catch((error: any) => Observable.throw(error));
  }

  addCheckboxQuestion(html, checkboxes): Observable<Response> {
    let i = 0;
    let tmp = [];
    console.log(checkboxes);
    while (i < checkboxes.length) {
      tmp[i] = {
        'answerId': checkboxes[i].id,
        'value': checkboxes[i].value,
        'checked': checkboxes[i].checked
      };
      i++;
    }
    let obj = {
      html: html,
      type: 'CHECKBOX',
      answers: tmp,
      quizUuid: this.uuid
    };
    console.log(obj);

    return this.http.post(this.endpoint,
      obj, {headers: this.headers})
      .map((res: any) => res.json().data)
      .catch((error: any) => Observable.throw(error));
  }

  addCheckboxTextQuestion(html, checkboxes): Observable<Response> {
    let i = 0;
    let tmp = [];
    console.log(checkboxes);
    while (i < checkboxes.length) {
      tmp[i] = {
        'answerId': checkboxes[i].id,
        'value': null,
        'checked': checkboxes[i].checked
      };
      i++;
    }
    let obj = {
      html: html,
      type: 'CHECKBOX_TEXT',
      answers: tmp,
      quizUuid: this.uuid
    };
    console.log(obj);

    return this.http.post(this.endpoint,
      obj, {headers: this.headers})
      .map((res: any) => res.json().data)
      .catch((error: any) => Observable.throw(error));
  }

  addRadioQuestion(html, radio): Observable<Response> {
    let i = 0;
    let tmp = [];
    console.log(radio);
    while (i < radio.length) {
      tmp[i] = {
        'answerId': radio[i].id,
        'value': radio[i].value,
        'checked': radio[i].checked
      };
      i++;
    }
    let obj = {
      html: html,
      type: 'RADIO',
      answers: tmp,
      quizUuid: this.uuid
    };
    console.log(obj);

    return this.http.post(this.endpoint,
      obj, {headers: this.headers})
      .map((res: any) => res.json().data)
      .catch((error: any) => Observable.throw(error));
  }

  addGroupsQuestion(html, groups): Observable<Response> {

    let i = 0;
    let tmp = [], tmp1 = [];
    console.log(groups);
    while (i < groups.items.length) {
      tmp = [];
      tmp.push({
        'answerId': groups[0].items[i].id,
        'value': groups[0].items[i].value
      });
      tmp.push({
        'answerId': groups[1].items[i].id,
        'value': groups[1].items[i].value
      });
      tmp1.push(tmp);
      i++;
    }
    let obj = {
      html: html,
      type: 'CATEGORY',
      answers: tmp1,
      quizUuid: this.uuid
    };
    console.log(obj);

    return this.http.post(this.endpoint,
      obj, {headers: this.headers})
      .map((res: any) => res.json().data)
      .catch((error: any) => Observable.throw(error));
  }
}
