import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {TutorialService} from '../../../services/tutorial.service';
import {ModuleService} from '../../../services/module.service';
import {Router, ActivatedRoute} from '@angular/router';
import {OnBoardService} from '../../../services/on-board.service';
import {Http} from '@angular/http';
import {CourseSetup} from '../../../model/course-setup';

@Injectable()
export class SuperUserStore {
  private setupCourseSubject: BehaviorSubject<CourseSetup> = new BehaviorSubject<CourseSetup>(null);
  public setupCourseObservable: Observable<CourseSetup> = this.setupCourseSubject.asObservable();

  constructor(private tutorialService: TutorialService,
              private moduleService: ModuleService,
              private route: ActivatedRoute,
              private router: Router,
              private http: Http,
              private onBoardService: OnBoardService) {
  }

  storeCourseSetup(course: CourseSetup) {
    this.setupCourseSubject.next(course);
  }
}
