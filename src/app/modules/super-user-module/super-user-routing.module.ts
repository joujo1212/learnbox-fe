import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CourseSetupComponent } from './course-setup/course-setup.component';
import { UserSetupComponent } from './user-setup/user-setup.component';
import { SuperUserComponent } from './super-user/super-user.component';
import { UserDashboardComponent } from '../../shared/user-dashboard/user-dashboard.component';
import { GetCourseResolver } from './resolver/get-course-resolver.service';
import {AddLearnerComponent} from '../../shared/add-learner/add-learner.component';
import {AddTutorExaminerComponent} from './user-setup/add-tutor-examiner/add-tutor-examiner.component';
import {AuthGuard} from '../../services/auth-guard';

const superUserRoutes: Routes = [
  {
    path: 'super-user',
    component: SuperUserComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    children: [
      {
        path: '',
        component: UserDashboardComponent
      },
      {
        path: 'user-setup',
        component: UserSetupComponent,
        children: [
          {
            path: 'add-learner',
            component: AddLearnerComponent
          },
          {
            path: 'add-tutor',
            component: AddTutorExaminerComponent,
            data : {type: 'tutor'}
          },
          {
            path: 'add-examiner',
            component: AddTutorExaminerComponent,
            data : {type: 'examiner'}
          }
        ]
      },
      {
        path: 'course-setup',
        component: CourseSetupComponent
      },
      {
        path: 'course-setup/:id',
        component: CourseSetupComponent,
        resolve: {
          value: GetCourseResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(superUserRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class SuperUserRoutingModule {
}
