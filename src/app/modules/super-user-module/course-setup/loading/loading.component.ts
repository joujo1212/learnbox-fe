/**
 * Created by pati on 11/03/2017.
 */
import {Component, Input, OnChanges} from '@angular/core';


@Component({
  selector: 'loading-2',
  templateUrl: 'loading.component.html',
  styleUrls: ['/loading.component.scss']

})
export class LoadingComponent implements OnChanges{

  private loadingState: boolean;
  @Input('loadingState') loadingStateInput: boolean;

  ngOnChanges(changes: any): void {
  this.loadingState = this.loadingStateInput;
  }



}
