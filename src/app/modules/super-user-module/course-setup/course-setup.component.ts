import {Component, OnInit} from '@angular/core';
import {CourseSetup} from '../../../model/course-setup';
import {CourseService} from '../../../services/course.service';
import {Subscription} from 'rxjs';
import {SuperUserStore} from '../service/super-user-store.service';
import {CourseType} from '../../../model/course-type.enum';
import {Router} from '@angular/router';
import { ModuleDetailedView} from '../../../model/module-detailed-view';

@Component({
  selector: 'course-setup',
  templateUrl: 'course-setup.component.html',
  styleUrls: ['course-setup.component.css']
})
export class CourseSetupComponent implements OnInit {

  public course: CourseSetup = new CourseSetup();
  public files = {logo: null, document: null};
  public subscriptionSetupCourse: Subscription;
  public courseDateString: string;
  public courseUpdatedString: string;
  public editorShown: boolean = false;

  constructor(public router: Router,
              public courseService: CourseService,
              public superUserStore: SuperUserStore) {
  }

  ngOnInit() {
    this.subscriptionSetupCourse = this.superUserStore.setupCourseObservable
      .subscribe(course => {
        if (course) {
          this.course = course;
          this.courseDateString = new Date(course.courseDate).toISOString().slice(0, 10);
          this.courseUpdatedString = new Date(course.courseUpdated).toISOString().slice(0, 10);
        }
      });
  }

  setEditorVisibility(value: boolean): void {
    this.editorShown = value;
  }

  createCourse(isValid: boolean) {
    if (isValid === true) {
      this.course.courseDate = new Date(this.courseDateString.split('-').join('-')).getTime();
      this.courseService.createCourse(this.course, this.files);
    }
  }

  saveAsDraft(isValid: boolean) {
    this.course.draft = true;
    this.createCourse(isValid);
  }

  publishCourse(isValid: boolean) {
    this.course.draft = false;
    this.createCourse(isValid);
  }

  deleteCourse() {
    // TODO implement when BE will be ready
  }

  addLogoFile($event) {
    this.files.logo = $event.target.files[0] || $event.scrElement.files;
  }

  addDocumentFile($event) {
    this.files.document = $event.target.files[0] || $event.scrElement.files;
  }

  changeCourse($event: any) {
    this.course.courseType = $event.index === 0 ? CourseType.BASIC : CourseType.ADVANCED;
  }

  addModule() {
    this.course.modules.push(new ModuleDetailedView());
  }
}
