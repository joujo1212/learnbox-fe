/**
 * Created by pati on 23/02/2017.
 */
import {Component, OnInit, ViewChild, Input, OnChanges, Output, EventEmitter} from '@angular/core';
import {ModalService} from '../../../../../services/modal.service';

declare var $: any;
@Component({
  selector: 'controller',
  templateUrl: '/control-panel.html',
  styleUrls: ['/control-panel.scss']
})
export class ControlPanel implements OnInit, OnChanges {
  @ViewChild('controller') controller: any;
  @Input('hide') hide;
  @Input('panelName') panelName;
  @Input('datadnd1') datadnd1;
  @Output('dnd1DataChanged') dnd1DataChanged = new EventEmitter();
  @Output('addFakeAnswerDnd1') addFakeAnswerDnd1 = new EventEmitter();

  ishidden = true;
  dataToShow = [];

  constructor(public modalService: ModalService) {}

  changedSomething(item) {
    this.dnd1DataChanged.emit({data: item});
  }

  ngOnChanges(changes): void {
    if (typeof changes.hide != 'undefined' ) {
      this.ishidden = changes.hide.currentValue;
    } else if (typeof changes.datadnd1 != 'undefined') {
      this.dataToShow = changes.datadnd1.currentValue;
    }
  }

  fakeAnswer() {
    this.showPromptDNDname(data => {
      const object = {
        id: null,
        name: data.fakeName,
        type : 'dragFromBottomToText',
        value: data.fakeAnswer
      };
      this.dataToShow.push(object);
      this.addFakeAnswerDnd1.emit({data: this.dataToShow});
    });
  }

  ngOnInit(): void {
    $(this.controller.nativeElement).resizable({
      minHeight: 350,
      minWidth: 250
    });
    $(this.controller.nativeElement).draggable({handle: '.h1'});
  }

  // prompt
  showPromptDNDname(callback) {
    const number = this.dataToShow.length + 1;
    this.modalService.showPromptModal('Set fake item', 'Enter name for fake answer!', 'fakeAnswer', 'Fake answer', '',
      '', true, 'Drop ' + number + ' (fake)')
      .then(res => res && callback(res));
  }
}
