/**
 * Created by pati on 23/02/2017.
 */
import {Component, OnInit, ViewChild, Input, OnChanges, Output, EventEmitter} from '@angular/core';
import {ModalService} from '../../../../../services/modal.service';

declare var $: any;
@Component({
    selector: 'controllergroups',
    templateUrl: '/groups-controller.html',
    styleUrls: ['/groups-controller.scss']
})
export class GroupsController implements OnInit, OnChanges {

    @ViewChild('controllerGroups') controller: any;

    ishidden = true;
    groupsData = [{items: [], out: '- 0 '}, {items: [], out: '- 1 '}];
    answers = [];

    @Input('hide') hide;
    @Input('panelName') panelName;
    @Input('datadnd1') datadnd1;
    @Output('dnd1DataChanged') dnd1DataChanged = new EventEmitter();
    @Output('addFakeAnswerDnd1') addFakeAnswerDnd1 = new EventEmitter();

    constructor(public modalService: ModalService) {}

    changedSomething(groupsData) {
        this.dnd1DataChanged.emit({groupsData: groupsData});
    }

    ngOnChanges(changes): void {
        if (typeof changes.hide != 'undefined') {
            this.ishidden = changes.hide.currentValue;
        } else if (typeof changes.datadnd1 != 'undefined') {
            this.groupsData = changes.datadnd1.currentValue;
        }
    }

    addItem() {
        this.showPromptDNDname(data => {
            const id1 = Math.floor((Math.random() * 1000000000) + 1);
            const id2 = Math.floor((Math.random() * 1000000000) + 1);
            const object = {
                id: id1,
                type: 'groups',
                value: data.answer1
            };
            const object1 = {
                id: id2,
                type: 'groups',
                value: data.answer2
            };
            const answer = {id1: id1, id2: id2};
            this.answers.push(answer);
            const tmp = this.groupsData[0];
            const tmp1 = this.groupsData[1];
            tmp.items.push(object);
            tmp1.items.push(object1);
            this.addFakeAnswerDnd1.emit({groupsData: this.groupsData});
        });
    }

    ngOnInit(): void {
        $(this.controller.nativeElement).resizable({
            minHeight: 350,
            minWidth: 500
        });
        $(this.controller.nativeElement).draggable({handle: '.h1'});
    }

    // prompt
    showPromptDNDname(callback) {
        this.modalService.showPromptModal('Set name for item', 'Enter name for answer!', 'answer1', 'Answer 1',
          'answer2', 'Answer 2')
          .then(res => res && callback(res));
    }
}
