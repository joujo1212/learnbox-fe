/**
 * Created by pati on 27/02/2017.
 */
import {Component, Input, OnChanges, Renderer, ViewEncapsulation} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

declare const document: any;

@Component({
    selector: 'projection',
    templateUrl: '/projection.component.html',
    styleUrls: ['/projection.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ProjectionComponent implements OnChanges {

    @Input('htmlData') projection: any;
    @Input('dnd1Data') dnd1Data: any;
    @Input('categoriesData') categoriesData: any;
    @Input('checkboxData') checkboxData: any;
    @Input('radioData') radioData: any;
    @Input('groupsData') groupsData: any;
    @Input('categoryNumber') categoryNumber: any;

    // private categoriesShuffledData = [];
    private cat1items = [];
    private cat2items = [];
    private loading = true;
    counter = 0;
    secretHolder = [];

    ngOnChanges(changes: any): void {
        this.loading = true;
        // TODO neskor odstranit BE nebude posielat data so spravnymi vysledkami - zaciatok
        let i = 0;
        while (i < this.checkboxData.length) {
            this.checkboxData[i].checked = false;
            i++;
        }
         i = 0;
        while (i < this.radioData.length) {
            this.radioData[i].checked = false;
            i++;
        }
        // TODO neskor odstranit BE nebude posielat data so spravnymi vysledkami - koniec

        let j = 0;
        if (changes.categoryNumber.currentValue == 0) {
            while (j < this.secretHolder.length) {
                this.secretHolder[i].parentNode.addChild(this.secretHolder[i]);
                j++;
            }
        }
        // could be done by full dynamic elements projection.. but not necessary because of time
        setTimeout(() => {
            let elementsToRemove: any;
            let i = 0;
            if (changes.categoryNumber.currentValue == 0) {
                elementsToRemove = document.querySelectorAll('#lecture_player .dragFromBottomToText');
                while (i < elementsToRemove.length) {
                    elementsToRemove[i].parentNode.removeChild(elementsToRemove[i]);
                    i++;
                }
            } else if (changes.categoryNumber.currentValue == 1) {
                elementsToRemove = document.querySelectorAll('#lecture_player .secretInputFromWord');
                this.secretHolder = elementsToRemove;
                while (i < elementsToRemove.length) {
                    elementsToRemove[i].parentNode.removeChild(elementsToRemove[i]);
                    i++;
                }
            } else {
                elementsToRemove = document.querySelectorAll('#lecture_player .dragFromBottomToText');
                while (i < elementsToRemove.length) {
                    elementsToRemove[i].parentNode.removeChild(elementsToRemove[i]);
                    i++;
                }
                i = 0;
                elementsToRemove = document.querySelectorAll('#lecture_player .secretInputFromWord');
                while (i < elementsToRemove.length) {
                    elementsToRemove[i].parentNode.removeChild(elementsToRemove[i]);
                    i++;
                }
            }
        }, 250);
        setTimeout(() => {
            this.loading = false;
        }, 500);
    }

    // TODO pri realnej projekcii naplnit categoriesshuffleddata :)
    transferDataSuccess($event) {
        if ($event.mouseEvent.target.id === 'cat1') {
            const sameArrayTest = this.deleteItemFromArray($event.dragData.id, this.cat1items);
            if (sameArrayTest != null) {
                this.cat1items.push(sameArrayTest);
                return;
            }
            const draggedItem = this.deleteItemFromArray($event.dragData.id, this.categoriesData[0].items);
            const draggedItem1 = this.deleteItemFromArray($event.dragData.id, this.cat2items);
            if (draggedItem != null) {
                this.cat1items.push(draggedItem);
            } else if (draggedItem1 != null) {
                this.cat1items.push(draggedItem1);

            } else {
                this.cat1items.push(this.deleteItemFromArray($event.dragData.id, this.categoriesData[1].items));
            }
        } else {
            const sameArrayTest = this.deleteItemFromArray($event.dragData.id, this.cat2items);
            if (sameArrayTest != null) {
                this.cat2items.push(sameArrayTest);
                return;
            }
            const draggedItem = this.deleteItemFromArray($event.dragData.id, this.categoriesData[0].items);
            const draggedItem1 = this.deleteItemFromArray($event.dragData.id, this.cat1items);
            if (draggedItem != null) {
                this.cat2items.push(draggedItem);
            } else if (draggedItem1 != null) {
                this.cat2items.push(draggedItem1);
            } else {
                this.cat2items.push(this.deleteItemFromArray($event.dragData.id, this.categoriesData[1].items));
            }
        }
    }

    deleteItemFromArray(id, array) {
        let i = 0;
        while (i < array.length) {
            if (array[i].id == id) {
                const returnValue = array[i];
                array.splice(i, 1);
                return returnValue;
            }
            i++;
        }
        return null;
    }

    // TODO use in future; array shuffler
    shuffle(array) {
        let currentIndex = array.length, temporaryValue, randomIndex;
        while (0 !== currentIndex) {
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }
        return array;
    }

    changedSomething(radioData, pos) {
        // console.log((radioData);
        if (pos != null) {
            if (!radioData[pos].checked) {
                radioData[pos].checked = false;
            } else {
                let i = 0;
                const change = true;
                while (i < radioData.length) {
                    radioData[i].checked = false;
                    i++;
                }
                if (change) {
                    radioData[pos].checked = true;
                }
            }
        }
    }
}
