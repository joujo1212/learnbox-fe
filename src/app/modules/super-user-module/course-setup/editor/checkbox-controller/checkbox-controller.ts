/**
 * Created by pati on 06/03/2017.
 */
import {Component, OnInit, ViewChild, Input, OnChanges, Output, EventEmitter} from '@angular/core';
import {ModalService} from '../../../../../services/modal.service';

declare var $: any;
@Component({
  selector: 'controllercheckbox',
  templateUrl: '/checkbox-controller.html',
  styleUrls: ['/checkbox-controller.scss']
})
export class CheckboxController implements OnInit, OnChanges {

  @ViewChild('controllerCheck') controller: any;

  ishidden = true;
  dataToShow = [];
  checked = false;
  @Input('hide') hide;
  @Input('panelName') panelName;
  @Input('datadnd1') datadnd1;
  @Output('dnd1DataChanged') dnd1DataChanged = new EventEmitter();
  @Output('addFakeAnswerDnd1') addFakeAnswerDnd1 = new EventEmitter();

  changedSomething(checkboxData) {
    this.dnd1DataChanged.emit({checkboxData: checkboxData});
  }

  constructor(public modalService: ModalService) {}

  ngOnChanges(changes): void {
    if (typeof changes.hide != 'undefined') {
      this.ishidden = changes.hide.currentValue;
    } else if (typeof changes.datadnd1 != 'undefined') {
      this.dataToShow = changes.datadnd1.currentValue;
    }
  }

  addAnswer() {
    this.showPromptDNDname(data => {
      let id = new Date().getTime() + '_' + Math.floor((Math.random() * 1000) + 1);
      let object = {
        id: id,
        type: 'checkbox',
        value: data.fakeAnswer,
        checked: false
      };
      this.dataToShow.push(object);
      this.dnd1DataChanged.emit({checkboxData: this.dataToShow});
    });
  }

  ngOnInit(): void {
    $(this.controller.nativeElement).resizable({
      minHeight: 350,
      minWidth: 250
    });
    $(this.controller.nativeElement).draggable({handle: '.h1'});
  }

  // prompt
  showPromptDNDname(callback) {
    this.modalService.showPromptModal('Add checkbox answer', 'Enter value for checkbox answer!', 'fakeAnswer', 'answer')
      .then(res => res && callback(res));
  }
}
