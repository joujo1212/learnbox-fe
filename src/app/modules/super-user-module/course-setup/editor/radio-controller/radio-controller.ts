/**
 * Created by pati on 06/03/2017.
 */
import {Component, OnInit, ViewChild, Input, OnChanges, Output, EventEmitter} from '@angular/core';
import {ModalService} from '../../../../../services/modal.service';

declare var $: any;
@Component({
  selector: 'controllerradio',
  templateUrl: '/radio-controller.html',
  styleUrls: ['/radio-controller.scss']
})
export class RadioController implements OnInit, OnChanges {

  @ViewChild('controllerRadio') controller: any;

  ishidden = true;
  dataToShow = [];
  @Input('hide') hide;
  @Input('panelName') panelName;
  @Input('datadnd1') datadnd1;
  @Output('dnd1DataChanged') dnd1DataChanged = new EventEmitter();
  @Output('addFakeAnswerDnd1') addFakeAnswerDnd1 = new EventEmitter();

  constructor(public modalService: ModalService) {}

  changedSomething(radioData, pos) {
   if (pos != null) {
     if (!radioData[pos].checked) {
       radioData[pos].checked = false;
     } else {
       let i = 0;
       const change = true;
       while (i < radioData.length) {
         radioData[i].checked = false;
         i++;
       }
       if (change) {
         radioData[pos].checked = true;
       }
     }
    }
    this.dnd1DataChanged.emit({radioData: radioData});
  }

  ngOnChanges(changes): void {
    if (typeof changes.hide != 'undefined') {
      this.ishidden = changes.hide.currentValue;
    } else if (typeof changes.datadnd1 != 'undefined') {
      this.dataToShow = changes.datadnd1.currentValue;
    }
  }

  addAnswer() {
    this.showPromptDNDname(data => {
      const id = new Date().getTime() + '_' + Math.floor((Math.random() * 1000) + 1);
      const object = {
        id: id,
        type: 'radio',
        value: data.fakeAnswer,
        checked: false
      };
      this.dataToShow.push(object);
      this.dnd1DataChanged.emit({radioData: this.dataToShow});
    });
  }

  ngOnInit(): void {
    $(this.controller.nativeElement).resizable({
      minHeight: 350,
      minWidth: 250
    });
    $(this.controller.nativeElement).draggable({handle: '.h1'});
  }

  // prompt
  showPromptDNDname(callback) {
    this.modalService.showPromptModal('Add radio answer', 'Enter value for checkbox answer!', 'fakeAnswer', 'Answer')
      .then(res => res && callback(res));
  }
}
