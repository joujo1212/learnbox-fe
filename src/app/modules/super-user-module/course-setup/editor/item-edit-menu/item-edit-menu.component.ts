/**
 * Created by pati on 03/04/2017.
 */
import {
  Component, trigger, state, style, transition, animate, Input, OnChanges, Output, EventEmitter
} from '@angular/core';

@Component({
  selector: 'item-edit-menu',
  templateUrl: '/item-edit-menu.component.html',
  styleUrls: ['/item-edit-menu.component.scss'],
  animations: [
    trigger('panel', [
      state('void', style({
        left: '-200px'
      })),
      state('*',   style({
        left: '0px'
      })),
      transition('void => *', animate('300ms linear')),
      transition('* => void', animate('300ms ease-out'))
    ])
  ]
})
export class ItemEditMenuComponent implements OnChanges {
  private itemToEdit: any = null;
  private lineHeight: any;

  @Output('lineHeight') lh = new EventEmitter();
  @Input('panelVisible') panelVisible;
  @Input('itemEditElement') itemEditElement;

  ngOnChanges(changes: any): void {
    if (typeof changes.itemEditElement != 'undefined') {
      if (changes.itemEditElement.currentValue != null) {
        this.itemToEdit = changes.itemEditElement.currentValue;
      }
    }
  }

  outputValues() {
      this.lh.emit({lineH: this.lineHeight});
  }
}
