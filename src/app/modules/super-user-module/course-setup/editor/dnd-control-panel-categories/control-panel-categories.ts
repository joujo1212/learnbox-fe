/**
 * Created by pati on 23/02/2017.
 */
import {Component, OnInit, ViewChild, Input, OnChanges, Output, EventEmitter} from '@angular/core';
import {ModalService} from '../../../../../services/modal.service';

declare var $: any;
@Component({
  selector: 'controllercat',
  templateUrl: '/control-panel-categories.html',
  styleUrls: ['/control-panel-categories.scss']
})
export class ControlPanelCategories implements OnInit, OnChanges {

  @ViewChild('controllerCat') controller: any;

  ishidden = true;
  categories = [{name: '', items: []}, {name: '', items: []}];

  @Input('hide') hide;
  @Input('panelName') panelName;
  @Input('datadnd1') datadnd1;
  @Output('dnd1DataChanged') dnd1DataChanged = new EventEmitter();
  @Output('addFakeAnswerDnd1') addFakeAnswerDnd1 = new EventEmitter();

  constructor(public modalService: ModalService) {}

  changedSomething(categories) {
    this.dnd1DataChanged.emit({categories: categories});
  }

  ngOnChanges(changes): void {
    if (typeof changes.hide != 'undefined') {
      this.ishidden = changes.hide.currentValue;
    } else if (typeof changes.datadnd1 != 'undefined') {
      this.categories = changes.datadnd1.currentValue;
    }
  }

  addItem(pos) {
    this.showPromptDNDname(data => {
      const id = new Date().getTime() + '_' + Math.floor((Math.random() * 1000) + 1);
      const object = {
        id: id,
        type: 'dragCategory',
        name: data.answer
      };
      const tmp = this.categories[pos];
      tmp.items.push(object);
      this.addFakeAnswerDnd1.emit({categories: this.categories});
    });
  }

  ngOnInit(): void {
    $(this.controller.nativeElement).resizable({
      minHeight: 350,
      minWidth: 500
    });
    $(this.controller.nativeElement).draggable({handle: '.h1'});
  }

  // prompt
  showPromptDNDname(callback) {
    this.modalService.showPromptModal('Set name for item', 'Enter name for answer!', 'answer', 'Answer')
      .then(res => res && callback(res));
  }
}
