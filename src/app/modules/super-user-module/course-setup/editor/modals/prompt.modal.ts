import { Component } from '@angular/core';

import { DialogRef, ModalComponent, CloseGuard } from 'angular2-modal';
import { BSModalContext } from 'angular2-modal/plugins/bootstrap';

export class PromptModalContext extends BSModalContext {
  public title: string;
  public desc: string;
  public input1Key: string;
  public input1Placeholder: string;
  public input2Key: string;
  public input2Placeholder: string;
  public canDismiss: boolean;
  public input1Value: string;
}

@Component({
  selector: 'app-prompt-modal',
  styleUrls: ['prompt.modal.css'],
  templateUrl: 'prompt.modal.html'
})
export class PromptModalComponent implements CloseGuard, ModalComponent<PromptModalContext> {
  context: PromptModalContext;

  public wrongAnswer: boolean;

  constructor(public dialog: DialogRef<PromptModalContext>) {
    this.context = dialog.context;
    dialog.setCloseGuard(this);
    this.wrongAnswer = true;
  }

  close(): void {
    this.dialog.close();
  }

  beforeDismiss(): boolean {
    return !this.context.canDismiss;
  }

  save(input1, input2) {
    this.dialog.close({
      [this.context.input1Key]: input1 ? input1.value : '',
      [this.context.input2Key]: input2 ? input2.value : ''
    });
  }
}
