/**
 * Created by pati on 17/02/2017.
 */
import {
  Component, OnInit, ViewChild, Renderer, ViewEncapsulation, EventEmitter, Output,
  ViewContainerRef
} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

import {EditorService} from '../../service/editor.service';
import {ModalService} from '../../../../services/modal.service';

declare let document: any;
declare let $: any;

@Component({
  selector: 'editor',
  templateUrl: '/editor.component.html',
  styleUrls: ['/editor.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    '(input)': 'changedSomething($event)',
    '(click)': 'testFunction($event)',
    '(change)': 'checkUnitsData($event)'
  }
})
export class EditorComponent implements OnInit {

  // controller variables
  private textAlignState = null;
  private isBold = false;
  private isItalic = false;
  // controller variables
  private editMode = true;
  private Fsize = 12;
  private isControllerHidden = true;
  private isControllerCatHidden = true;
  private isControllerCheckHidden = true;
  private isControllerRadioHidden = true;
  private isControllerGroupsHidden = true;
  private dnd1Data = [];
  private categoriesData = [{name: '', items: []}, {name: '', items: []}];
  private checkboxData = [];
  private radioData = [];
  private groupsData = [];
  private categoryNumber = 0;
  private selection = [1, 0, 0, 0, 0, 0, 0];
  private sel: any;
  private helpEnabled: boolean = false;
  private itemEditMenuVisible: boolean = false;
  private itemEditElement: any = null;
  private tempUUID: string;
  @ViewChild('canvas') canvas: any;
  @Output('closeEditor') closeEditor = new EventEmitter();

  constructor(private _sanitizer: DomSanitizer, private _editor: EditorService, private _renderer: Renderer,
              public modalService: ModalService, private vcRef: ViewContainerRef) {}

  close() {
    this.closeEditor.emit({});
  }

  changeEndpoint(e) {
    if (e.target.selectedIndex === 0) {
      console.log('dev');
      this._editor.setEndpoint('dev');
    } else {
      console.log('prod');
      this._editor.setEndpoint('prod');
    }
  }

  ngOnInit() {
    this._renderer.listen(this.canvas.nativeElement, 'doubleclick', (event) => {
      this.itemEditMenuVisible = true;
      this.itemEditElement = event.srcElement;
    });
    this._renderer.listen(this.canvas.nativeElement, 'click', event => {
      this.itemEditMenuVisible = false;
    });
  }

  addUnitComponent() {
    const idSelect = new Date().getTime() + '_' + Math.floor((Math.random() * 1000) + 1);

    this.secretInputs.push({
      type: 'secretInputFromWord',
      id: idSelect,
      value: 'cm'
    });

    this.pasteHtmlAtCaret(`<select id='${idSelect}' class='units-select'>
      </select>`);
    this.testProjection(this.canvas.nativeElement);
  }

  checkUnitsData($e) {
    if ($e.target.className == 'units-select') {
      let id = $e.target.id;
      let nextValue = $e.target.value;
      console.log(nextValue);
      let i = 0;
      while (i < this.secretInputs.length) {
        if (this.secretInputs[i].id == id) {
          this.secretInputs[i].value = nextValue;
          console.log('nasiel sa v secretoch');
          break;
        }
        i++;
      }
      console.log(this.secretInputs);
    }
  }

  testFunction($e) {
    // checkbox in text binding !
    if ($e.target.className == 'check') {
      const id = $e.target.id;
      const nextValue = !$e.target.parentElement.parentElement.firstChild.checked;
      let i = 0;
      while (i < this.textCheckboxes.length) {
        if (this.textCheckboxes[i].id == id) {
          this.textCheckboxes[i].checked = nextValue;
          break;
        }
        i++;
      }
    }
    let i = 0, tmp = [];
    while (i < this.dnd1Data.length) {
      tmp.push({id: this.dnd1Data[i].id, value: this.dnd1Data[i].value});
      i++;
    }
  }

  addQuestion() {
    this._editor.setUUID(this.tempUUID);
    if (this.selection[0]) {
      this._editor.addSecretQuestion(this.projection.changingThisBreaksApplicationSecurity, this.secretInputs).subscribe((res) => {
        console.log(res);
      });
    } else if (this.selection[1]) {
      this._editor.addDNDQuestion(this.projection.changingThisBreaksApplicationSecurity, this.dnd1Data).subscribe((res) => {
        console.log(res);
      });
    } else if (this.selection[2]) {
      console.log(this.categoriesData);
      this._editor.addCategoriesQuestion(this.projection.changingThisBreaksApplicationSecurity, this.categoriesData).subscribe((res) => {
        console.log(res);
      });
    } else if (this.selection[3]) {
      this._editor.addCheckboxQuestion(this.projection.changingThisBreaksApplicationSecurity, this.checkboxData).subscribe((res) => {
        console.log(res);
      });
    } else if (this.selection[4]) {
      this._editor.addRadioQuestion(this.projection.changingThisBreaksApplicationSecurity, this.radioData).subscribe((res) => {
        console.log(res);
      });
    } else if (this.selection[5]) {
      this._editor.addGroupsQuestion(this.projection.changingThisBreaksApplicationSecurity, this.groupsData).subscribe((res) => {
        console.log(res);
      });
    } else if (this.selection[6]) {
      this._editor.addCheckboxTextQuestion(this.projection.changingThisBreaksApplicationSecurity, this.textCheckboxes).subscribe((res) => {
        console.log(res);
      });
    } else {
    }
  }

  //projection
  transferData: Object = {id: 1, msg: 'Hello'};
  receivedData: Array<any> = [];

  transferDataSuccess($event: any) {
    console.log($event);
  }

  //projection
  dnd1changed($event) {
    document.getElementById($event.data.id).value = $event.data.name; // pamataj na inputoch su listenery
  }

  categoryChanged($event) {
    console.log($event.categories);
    this.categoriesData = [];
    this.categoriesData = $event.categories;
  }

  checkboxChanged($event) {
    console.log($event.checkboxData);
    this.checkboxData = [];
    this.checkboxData = $event.checkboxData;
  }

  radioChanged($event) {
    console.log($event.radioData);
    this.radioData = [];
    this.radioData = $event.radioData;
  }

  groupsChanged($event) {
    console.log($event.groupsData);
    this.groupsData = [];
    this.groupsData = $event.groupsData;
  }

  addFakeAnswerDnd1(object) {
    this.draggablesFromBottomToText = object;
    this.dnd1Data = this.draggablesFromBottomToText;
  }

  changedSomething($e) {
    if ($e.srcElement.localName == 'input' && $e.srcElement.className == 'secretInputFromWord') {
      let i = 0;
      while (i < this.secretInputs.length) {
        if (this.secretInputs[i].id == $e.srcElement.id) {
          this.secretInputs[i].value = $e.target.value;
          break;
        }
        i++;
      }
      console.log(this.secretInputs);
    } else if ($e.srcElement.localName == 'input' && $e.srcElement.className == 'dragFromBottomToText') {
      let i = 0;
      while (i < this.draggablesFromBottomToText.length) {
        if (this.draggablesFromBottomToText[i].id == $e.srcElement.id) {
          this.draggablesFromBottomToText[i].name = $e.target.value;
          break;
        }
        i++;
      }
      this.dnd1Data = this.draggablesFromBottomToText;
    }
  }

  setFront() {
    this.editor('fontSize', this.Fsize + 'px');
    this.testProjection(this.canvas.nativeElement);
  }

  setBold() {
    if (this.isBold) {
      this.editor('removeBold', '');
    } else {
      this.editor('bold', '');
    }
    this.testProjection(this.canvas.nativeElement);
  }

  setItalic() {
    if (this.isItalic) {
      this.editor('removeItalic', '');
    } else {
      this.editor('italic', '');
    }
    this.testProjection(this.canvas.nativeElement);
  }

  setTextStyle(style) {
    switch (style) {
      case 'left' :
        this.editor('TS-left', '');
        break;
      case 'center' :
        this.editor('TS-center', '');
        break;
      case 'right' :
        this.editor('TS-right', '');
        break;
      // TODO use in future
      // case 'justify' :
      //   this.editor("TS-justify","");
      //   break;
    }
    this.testProjection(this.canvas.nativeElement);
  }

  setCustomExercise(val) {
    if (this.selectionState.toString().length != 0 || val == 'checkbox-text') {
      switch (val) {
        case 'secret' :
          this.editor('secretInputFromWord', '');
          break;
        case 'dnd1' :
          this.editor('dnd1', '');
          break;
        case 'checkbox-text' :
          this.editor('checkbox-text', '');
          break;
        default:
          console.log('I cant set custom exercise');
      }
      this.testProjection(this.canvas.nativeElement);
    } else {
      this.showAlert('no-selection');
    }
  }

  private secretInputs: any = [];
  private textCheckboxes: any = [];
  private draggablesFromBottomToText: any = [];
  private selectionState: any = {};
  content: any = '';
  projection: any;

  saveSelectionState() {
    try {
      if (window.getSelection) {
        this.selectionState = window.getSelection();
      } else {
        this.selectionState = document.selection();
      }
    } catch (e) {
      console.log(e);
    }
    // console.log(this.selectionState);
    // console.log("sel state");
    this.editMenuShow(true);
  }

  editor(modifier, value) {
    let editorTool: any = {};

    switch (modifier) {
      //\\//\\ CUSTOMIZABLE PART ! //\\//\\
      case 'fontSize' :
        if (this.selectionState.toString().length != 0) {
          let inputFS = document.createElement('div');
          inputFS.setAttribute('style', 'font-size:' + value);
          inputFS.setAttribute('class', 'fontSize');

          editorTool = {
            modern: inputFS,
            IE1: '<span style="font-size:' + value + '">',
            IE2: '</span>'
          };
          replaceText(editorTool, false, this);
        } else {
          this.showAlert('no-selection');
        }

        break;
      case 'bold' :
        if (this.selectionState.toString().length != 0) {

          editorTool = {
            modern: document.createElement('b'),
            IE1: '<b>',
            IE2: '</b>'
          };
          replaceText(editorTool, false, this);
        } else {
          this.showAlert('no-selection');
        }
        break;

      case 'removeBold' :
        if (this.selectionState.toString().length != 0) {
          let sel_val = this.selectionState.toString();
          let parent = this.getSelectionContainerElement();
          if (sel_val == parent.innerText) {
            parent.parentNode.removeChild(parent);
          }
          this.pasteHtmlAtCaret("<span style='font-weight:normal;'>" + sel_val + "</span>");
          this.parentCheckOnClick();
        } else {
          this.showAlert('no-selection');
        }

        break;

      case 'italic' :
        if (this.selectionState.toString().length != 0) {

          editorTool = {
            modern: document.createElement('i'),
            IE1: '<i>',
            IE2: '</i>'
          };
          replaceText(editorTool, false, this);
        } else {
          this.showAlert('no-selection');
        }
        break;

      case 'removeItalic' :
        if (this.selectionState.toString().length != 0) {
          let sel_val = this.selectionState.toString();
          let parent = this.getSelectionContainerElement();
          if (sel_val == parent.innerText) {
            parent.parentNode.removeChild(parent);
          }
          this.pasteHtmlAtCaret("<span style='font-style:normal;'>" + sel_val + "</span>");
          this.parentCheckOnClick();
        } else {
          this.showAlert('no-selection');
        }

        break;

      case 'TS-left' :
        let sel_val = this.selectionState.toString();
        let parent = this.getSelectionContainerElement();
        console.log(parent.className);
        if (parent.className != 'cms_center_div') {
          this.pasteHtmlAtCaret("<div class='cms_center_div' style='text-align:left;width:100%;'>" + sel_val + "</div>");
        } else {
          if (this.textAlignState == 'left' && sel_val == parent.innerText) {
            parent.parentNode.removeChild(parent);
            this.pasteHtmlAtCaret(sel_val);
          } else {
            parent.style.textAlign = 'left';
          }
        }
        this.textAlignState = 'left';

        setTimeout(() => {
          this.parentCheckOnClick();
        }, 50);

        break;

      case 'TS-center' :
        let sel_val1 = this.selectionState.toString();
        let parent1 = this.getSelectionContainerElement();

        if (parent1.className != 'cms_center_div') {
          this.pasteHtmlAtCaret("<div class='cms_center_div' style='text-align:center;width:100%;'>" + sel_val1 + "</div>");
        } else {
          if (this.textAlignState == 'center' && sel_val1 == parent1.innerText) {
            parent1.parentNode.removeChild(parent1);
            this.pasteHtmlAtCaret(sel_val1);
          } else {
            parent1.style.textAlign = 'center';

          }
        }
        this.textAlignState = 'center';

        setTimeout(() => {
          this.parentCheckOnClick();
        }, 50);

        break;

      case 'TS-right' :
        let sel_val2 = this.selectionState.toString();
        let parent2 = this.getSelectionContainerElement();

        if (parent2.className != 'cms_center_div') {
          this.pasteHtmlAtCaret("<div class='cms_center_div' style='text-align:right;width:100%;'>" + sel_val2 + "</div>");
        } else {
          if (this.textAlignState == 'right' && sel_val2 == parent2.innerText) {
            parent2.parentNode.removeChild(parent2);
            this.pasteHtmlAtCaret(sel_val2);
          } else {
            parent2.style.textAlign = 'right';
          }
        }
        this.textAlignState = 'right';
        setTimeout(() => {
          this.parentCheckOnClick();
        }, 50);

        break;

      // case 'TS-justify' :
      //   if (this.selectionState.toString().length != 0) {
      //     let sel_val = this.selectionState.toString();
      //     let parent  = this.getSelectionContainerElement();
      //
      //     if(parent.className != 'cms_center_div'){
      //       this.pasteHtmlAtCaret("<div class='cms_center_div' style='text-align:justify;width:100%;'>" + sel_val + "</div>");
      //     }else {
      //       if(this.textAlignState == 'justify' && sel_val == parent.innerText){
      //         parent.parentNode.removeChild(parent);
      //         this.pasteHtmlAtCaret(sel_val);
      //       }else{
      //         if(this.textAlignState != 'justify'){
      //           parent.style.textStyle = 'justify';
      //         }else {
      //           let alert = this.alertCtrl.create({
      //             title: 'Removing text style!',
      //             subTitle: 'Unfortunately you have to select the whole block to remove text style!',
      //             buttons: ['OK']
      //           });
      //           alert.present();
      //         }
      //       }
      //     }
      //     setTimeout(() => {
      //       this.parentCheckOnClick();
      //     },50);
      //   } else {
      //     this.showAlert('no-selection');
      //   }
      //   break;


      case 'secretInputFromWord' :
        const idSecret = new Date().getTime() + '_' + Math.floor((Math.random() * 1000) + 1);
        const inputSecret = document.createElement('input');
        inputSecret.setAttribute('class', 'secretInputFromWord');
        inputSecret.setAttribute('id', idSecret);
        inputSecret.setAttribute('placeholder', 'secret');
        inputSecret.value = this.selectionState.toString();

        editorTool = {
          modern: inputSecret,
          IE1: "<input #input" + idSecret + " id='" + idSecret + "'" + "placeholder='secret' class='secretInputFromWord'/>",
          IE2: ''
        };
        let replacedTextSecret = replaceText(editorTool, 'secretInputFromWord', this);
        this.secretInputs.push({
          type: 'secretInputFromWord',
          id: idSecret,
          value: replacedTextSecret
        });

        // console.log('data co mi treba');
        // console.log(this.projection);
        // console.log(this.dnd1Data);
        break;

      case 'dnd1' :
        const idDnd1 = new Date().getTime() + '_' + Math.floor((Math.random() * 1000) + 1);
        const good_value = this.selectionState.toString();
        // console.log(good_value);
        this.pasteHtmlAtCaret("<input (click)='error' value='drop area' id='" + idDnd1 + "' class='dragFromBottomToText' dnd-droppable disabled (onDropSuccess)='transferDataSuccess($event)'/>");

        this.showPromptDNDname(name => {
          this.draggablesFromBottomToText.push({
            type: 'dragFromBottomToText',
            id: idDnd1,
            value: good_value,
            name: name
          });
          const createdInput = document.getElementById(idDnd1);
          createdInput.value = name;
          this.dnd1Data = this.draggablesFromBottomToText;
        });
        break;

      case 'checkbox-text' :
        let idCheckbox = new Date().getTime() + '_' + Math.floor((Math.random() * 1000) + 1);
        this.pasteHtmlAtCaret("&nbsp;<div contenteditable='false'  class='checkbox-text checkbox brown'>" + "<label><input value ='false' type='checkbox'  ><span class='checkbox-material'><span id='" + idCheckbox + "' class='check'></span></span></label></div>&nbsp;");
        this.textCheckboxes.push({
          id: idCheckbox,
          type: 'checkbox-text',
          value: null,
          checked: false
        });
        console.log(this.textCheckboxes);
        break;
      default :
        console.log('something went wrong! unknown modifier');
    }

    //\\//\\ CUSTOMIZABLE PART - end ! //\\//\\


    //\\//\\ MAIN LOGIC FUNCTIONS do not change anything !!! //\\//\\
    // surrond selection with html tags
    // editorTool input is created HTML tag for modern browsers and tag in string for IE
    // operation type

    function replaceText(editorTool, operationType, thisRef) {
      if (operationType == false) {
        console.log('NO OP');
        if (thisRef.selectionState) {
          // not IE
          const selObj = thisRef.selectionState;
          const selRange = selObj.getRangeAt(0);

          const newElement = editorTool.modern;
          const documentFragment = selRange.extractContents();
          newElement.appendChild(documentFragment);
          selRange.insertNode(newElement);
          selObj.removeAllRanges();
        } else if (document.selection && document.selection.createRange && document.selection.type != 'None') {
          // IE
          const range = thisRef.selectionState.createRange();
          const selectedText = range.htmlText;
          const newText = editorTool.IE1 + selectedText + editorTool.IE2;
          thisRef.selectionState.createRange().pasteHTML(newText);
        }
      } else if (operationType == 'secretInputFromWord') {
        console.log(thisRef.selectionState);
        const returnValue = thisRef.selectionState.toString();
        if (thisRef.selectionState) {
          // not IE
          const selObj = thisRef.selectionState;
          const selRange = selObj.getRangeAt(0);
          const newElement = editorTool.modern;
          selRange.extractContents();
          selRange.insertNode(newElement);
          selObj.removeAllRanges();
        } else if (document.selection && document.selection.createRange && document.selection.type != 'None') {
          // IE
          const range = thisRef.selectionState.createRange();
          range.htmlText;
          const newText = editorTool.IE1;
          thisRef.selectionState.createRange().pasteHTML(newText);
        }
        return returnValue;
      } else if (operationType == 'dragFromBottomToText') {
        // TODO delete this because its not used
        const returnValue = thisRef.selectionState.toString();
        if (thisRef.selectionState) {
          // not IE
          const selObj = thisRef.selectionState;
          const selRange = selObj.getRangeAt(0);
          const newElement = editorTool.modern;
          selRange.extractContents();
          selRange.insertNode(newElement);
          selObj.removeAllRanges();
        } else if (document.selection && document.selection.createRange && document.selection.type != 'None') {
          // IE
          let range = thisRef.selectionState.createRange();
          range.htmlText;
          let newText = editorTool.IE1;
          thisRef.selectionState.createRange().pasteHTML(newText);
        }
        return returnValue;
      }
    }
  }

  // vlozi html na poziciu kurzora
  // document.getElementById('test').focus(); pasteHtmlAtCaret('<b>INSERTED</b>'); <= usage
  pasteHtmlAtCaret(html) {
    let sel, range;
    if (window.getSelection) {
      // IE9 and non-IE
      sel = window.getSelection();
      if (sel.getRangeAt && sel.rangeCount) {
        range = sel.getRangeAt(0);
        range.deleteContents();
        const el = document.createElement('div');
        el.innerHTML = html;
        const frag = document.createDocumentFragment();
        let node;
        let lastNode;
        while ((node = el.firstChild)) {
          lastNode = frag.appendChild(node);
        }
        range.insertNode(frag);
        if (lastNode) {
          range = range.cloneRange();
          range.setStartAfter(lastNode);
          range.collapse(true);
          sel.removeAllRanges();
          sel.addRange(range);
        }
      }
    } else if (document.selection && document.selection.type != 'Control') {
      // IE < 9
      document.selection.createRange().pasteHTML(html);
    }
  }

  saveSelection() {
    if (window.getSelection) {
      this.sel = window.getSelection();
      if (this.sel.getRangeAt && this.sel.rangeCount) {
        return this.sel.getRangeAt(0);
      }
    } else if (document.selection && document.selection.createRange) {
      return document.selection.createRange();
    }
    return null;
  }

  restoreSelection(range) {
    if (range) {
      if (window.getSelection) {
        this.sel = window.getSelection();
        this.sel.removeAllRanges();
        this.sel.addRange(range);
      } else if (document.selection && range.select) {
        range.select();
      }
    }
  }

  getSelectionContainerElement() {
    let range, sel, container;
    if (document.selection && document.selection.createRange) {
      // IE case
      range = document.selection.createRange();
      return range.parentElement();
    } else if (window.getSelection) {
      sel = window.getSelection();
      if (sel.getRangeAt) {
        if (sel.rangeCount > 0) {
          range = sel.getRangeAt(0);
        }
      } else {
        // Old WebKit selection object has no getRangeAt, so
        // create a range from other selection properties
        range = document.createRange();
        range.setStart(sel.anchorNode, sel.anchorOffset);
        range.setEnd(sel.focusNode, sel.focusOffset);

        // Handle the case when the selection was selected backwards (from the end to the start in the document)
        if (range.collapsed !== sel.isCollapsed) {
          range.setStart(sel.focusNode, sel.focusOffset);
          range.setEnd(sel.anchorNode, sel.anchorOffset);
        }
      }

      if (range) {
        container = range.commonAncestorContainer;
        // Check if the container is a text node and return its parent if so
        return container.nodeType === 3 ? container.parentNode : container;
      }
    }
  }

  changeLineHeightOfParent($e: any, selHolder) {
    console.log($e.lineH);
    const parent = this.getSelectionContainerElement();
    this.restoreSelection(selHolder);
    if (parent.id == 'canvas') {
      const sel_val = this.selectionState.toString();
      this.pasteHtmlAtCaret("<div style='line-height:" + $e.lineH + "px;display:inline-block;vertical-align: top;'>" + sel_val + "</div>");
    }
    this._renderer.setElementStyle(parent, 'lineHeight', $e.lineH + 'px');
    this._renderer.setElementStyle(parent, 'display', 'inline-block');
    this._renderer.setElementStyle(parent, 'verticalAlign', 'top');
  }

  editMenuShow(val) {
    // this.setFront();
    this.itemEditMenuVisible = val;
  }

  parentCheckOnClick() {
    const parent = this.getSelectionContainerElement();
    if (typeof parent != 'undefined') {
      const parentTagName = parent.tagName;
      const parentClassName = parent.className;

      // BOLD LOGIC
      if (parentTagName == 'B') {
        this.isBold = true;
      } else {
        this.isBold = false;
      }
      // BOLD LOGIC

      // ITALIC LOGIC
      if (parentTagName == 'I') {
        this.isItalic = true;
        console.log(this.isBold);
      } else {
        this.isItalic = false;

      }
      // ITALIC LOGIC

      // text style
      if (parentClassName == 'cms_center_div') {
        this.textAlignState = parent.style.textAlign;
      } else {
        this.textAlignState = null;
      }
      // text style
    }
  }
  //\\//\\ MAIN LOGIC - end//\\//\\

  // other functions

  // drag events
  allowDrop(ev) {
    ev.preventDefault();
  }

  drag(ev) {
    ev.dataTransfer.setData('text', ev.target.id);
  }

  drop(ev) {
    ev.preventDefault();
    const data = ev.dataTransfer.getData('text');
    ev.target.appendChild(document.getElementById(data));
  }

  // drag events
  // images
  src: string = '';
  resizeOptions: any = {
    resizeMaxHeight: 350,
    resizeMaxWidth: 350
  };

  selected(imageResult) {
    console.log(imageResult);
    this.src = imageResult.resized
      && imageResult.resized.dataURL
      || imageResult.dataURL;
    this.canvas.nativeElement.focus();
    const idIMG = new Date().getTime() + '_' + Math.floor((Math.random() * 1000) + 1);

    this.pasteHtmlAtCaret("<img #image" + idIMG + " src='" + this.src + "'/>");
    this.testProjection(this.canvas.nativeElement);
  }

  // prompt
  showPromptDNDname(callback) {
    const number = this.dnd1Data.length + 1;
    this.modalService.showPromptModal('Set name', 'Enter UNIQUE name for this new drop area you\'ve created!', 'name', 'Name', '', '', false, 'Drop ' + number)
      .then(res => res && callback(res.name));
  }

  // ionic alerts could be replaced with any other alerts
  showAlert(select) {
    switch (select) {
      case 'secret' :
        if (this.editMode) {
          if (this.helpEnabled) {
            this.modalService.showSimpleModal('Create secret lection!', 'Please select the text and click to transform selected text to exercise!', 'success', this.vcRef);
          }
          this.isControllerHidden = true;
          this.isControllerCatHidden = true;
          this.isControllerCheckHidden = true;
          this.isControllerRadioHidden = true;
          this.isControllerGroupsHidden = true;
        } else {
          if (this.helpEnabled) {
            this.modalService.showSimpleModal('Projection of secret lection!', 'This is how it looks like!', 'success', this.vcRef);
          }
        }
        selectedItemChange(0, this);

        break;
      case 'dnd1' :

        if (this.editMode) {
          if (this.helpEnabled) {
            this.modalService.showSimpleModal('Create drag and drop lection!', 'Please select the text and click to transform selected text to exercise! Text will be saved as option and replaced by drop area, don\'t forget you can manually add fake answers in control panel!', 'success', this.vcRef);
          }
          this.isControllerHidden = false;
          this.isControllerCatHidden = true;
          this.isControllerCheckHidden = true;
          this.isControllerRadioHidden = true;
          this.isControllerGroupsHidden = true;
        } else {
          if (this.helpEnabled) {
            this.modalService.showSimpleModal('Projection of drag and drop lection!', 'This is how it looks like!', 'success', this.vcRef);
          }
        }
        selectedItemChange(1, this);
        break;

      case 'dndCategories' :

        if (this.editMode) {
          if (this.helpEnabled) {
            this.modalService.showSimpleModal('Create drag and drop categories lection', 'Fill category names and items!', 'success', this.vcRef);
          }
          this.isControllerHidden = true;
          this.isControllerCatHidden = false;
          this.isControllerCheckHidden = true;
          this.isControllerRadioHidden = true;
          this.isControllerGroupsHidden = true;
        } else {
          if (this.helpEnabled) {
            this.modalService.showSimpleModal('Projection of drag and drop categories lection', 'This is how it looks like!', 'success', this.vcRef);
          }
        }
        selectedItemChange(2, this);

        break;

      case 'checkboxes' :

        if (this.editMode) {
          if (this.helpEnabled) {
            this.modalService.showSimpleModal('Create multiple answer lection', 'Create answers and check the right values!', 'success', this.vcRef);
          }
          this.isControllerHidden = true;
          this.isControllerCatHidden = true;
          this.isControllerCheckHidden = false;
          this.isControllerRadioHidden = true;
          this.isControllerGroupsHidden = true;
        } else {
          if (this.helpEnabled) {
            this.modalService.showSimpleModal('Projection of multiple answer lection', 'This is how it looks like!', 'success', this.vcRef);
          }
        }
        selectedItemChange(3, this);

        break;

      case 'radio':

        if (this.editMode) {
          if (this.helpEnabled) {
            this.modalService.showSimpleModal('Create single answer lection', 'Create answers and choose the right value!', 'success', this.vcRef);
          }
          this.isControllerHidden = true;
          this.isControllerCatHidden = true;
          this.isControllerCheckHidden = true;
          this.isControllerRadioHidden = false;
          this.isControllerGroupsHidden = true;
        } else {
          if (this.helpEnabled) {
            this.modalService.showSimpleModal('Projection of single answer lection', 'This is how it looks like!', 'success', this.vcRef);
          }
        }
        selectedItemChange(4, this);

        break;

      case 'groups':

        if (this.editMode) {
          if (this.helpEnabled) {
            this.modalService.showSimpleModal('Create groups leaction', 'Create answer pairs!', 'success', this.vcRef);
          }
          this.isControllerHidden = true;
          this.isControllerCatHidden = true;
          this.isControllerCheckHidden = true;
          this.isControllerRadioHidden = true;
          this.isControllerGroupsHidden = false;
        } else {
          if (this.helpEnabled) {
            this.modalService.showSimpleModal('Projection of groups leaction', 'This is how it looks like!', 'success', this.vcRef);
          }
        }
        selectedItemChange(5, this);
        break;

      case 'checkbox-text':
        console.log('dostal som sa sem');
        if (this.editMode) {
          if (this.helpEnabled) {
            this.modalService.showSimpleModal('Create inline checkboxes', 'Create while writing!', 'success', this.vcRef);
          }
          this.isControllerHidden = true;
          this.isControllerCatHidden = true;
          this.isControllerCheckHidden = true;
          this.isControllerRadioHidden = true;
          this.isControllerGroupsHidden = true;

        } else {
          if (this.helpEnabled) {
            this.modalService.showSimpleModal('Projection of groups leaction', 'This is how it looks like!', 'success', this.vcRef);
          }
        }
        selectedItemChange(6, this);
        break;

      case 'no-selection':
        this.modalService.showSimpleModal('Nothing is selected!', 'Please select text to proceed!', 'success', this.vcRef);
        break;

      default:
        console.log('I cant set custom exercise');
    }


    function selectedItemChange(value, thisRef) {
      let i = 0;

      while (i < thisRef.selection.length) {
        thisRef.selection[i] = 0;
        i++;
      }
      thisRef.selection[value] = 1;
      thisRef.categoryNumber = value;
      console.log(thisRef.selection);
    }
  }

  helpChange() {
    this.helpEnabled = !this.helpEnabled;
    if (this.helpEnabled) {
      this.modalService.showSimpleModal('Help enabled', 'Modals will inform you what\'s going on when you click on something.', 'success', this.vcRef);
    }
  }

  showProjection(bool) {
    if (bool) {
      this.editMode = false;
      this.isControllerHidden = true;
      this.isControllerCatHidden = true;
      this.isControllerCheckHidden = true;
      this.isControllerRadioHidden = true;
      this.isControllerGroupsHidden = true;
      if (this.helpEnabled) {
        let name: string = '';
        switch (this.categoryNumber) {
          case 0:
            name = 'secret';
            break;
          case 1:
            name = 'drag and drop';
            break;
          case 2:
            name = 'drag and drop categories';
            break;
          case 3:
            name = 'multiple answer';
            break;
          case 4:
            name = 'single answer';
            break;
          case 5:
            name = 'groups';
            break;
          case 6:
            name = 'inline checkboxes';
            break;
          default:
            break;
        }
        this.modalService.showSimpleModal('Projection', 'Projection of ' + name + ' lection!', 'success', this.vcRef);
      }
    } else {
      this.editMode = true;
      if (this.selection[1] == 1) {
        this.isControllerHidden = false;
      } else if (this.selection[2] == 1) {
        this.isControllerCatHidden = false;
      } else if (this.selection[3] == 1) {
        this.isControllerCheckHidden = false;
      } else if (this.selection[4] == 1) {
        this.isControllerRadioHidden = false;
      } else if (this.selection[5] == 1) {
        this.isControllerGroupsHidden = false;
      }
    }
  }

  // test projection + start div
  testProjection(canvas) {
    this.projection = this._sanitizer.bypassSecurityTrustHtml(canvas.innerHTML);
    // adding div when parent is canvas ! .. line height
    // const parent = this.getSelectionContainerElement();
    // this._renderer.setElementStyle(parent, 'lineHeight', '22px');


    // if(parent.id == 'canvas'){
    //   this.pasteHtmlAtCaret('<div>' +'</div>');
    //   console.log(this.projection);

    // }

  }
}

// some commented code / It can be deleted later when cms main stuff will be ready and tested


// // replace selection and return selected VALUE
// function replaceSelection(html, selectInserted) {
//   let sel, range, fragment, returnValue = window.getSelection().toString();
//
//   if (typeof window.getSelection != "undefined") {
//     sel = window.getSelection();
//
//     if (sel.getRangeAt && sel.rangeCount) {
//       range = window.getSelection().getRangeAt(0);
//       range.deleteContents();
//
//       if (range.createContextualFragment) {
//         fragment = range.createContextualFragment(html);
//       } else {
//         let div = document.createElement("div"), child;
//         div.innerHTML = html;
//         fragment = document.createDocumentFragment();
//         while ((child = div.firstChild)) {
//           fragment.appendChild(child);
//         }
//       }
//       let firstInsertedNode = fragment.firstChild;
//       let lastInsertedNode = fragment.lastChild;
//       range.insertNode(fragment);
//       if (selectInserted) {
//         if (firstInsertedNode) {
//           range.setStartBefore(firstInsertedNode);
//           range.setEndAfter(lastInsertedNode);
//         }
//         sel.removeAllRanges();
//         sel.addRange(range);
//       }
//     }
//   } else if (document.selection && document.selection.type != "Control") {
//     range = document.selection.createRange();
//     range.pasteHTML(html);
//   }
//   return returnValue;
// }


// replaceSelectionWithHtml("<b>REPLACEMENT HTML</b>");
// test function
// function replaceSelectionWithHtml(html) {
//   let range;
//   if (window.getSelection && window.getSelection().getRangeAt) {
//     range = window.getSelection().getRangeAt(0);
//     range.deleteContents();
//     let div = document.createElement("div");
//     div.innerHTML = html;
//     let frag = document.createDocumentFragment(), child;
//     while ((child = div.firstChild)) {
//       frag.appendChild(child);
//     }
//     range.insertNode(frag);
//   } else if (document.selection && document.selection.createRange) {
//     range = document.selection.createRange();
//     range.pasteHTML(html);
//   }
// }
