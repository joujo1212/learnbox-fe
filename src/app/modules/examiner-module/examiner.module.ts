import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {ExaminerRoutingModule} from './examiner-routing.module';
import {ExaminerComponent} from './components/examiner/examiner.component';
import {SharedModule} from '../../shared/shared.module';
import {MaterialModule} from '@angular/material';
import {FlexLayoutModule} from '@angular/flex-layout';
import {ProgressComponent} from './components/examiner/progress/progress.component';
import {ReviewComponent} from './components/examiner/review/review.component';
import {DashboardComponent} from './components/examiner/dashboard/dashboard.component';
import {ClassProgressComponent} from './components/examiner/progress/class-progress/class-progress.component';
import {ViewResultsExaminerComponent} from './components/examiner/review/view-results/view-results-examiner.component';
//import { ExaminerComponent } from "./examiner/examiner.component";
import { ExaminerDashboardComponent } from "./examiner-dashboard/examiner-dashboard.component";
import { ModalModule } from 'angular2-modal';
import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';
import { DndModule } from "ng2-dnd";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ExaminerRoutingModule,
    SharedModule,
    MaterialModule.forRoot(),
    FlexLayoutModule,
    ModalModule.forRoot(),
    BootstrapModalModule,
    DndModule.forRoot()
  ],
  declarations: [
    ExaminerComponent,
    ProgressComponent,
    ReviewComponent,
    DashboardComponent,
    ClassProgressComponent,
    ViewResultsExaminerComponent,
    ExaminerDashboardComponent
  ],
  exports: []

})
export class ExaminerModule {
}
