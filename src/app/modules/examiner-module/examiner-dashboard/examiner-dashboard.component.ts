import { Component, OnChanges, Input, Output } from '@angular/core';

@Component({
  selector: 'examiner-dashboard',
  templateUrl: 'examiner-dashboard.component.html',
  styleUrls: ['examiner-dashboard.component.css']
})
export class ExaminerDashboardComponent{

  examinerCourses: any[] = [];
  examinerClasses: any[] = [];

  ngOnInit() {
    this.examinerCourses.push({
      img: "/assets/maths-course.png"
    });
    this.examinerCourses.push({
      img: "/assets/english-course.png"
    });

    this.examinerClasses.push({
      teacherName: "L DAVIES",
      subjectName: "Maths",
      note: "June Enrolment"
    });
    this.examinerClasses.push({
      teacherName: "L DAVIES",
      subjectName: "Maths",
      note: "September Enrolment"
    });
    this.examinerClasses.push({
      teacherName: "P JOHNSON",
      subjectName: "English",
      note: "Spring Term"
    });
    this.examinerClasses.push({
      teacherName: "L DAVIES",
      subjectName: "Maths",
      note: "June Enrolment"
    });
    this.examinerClasses.push({
      teacherName: "L DAVIES",
      subjectName: "Maths",
      note: "September Enrolment"
    });
    this.examinerClasses.push({
      teacherName: "P JOHNSON",
      subjectName: "English",
      note: "Spring Term"
    });
  }

  showMoreLessClasses(): void {

  }
}
