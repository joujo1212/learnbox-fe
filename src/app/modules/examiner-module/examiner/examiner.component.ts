import { Component } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'examiner-module',
  templateUrl: 'examiner.component.html',
  styleUrls: ['examiner.component.css']
})
export class ExaminerComponent{
  constructor(
    public router: Router
  ){
  };

}
