
import {Routes, RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {ExaminerComponent} from './components/examiner/examiner.component';
import {ProgressComponent} from './components/examiner/progress/progress.component';
import {ReviewComponent} from './components/examiner/review/review.component';
import {DashboardComponent} from './components/examiner/dashboard/dashboard.component';
import {ClassProgressComponent} from './components/examiner/progress/class-progress/class-progress.component';
import {ViewResultsExaminerComponent} from './components/examiner/review/view-results/view-results-examiner.component';

//TODO check Matus files
import { ExaminerDashboardComponent } from './examiner-dashboard/examiner-dashboard.component';
import {AuthGuard} from '../../services/auth-guard';

const examinerRoutes: Routes = [
  {
    path: 'examiner',
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    component: ExaminerComponent,
    children: [
      {
        path: '',
        component: DashboardComponent

      },
      {
        path: 'progress',
        component: ProgressComponent,
        children: [
          {
            path: '',
            redirectTo: 'class',
            pathMatch: 'full'
          },
          {
            path: 'class',
            component: ClassProgressComponent
          }
        ]
      },
      {
        path: 'review',
        component: ReviewComponent,
        children: [
          {
            path: '',
            redirectTo: 'view-results',
            pathMatch: 'full'
          },
          {
            path: 'view-results',
            component: ViewResultsExaminerComponent
          }
        ]
      }, {
        path: 'dashboard',
        component: ExaminerDashboardComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(examinerRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class ExaminerRoutingModule {
}
