/**
 * Created by pati on 30/04/2017.
 */
import {Component, ViewEncapsulation} from '@angular/core';

@Component({
    selector: 'view-results-examiner',
    templateUrl: 'view-results-examiner.component.html',
    styleUrls: ['view-results-examiner.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ViewResultsExaminerComponent {

}
