/**
 * Created by pati on 29/04/2017.
 */
import {Component} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'examiner',
  templateUrl: 'examiner.component.html',
  styleUrls: ['examiner.component.scss']
})
export class ExaminerComponent {

  constructor(public router: Router) {
  };

  getProgressButtonStatus() {
    return this.router.isActive('/examiner/progress', false)
      || this.router.isActive('/examiner/progress/class', false)
      || this.router.isActive('/examiner/progress/student', false);
  }
}
