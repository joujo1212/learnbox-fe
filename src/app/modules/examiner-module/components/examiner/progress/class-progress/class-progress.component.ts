/**
 * Created by pati on 29/04/2017
 */
import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {getClassLearnerProgress} from '../../../../../../mocks/learner-admin.mocks';

@Component({
  selector: 'class-progress-component',
  templateUrl: 'class-progress.component.html',
  styleUrls: ['class-progress.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ClassProgressComponent {
  private tableType = 'class-learner-progress';
  private data: any[];
  private requestOptions = {
    className: null,
    averageProgress: null,
    averageScore: null,
  };
  constructor() {
    this.data = getClassLearnerProgress();
  }




}
