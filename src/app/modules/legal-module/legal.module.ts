import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MaterialModule } from "@angular/material";
import { LegalRoutingModule } from "./legal-routing.module";
import { FlexLayoutModule } from "@angular/flex-layout";
import { SharedModule } from "../../shared/shared.module"
import { DndModule } from "ng2-dnd";
import { LegalComponent } from "./legal/legal.component";
import {CookiesComponent} from './cookies/cookies.component';
import {PrivacyComponent} from './privacy/privacy.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule.forRoot(),
    LegalRoutingModule,
    FlexLayoutModule,
    SharedModule,
    DndModule.forRoot()

  ],
  declarations: [
    LegalComponent,
    PrivacyComponent,
    CookiesComponent
  ]

})
export class LegalModule {}
