import { Component } from '@angular/core';

@Component({
  selector: 'privacy-policy',
  template: '<generic-text-page [innerHTML]="privacyHTML" [masterTitle]="\'Privacy Policy\'"></generic-text-page>'
})
export class PrivacyComponent {

  public privacyHTML = '<p>Learn Box Limited (“We”) are committed to protecting and respecting your privacy.</p> \
    <p>This policy (together with our terms of use and any other documents referred to on it) sets out the basis on which any personal data we collect from you, or that you provide to us, will be processed by us. Please read the following carefully to understand our views and practices regarding your personal data and how we will treat it. By visiting <a href="/">our site</a>, you are accepting and consenting to the practices described in this policy.</p> \
    <p>Please read this Privacy Policy carefully – by using the Website and the Online Courses and Content (whether as a Visitor or a Learner), you agree to its terms (including as amended from time to time). If, for any reason, you do not agree to the terms of this Privacy Policy, please stop using the Website and the Online Courses and Content immediately.</p> \
    <p>For the purpose of the Data Protection Act 1998 (the Act), the data controller is Mr. Anthony Nicholas of Pemberton House, Stafford Court, Stafford Park 1, Telford, Shropshire, TF3 3BD.</p> \
    <h3>1. INFORMATION WE COLLECT FROM YOU</h3> \
    <p>We will collect and process the following data about you:</p> \
    <p>1.1 Collection of Information</p> \
    <p>a. When you access the Website via any means, use the Online Courses and Content, register or post notes, assignments or other material or provide comments, we may collect, store and use certain personal information that you voluntarily disclosed to us. We may also ask you for information when you report a problem with the Website or the Online Courses and Content.</p> \
    <p>b. We may also collect data relating to your visits to the Website that cannot identify you but records your use of our Website and Online Courses and Content including, for example, details of how long you have used the Website and the Online Courses and Content for.</p> \
    <p>c. We may also collect your computer’s IP address in order to help us tailor the service to your location.</p> \
    <p>d. If you elect to verify your identity through our Website, we will receive, process and store your personal information. In completing the verification process, your information and identity documentation will be shared with the third party identity verification provider who may store your information for a maximum of 5 days for the sole purpose of resolving user support issues. Upon successful verification of your identity, we will store only the name given on your identification documentation; if you participate in a credit bearing course or Program, we may share that information with the relevant course provider or accrediting organisation to allow you to receive credit.</p> \
    <p>e. Finally, we may receive information about you from third parties (such as credit reference agencies) who are legally entitled to disclose that information.</p> \
    <p>1.2 Information we receive from other sources. This is information we receive about you if you use any of the other websites we operate or the other services we provide. [In this case we will have informed you when we collected that data if we intend to share those data internally and combine it with data collected on this site. We will also have told you for what purpose we will share and combine your data]. We are working closely with third parties (including, for example, business partners, sub-contractors in technical, payment and delivery services, advertising networks, analytics providers, search information providers, credit reference agencies). We will notify you when we receive information about you from them and the purposes for which we intend to use that information.</p> \
    <h3>2. USES OF YOUR INFORMATION</h3> \
    <p>2.1 We use information held about you in the following ways:</p> \
    <p>2.1.1 By accessing the Website via any means, use the Online Courses and Content, registering or posting notes, assignments or other material, or providing comments, you agree that your personal information may be collected, stored, used and shared by us and/or our other partners, and course and content providers chosen by us for any of the following purposes:</p> \
    <p>(a) to provide, maintain, protect and improve the quality of the Website and the Online Courses and Content we offer, including by conducting anonymised market research, and to protect us and our users;</p> \
    <p>(b) to provide you with a personalised browsing experience when using the Website;</p> \
    <p>(c) to fulfil any contractual agreements between you and us (for example, when you register as a Learner in relation to the Online Courses and Content);</p> \
    <p>(d) to send you details of other products and services which we think may interest you including via relevant social media channels, unless you opt out via your personal settings;</p> \
    <p>(e) to sell sponsorship on the Website;</p> \
    <p>(f) to manage your Learner account that you hold with us;</p> \
    <p>(g) to allow you to use some features of our Website or Online Courses and Content;</p> \
    <p>(h) to send you email notifications and updates about the Website or Online Courses and Content you are enrolled on; and</p> \
    <p>(i) to comply with legal and regulatory requirements.</p> \
    <p>(j) to contact you occasionally in order to invite you to share your opinions and experiences of LearnBox with us.</p> \
    <p>(k) to share your information with third parties where necessary in order to provide services that you have requested including, for example, confirmation of the Courses you have participated in and/or the award you have achieved as a result (if applicable) for accreditation or CPD purposes, or otherwise.</p> \
    <p>2.1.2 We may use your email address to send you course notices and updates about our services. You can always control your email preferences.</p> \
    <p>2.1.3 If you object to your information being collected and used as outlined in this Privacy Policy please cease using the Website immediately.</p> \
    <p>2.2 Information we collect about you.</p> \
    <p>2.2.1 We will use this information:</p> \
    <p>a. to administer our site and for internal operations, including troubleshooting, data analysis, testing, research, statistical and survey purposes;</p> \
    <p>b. to improve our site to ensure that content is presented in the most effective manner for you and for your computer;</p> \
    <p>c. to allow you to participate in interactive features of our service, when you choose to do so;</p> \
    <p>d. as part of our efforts to keep our site safe and secure;</p> \
    <p>e. to measure or understand the effectiveness of advertising we serve to you and others, and to deliver relevant advertising to you;</p> \
    <p>f. to make suggestions and recommendations to you and other users of our site about goods or services that may interest you or them.</p> \
    <p>2.3 Information we receive from other sources.</p> \
    <p>2.3.1 We will combine this information with information you give to us and information we collect about you. We will use this information and the combined information for the purposes set out above (depending on the types of information we receive).</p> \
    <h3>3. DISCLOSURE OF YOUR INFORMATION</h3> \
    <p>3.1 You agree that we have the right to share your personal information with:</p> \
    <p>a. Any member of our group, which means our subsidiaries, our ultimate holding company and its subsidiaries, as defined in section 1159 of the UK Companies Act 2006;</p> \
    <p>b. Selected third parties including:</p> \
    <p>• business partners, suppliers and sub-contractors for the performance of any contract we enter into with;</p> \
    <p>• if we are under a duty to disclose or share your information in order to comply with any legal obligation, or in order to enforce or apply our Terms and other agreements; or to protect our rights, property, or safety, our users, or others. This includes exchanging information with other companies and organisations for the purposes of fraud protection and credit risk reduction.</p> \
    <p>• if we sell or buy any business or assets, in which case we may disclose your information to the prospective seller or buyer of such business or assets, provided that they continue to use your information substantially in accordance with the terms of this Privacy Policy;</p> \
    <p>• analytics and search engine providers that assist us in the improvement and optimisation of our site;</p> \
    <p>• In the event that we sell or buy any business or assets, in which case we will disclose your personal data to the prospective seller or buyer of such business or assets.</p> \
    <p>3.2 When you register as a Learner for a particular Online Course or Content, you agree that your personal data may be shared with the specific Partner Institution providing such Online Courses or Content.</p> \
    <p>3.3 When you place an order through us for products or services to be fulfilled by third parties in connection with a Course (such as externally administered assessments) any information you provide to us in connection with your order may be shared with the third party provider, for the purpose of processing and fulfilling your order.</p> \
    <h3>4. STORAGE OF DATA: WHERE WE STORE YOUR PERSONAL DATA</h3> \
    <p>4.1 All information is stored on our secure servers. Where we have given you (or where you have chosen) a password which enables you to access certain parts of our site, you are responsible for keeping this password confidential. We ask you not to share a password with anyone.</p> \
    <p>4.2 The transmission of information via the internet is not completely secure. Although we will do our best to protect your personal data, we cannot guarantee the security of your data transmitted to our site; any transmission is at your own risk. Once we have received your information, we will use strict procedures and security features to try to prevent unauthorised access.</p> \
    <p>4.3 The data that we collect from you will be transferred to, and stored at, a destination outside the European Economic Area (”EEA”). It will also be processed by staff operating outside the EEA who work for us or for one of our suppliers. This includes staff engaged in, among other things, the fulfilment of your order, the processing of your payment details and the provision of support services. By submitting your personal data, you agree to this transfer, storing or processing. However, whilst we have used our best efforts to ensure the security of your data, please be aware that we cannot guarantee the security of information transmitted over the Internet.</p> \
    <h3>5. USE OF THE WEBSITE AND ONLINE COURSES AND CONTENT</h3> \
    <p>5.1 This Privacy Policy applies only to your use of the Website and Online Courses and Content. We are not responsible for any information gathered by Partner Institutions who provide the Online Courses and Content or other linking sites found on the Website and you should consult those other parties’ privacy policies as appropriate and applicable.</p> \
    <h3>6. LEGAL NOTICE</h3> \
    <p>6.1 We will at all times only collect and process your personal information in accordance with the Data Protection Act 1998, the Privacy and Electronic Communications (EC Directive) Regulations 2003 and any other applicable data protection legislation.</p> \
    <h3>7. YOUR RIGHTS</h3> \
    <p>7.1 You have the right to ask us not to process your personal data for marketing purposes. We will usually inform you (before collecting your data) if we intend to use your data for such purposes or if we intend to disclose your information to any third party for such purposes. You can exercise your right to prevent such processing by checking certain boxes on the forms we use to collect your data. You can also exercise the right at any time by contacting us at <a href="mailto:Lucy.Dunleavy@boxcts.com">Lucy.Dunleavy@boxcts.com</a>.</p> \
    <p>7.2 Our site may, from time to time, contain links to and from the websites of our partner networks, advertisers and affiliates. If you follow a link to any of these websites, please note that these websites have their own privacy policies and that we do not accept any responsibility or liability for these policies. Please check these policies before you submit any personal data to these websites.</p> \
    <h3>8. ACCESS TO INFORMATION</h3> \
    <p>8.1 The Act gives you the right to access information held about you. Your right of access can be exercised in accordance with the Act. Any access request will be subject to a fee of £10 to meet our costs in providing you with details of the information we hold about you.</p> \
    <p>8.2 You may unsubscribe from certain email communications by following the Unsubscribe link in the email communication itself. You may also update your personal information by logging into the Website and visiting your Learner account page. You may also email us at <a href="mailto:Lucy.Dunleavy@boxcts.com">Lucy.Dunleavy@boxcts.com</a> in order to delete or update your personal information on our systems. We try to answer every email promptly, but may not always be able to do so. Please keep in mind, however, that there will be residual information that will remain within our databases, access logs and other records, which may or may not contain your personal information.</p> \
    <h3>9. COOKIES</h3> \
    <p>9.1 The Website uses cookies. Cookies are small files stored on your computer’s hard drive which are used to collect your personal information. You may choose to refuse cookies but, if you do so, some of the functionality of the Website or Online Courses may no longer be available to you.</p> \
    <p>9.2 For more information about cookies, including further details as to what they are and how to refuse them, please see our <a href="/cookies">Cookies Policy</a>.</p> \
    <h3>10. CHANGES TO OUR PRIVACY POLICY</h3> \
    <p>10.1 We may update or amend this Privacy Policy from time to time, to comply with law or to meet our changing business requirements, without notice to you. Any updates or amendments will be posted on the Website. By continuing to access the Website, using the Online Courses and Content or providing content in connection with the Online Courses and Content, you agree to be bound by the terms of these updates and amendments.</p> \
    <h3>CONTACT</h3> \
    <p>Questions, comments and requests regarding this privacy policy are welcomed and should be addressed to:</p> \
    <p> Legal Director,<br/> \
        Pemberton House,<br/> \
        Stafford Court,<br/> \
        Stafford Park 1,<br/> \
        Telford,<br/> \
        TF3 3BD. \
    </p>';

}
