import {Routes, RouterModule} from "@angular/router";
import {LegalComponent} from "./legal/legal.component";
import {CookiesComponent} from "./cookies/cookies.component";
import {PrivacyComponent} from "./privacy/privacy.component";
import {NgModule} from "@angular/core";
import {AuthGuard} from '../../services/auth-guard';
const legalRoutes: Routes = [
  {
    path: 'legal',
    canActivate: [AuthGuard],
    component: LegalComponent
  },
  {
    path: 'cookies',
    canActivate: [AuthGuard],
    component: CookiesComponent
  },
  {
    path: 'privacy',
    canActivate: [AuthGuard],
    component: PrivacyComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(legalRoutes)
  ],
  exports: [
    RouterModule
  ],
})
export class LegalRoutingModule {
}
