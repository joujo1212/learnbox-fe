import { Component } from '@angular/core';

@Component({
  selector: 'legal-module',
  template: '<generic-text-page [json]="legalJSON"></generic-text-page>'
})
export class LegalComponent {

  public legalJSON = {
    'master-title' : 'Legal',
    'Subtitle 1' : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur turpis at risus consequat, sit amet porta nisl molestie. Nam ut blandit neque. Etiam justo diam, sollicitudin et porttitor sed, suscipit at lorem. Maecenas a lacus tempor, hendrerit sem ornare, suscipit tortor. Vestibulum sit amet tellus placerat, pretium ipsum id, tincidunt nisl. Morbi gravida rhoncus turpis et molestie. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed tristique libero neque, ut feugiat mi facilisis et. Ut a risus a quam commodo fermentum. Cras in mauris tincidunt, dignissim quam vel, pellentesque nunc. Nullam rhoncus, odio in commodo aliquet, nibh metus venenatis felis, at placerat ligula purus sit amet leo. Vestibulum nec felis interdum, tincidunt erat a, malesuada nibh. Morbi at convallis nisl. Vivamus facilisis accumsan ligula, vel porta purus ullamcorper vel. Donec maximus risus sed finibus eleifend.',
    'Subtitle 2' : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur turpis at risus consequat, sit amet porta nisl molestie. Nam ut blandit neque. Etiam justo diam, sollicitudin et porttitor sed, suscipit at lorem. Maecenas a lacus tempor, hendrerit sem ornare, suscipit tortor. Vestibulum sit amet tellus placerat, pretium ipsum id, tincidunt nisl. Morbi gravida rhoncus turpis et molestie. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed tristique libero neque, ut feugiat mi facilisis et. Ut a risus a quam commodo fermentum. Cras in mauris tincidunt, dignissim quam vel, pellentesque nunc. Nullam rhoncus, odio in commodo aliquet, nibh metus venenatis felis, at placerat ligula purus sit amet leo. Vestibulum nec felis interdum, tincidunt erat a, malesuada nibh. Morbi at convallis nisl. Vivamus facilisis accumsan ligula, vel porta purus ullamcorper vel. Donec maximus risus sed finibus eleifend.' +
    '\n\nCras finibus justo risus, vel fringilla mi eleifend at. Interdum et malesuada fames ac ante ipsum primis in faucibus. Suspendisse rutrum, elit sit amet dictum sodales, elit mi dapibus ipsum, vitae luctus libero leo quis arcu. Duis ac consequat ex. Fusce a risus aliquam diam aliquam venenatis. Integer tortor est, venenatis facilisis maximus in, suscipit ac erat. Vestibulum facilisis ante in erat commodo, eget rhoncus neque sagittis.'
  };

}
