import { Component } from '@angular/core';

@Component({
  selector: 'cookies-policy',
  template: '<generic-text-page [innerHTML]="cookiesHTML" [masterTitle]="\'Cookies Policy\'"></generic-text-page>'
})
export class CookiesComponent {

  public cookiesHTML = '<p>LearnBox uses cookies for certain areas of our various websites. This document provides further detail on what you need to know about how we use cookies and how you can manage or remove them should you so wish. If after reading this, you continue to use the LearnBox websites without any amends to your settings, we will assume you’re happy to continue receiving all cookies on LearnBox websites.</p> \
    <h3>About Cookies</h3> \
    <p>A cookie is the name for a small file that’s downloaded to your PC or other device when you’re visiting certain websites. The cookie then gets sent back to that website when you visit it again. Cookies allow a website to recognise a user’s device, and thus you, the user. This in turn allows the website owner to give you a more ‘personalised experience’ when you use their site.</p> \
    <h3>LearnBox Cookies</h3> \
    <p>The cookies used by LearnBox across our various websites, including our <a href="/">site</a>, are as follows:</p> \
    <h3>Session Cookies</h3> \
    <p>Session cookies are stored only temporarily during a browser session. When the browser is closed the cookie is deleted from a user’s device. LearnBox sets its own Session Cookie for analytical and tracking purposes, measuring the pages visited by a user and at what time. This cookie does not collect or store personal data, so individuals cannot be identified by LearnBox. Analytics are used to help us to improve our website and make sure it is meeting user needs.</p> \
    <p>In addition to the LearnBox Session Cookie, we also set Session Cookies from Google to date/time stamp exactly when a user enters and leaves the site.</p> \
    <h3>Persistent Cookies</h3> \
    <p>Persistent Cookies are saved on a user’s device for a fixed period (typically one year or more) and not deleted when the browser session is closed. LearnBox sets a Persistent Cookie related to User ID, which is used for those sites where a user logs in. This cookie is essential for users logging in to the private area of such LearnBox sites. Users who merely browse public areas of our sites will also have this cookie set but it will not collect any data about such a user, and blocking of this cookie will not affect access to public areas. This Persistent Cookie is randomised or nulled on log-out, so retains no user information for future use.</p> \
    <p>LearnBox also sets Persistent Cookies from Google for analytical purposes. One such cookie tracks number of visits by a unique user and the other tracks where the user came from (e.g. search engine or other link) and where in the world the user is located when accessing the site.</p> \
    <h3>Third Party Cookies</h3> \
    <p>Third Party Cookies are not set directly by LearnBox, but by third-party providers.</p> \
    <p>In addition to Google’s analytical cookies as described above, Third Party Cookies may be set by providers whose services or functionality is embedded in the LearnBox websites. Such providers include, but may not be limited to Twitter, YouTube, FaceBook and WordPress. Where existing LearnBox Customers access the LearnBox Store, any third party resources will potentially set their own cookies.</p> \
    <h3>Managing Cookies on LearnBox Websites</h3> \
    <p>The Internet browser you use on your device allows you to manage cookies. You can modify your browser to notify you when cookies are being set. You can also set it to accept or refuse all or some cookies. You can even delete cookies that have already been set.</p> \
    <p>Because each browser is different, the specific way you amend the settings for cookies will vary from browser to browser; the Help function within your browser should tell you how. Alternatively, you can visit <a href="http://aboutcookies.org">www.aboutcookies.org</a> where you will find information on managing cookies on a wide variety of desktop browsers. On mobile devices, you may need to refer to the manual for that device for details on how to amend preferences.</p> \
    <p>Please note that completely disabling cookies when using LearnBox websites may not allow you to make full use of all the website’s features and functionality.</p> \
    <p>To manage third party cookies you may need to visit the websites of these third parties.</p>';
}
