import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {ModuleDetail} from '../../../../model/module-detail';
import {TutorialInfo} from '../../../../model/tutorial-info';

@Component({
  selector: 'app-module-item',
  templateUrl: 'module-item.component.html',
  styleUrls: ['./module-item.component.scss']
})
export class ModuleItemComponent implements OnInit, OnDestroy {
  private _moduleDetail: ModuleDetail;
  private _actualTutorialEnrollment: string;
  private selectedTab: number = 0;
  private itemsCount: number;
  public itemShow: number;
  private tabsCount: number;
  private leftItems: number;
  private scrollApply: boolean;
  private passedTutorials: number = 0;

  @Input()
  private progressApply: boolean = false;
  @Input()
  private showViewAllModules: boolean = false;

  constructor(private router: Router) {
  }

  ngOnInit() {
    console.log('ngOnInitModule');
    if (this.progressApply) {
      this._moduleDetail.tutorials.forEach((tutorial: TutorialInfo) => {
        if (tutorial.status === 'PASS') {
          this.passedTutorials++;
        }
      });
    }
  }

  ngOnDestroy(): void {
    console.log('ngOnDestroyModule');
  }

  changeTab(position: number) {
    const nextTab: number = this.selectedTab + position;
    if (nextTab > -1 && nextTab < this.tabsCount) {
      this.selectedTab = nextTab;
    }
  }

  createRange(number) {
    const items: number[] = [];
    for (let i = 1; i <= number; i++) {
      items.push(i);
    }
    return items;
  }

  onResize(event) {
    this.scrollApply = window.innerWidth <= 1024;
    if (!this.scrollApply) {
      this.recountItems(window.innerWidth);
      this.setSelectedIndexByActualTutorial();
    }
  }

  private recountItems(width: number) {
    if (width < 1200) {
      this.itemShow = 5;
    } else if (width < 1300) {
      this.itemShow = 6;
    } else if (width < 1400) {
      this.itemShow = 5;
    } else if (width < 1600) {
      this.itemShow = 6;
    } else if (this.progressApply && width > 1600) {
      this.itemShow = 6;
    } else if (width < 1800) {
      this.itemShow = 7;
    } else if (width < 2000) {
      this.itemShow = 8;
    } else if (width < 2200) {
      this.itemShow = 9;
    } else if (width < 2400) {
      this.itemShow = 10;
    } else {
      this.itemShow = 11;
    }
    if (this.progressApply) {
      this.itemShow--;
    }

    this.itemsCount = this._moduleDetail.tutorials.length;
    this.tabsCount = Math.ceil(this.itemsCount / this.itemShow);

    this.leftItems = this.itemsCount % this.itemShow;

    if (this.itemsCount === this.itemShow) {
      this.leftItems = this.itemsCount;
    } else if (this.leftItems === 0) {
      this.leftItems = this.itemShow;
    }
  }

  selectTutorial(tutorialInfo: TutorialInfo) {
    if (tutorialInfo.status !== 'UNAVAILABLE') {
      console.log(tutorialInfo);
      this.router.navigateByUrl('student/tutorial/' + tutorialInfo.enrollmentUuid);
    }

  }

  goToOverview() {
    this.router.navigateByUrl('/student/overview');
  }

  @Input()
  set actualTutorialEnrollment(value: string) {
    this._actualTutorialEnrollment = value;
    if (this.itemShow != null) {
      this.setSelectedIndexByActualTutorial();
    }

  }

  private setSelectedIndexByActualTutorial(): void {
    const indexOfNextTutorial =
      this._moduleDetail.tutorials
        .findIndex(tutorial => tutorial.enrollmentUuid === this._actualTutorialEnrollment);

    if (indexOfNextTutorial === -1) {
      console.log('tutorial is not in moduleDetail somewhere is error');
      return;
    }

    this.selectedTab = Math.floor(indexOfNextTutorial / this.itemShow);

  }


  get moduleDetail(): ModuleDetail {
    return this._moduleDetail;
  }

  @Input()
  set moduleDetail(value: ModuleDetail) {
    this._moduleDetail = value;
    this.scrollApply = window.innerWidth <= 1024;
    if (!this.scrollApply) {
      this.recountItems(window.innerWidth);
      this.setSelectedIndexByActualTutorial();
    }
  }

  public isLastQuiz(index: number): boolean {
    let indexOfLastQuiz = -1;
    for (let i = this._moduleDetail.tutorials.length - 1; i >= 0; i--) {
      const item = this._moduleDetail.tutorials[i];
      if (item['@type'] === 'QUIZ') {
        indexOfLastQuiz = i;
        break;
      }
    }
    if (indexOfLastQuiz === -1) { return false; }
    return index === indexOfLastQuiz;
  }
}
