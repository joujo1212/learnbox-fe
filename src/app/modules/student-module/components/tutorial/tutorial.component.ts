import {Component, OnInit, OnDestroy} from '@angular/core';
import {TutorialModuleStore} from '../../service/tutorial-module-store.service';
import {Subscription} from 'rxjs';
import {Params, ActivatedRoute} from '@angular/router';
import {TutorialDetail} from '../../../../model/tutorial-detail';
import {ModuleDetail} from '../../../../model/module-detail';
import {TutorialType} from '../../../../model/tutorial-type';
import {Endpoints} from '../../../../endpoints';
import {QuizTutorialComponent} from '../quiz-tutorial/quiz-tutorial.component';

@Component({
  selector: 'app-tutorial',
  templateUrl: 'tutorial.component.html',
  styleUrls: ['tutorial.component.scss'],
})
export class TutorialComponent implements OnInit, OnDestroy {

  tutorialDetail: TutorialDetail;
  moduleDetail: ModuleDetail;
  private subscriptionTutorial: Subscription;
  private subscriptionModule: Subscription;
  public disableFooter: boolean = false;

  constructor(private route: ActivatedRoute,
              private tutorialModuleStore: TutorialModuleStore) { }

  ngOnInit() {
    console.log('ngOnInitTutorial');
    this.subscriptionTutorial = this.tutorialModuleStore.tutorialObservable
      .subscribe(tutorial => {
        this.tutorialDetail = tutorial;
        console.log(tutorial);
      });
    this.subscriptionModule = this.tutorialModuleStore.moduleObservable
      .subscribe(module => this.moduleDetail = module);

  }

  handleDisableFooter($event) {
    // $event is a boolean value, equal to QuizTutorialComponent 'isQuizRunning' variable
    this.disableFooter = $event;
  }

  ngOnDestroy(): void {
    console.log('ngOnDestroyTutorial');
    this.subscriptionModule.unsubscribe();
    this.subscriptionTutorial.unsubscribe();
  }

  // START for demo purpose
  submitTutorial() {
    this.tutorialModuleStore.submitTutorial(this);
  }

  getEndpoint(): string {
    if (this.tutorialDetail['@type'] === 'TEXT') {
      return Endpoints.submitTextTutorial;
    }
  }

  getSubmitData(): any {
    return {
      enrollmentUuid: this.tutorialDetail.enrollmentUuid
    };
  }

}
