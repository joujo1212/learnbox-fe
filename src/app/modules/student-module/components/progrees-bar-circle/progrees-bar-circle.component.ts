import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'progrees-bar-circle',
  templateUrl: 'progrees-bar-circle.component.html',
  styleUrls: ['progrees-bar-circle.component.css']
})

/**
 * actualValue - value up in fraction
 * totalValue - value down in fraction
 * progressType - type of progress bar (percent or any)
 */
export class ProgreesBarCircleComponent implements OnInit {
  total: number = 100;
  actual: number = 0;
  actaulInGraph: number = 101;
  progressTyp: string = '';

  @Input()
  get progressType() {
    return this.progressTyp;
  }

  set progressType(type) {
    this.progressTyp = type;
  }

  @Input()
  get totalValue() {
    return this.total;
  }

  set totalValue(value) {
    this.total = value;
  }

  @Input()
  get actualValue() {
    return this.actual;
  }

  set actualValue(value: number) {
    console.log("in actual", this.totalValue);
    this.actual = value;

    value = Math.floor((this.actual*100)/this.totalValue);

    let c = Math.PI*(16*2);

    if (value < 0) { value = 0;}
    if (value > 100) { value = 100;}

    let pct = ((100-value)/100)*c;
    this.actaulInGraph = Math.ceil(pct);

  }


  constructor() {
  }

  ngOnInit() {
  }
}
