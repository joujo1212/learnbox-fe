import {Component, OnInit, Output, EventEmitter, ViewChild, ViewContainerRef, OnChanges, Input, ElementRef} from '@angular/core';
import {Router} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';
import {TutorialService} from '../../../../services/tutorial.service';
import {CountdownComponent} from '../countdown/countdown.component';
import {CalculatorComponent} from '../../../../shared/calculator/calculator.component';
import {ProjectionService} from '../../service/projection.service';

@Component({
  selector: 'app-quiz-screen',
  templateUrl: 'quiz-screen.component.html',
  styleUrls: ['quiz-screen.component.scss']
})
export class QuizScreenComponent implements OnInit, OnChanges {


  /**
   * array of questions
   */
  @Input('data') data;

  /**
   * time for quiz
   */
  @Input('time') time: number;
  @Input('enrollmentUuid') enrollmentUuid: string;
  @Input('isCalculatorRequired') isCalculatorRequired: boolean = true;

  @Output('submit') submit = new EventEmitter();

  @ViewChild('container', {read: ViewContainerRef}) container: ViewContainerRef;
  @ViewChild('countdown') countdown: CountdownComponent;
  @ViewChild('quiz') quizDiv: ElementRef;
  @ViewChild('calculator') calculator: CalculatorComponent;

  // Not needed to describe variables (most of them are described in projection.component, others are described below or above functions)
  currentQuestion;
  maxQuestions;

  resolvedModules;
  maxModules;
  avgTest: number;


  private categoryNumber = 0; // 0 == secret
  public htmlData;
  private dndData = [];
  public categoriesData = [{name: '', items: []}, {name: '', items: []}];
  private checkboxData = [];
  private textCheckboxesData = [];
  private radioData = [];
  private groupsData = [];
  private keyUpEventOnly = false;
  private secretDataHold = [];
  private secretDataHold1 = [];
  public workpadText = '';
  private isButtonShown = false;
  public useMobileLayout: boolean = false;

  public showCalc: boolean = false;
  private isSubmitted = false;
  private didInitCountdown: boolean = false;
  private userAnswers; // saves info about the user's responses as the user is taking a quiz
  private units: any;

  constructor(private _sanitizer: DomSanitizer,
              private _tutorial: TutorialService,
              private _router: Router,
              private _projection: ProjectionService) {
  }

  ngOnInit() {
    this.currentQuestion = 1;
    this.resolvedModules = '03';
    this.maxModules = '08';
    this.avgTest = 40;
    this.layoutQuizScreen();
    this._projection.getUnits().then((units: any) => {
      this.units = units.map(unit => Object.assign(unit, {sign: decodeURI(unit.sign)}));
    });
  }

  getTime(): any {
    if (!this.didInitCountdown) {
      this.didInitCountdown = true;
      return this.time;
    } else {
      return this.countdown.end;
    }
  }

  layoutQuizScreen() {
    // 1024 px is the cut-off. Any less and it will render with a vertical (mobile) layout
    this.useMobileLayout = window.innerWidth < 1024;
  }

  openCalc() {
    if (typeof this.calculator !== 'undefined') {
      this.calculator.centerCalculator();
    }
    if (this.isCalculatorRequired) {
      this.showCalc = true;
    }
  }

  closeCalc = () => {
    this.showCalc = false;
  };

  updateUsingQuestion(question: any): void {
    const questionType = question.questionType;
    this.htmlData = this._sanitizer.bypassSecurityTrustHtml(question.html);
    if (questionType === 'SECRET') {
      this.categoryNumber = 0;
    } else if (questionType === 'DROPTEXT') {
      this.dndData = question.options;
      console.log(this.dndData);
      this.categoryNumber = 1;
    } else if (questionType === 'CATEGORY') {
      this.categoriesData = question.answers;
      this.categoryNumber = 2;
    } else if (questionType === 'CHECKBOX') {
      this.checkboxData = question.answers;
      this.categoryNumber = 3;
    } else if (questionType === 'RADIO') {
      this.radioData = question.answers;
      this.categoryNumber = 4;
    } else if (questionType === 'CHECKBOX_TEXT') {
      this.textCheckboxesData = question.answers;
      this.categoryNumber = 6;
    }
    this.workpadText = question.workpad;
  }

  // initial data fill to projection
  ngOnChanges(changes: any): void {
    console.log('quiz-screen.component executing ngOnChanges()');
    console.log(this.data);
    if (typeof changes.data !== 'undefined') {
      this.maxQuestions = changes.data.currentValue.length;
      this.updateUsingQuestion(changes.data.currentValue[0]);
    }
  }

  // fill projection with next question data
  next() {
    if (this.currentQuestion < this.maxQuestions) {
      this.currentQuestion++;
    }
    this.secretDataHold1 = this.secretDataHold;
    this.saveUserAnswers();
    this.updateUsingQuestion(this.data[this.currentQuestion - 1]);
    window.scroll(0, 0);
    this.quizDiv.nativeElement.scrollTop = '0px';
  }

  // change question data in projection component to previous
  previous() {
    if (this.currentQuestion > 1) {
      this.currentQuestion--;
    }
    this.secretDataHold1 = this.secretDataHold;
    this.saveUserAnswers();
    this.updateUsingQuestion(this.data[this.currentQuestion - 1]);
    window.scroll(0, 0);
    this.quizDiv.nativeElement.scrollTop = '0px';
  }

  fakeDndValueFillerBackendFail(data) {
    for (let i = 0; i < data.length; i++) {
      data[i].value = 'opravte_backend_' + i;
    }
    console.log(data);
    return data;
  }

  // update secretData holders
  secretValuesChangedInProjection($e: any) {
    if (this.secretDataHold.length === 0) {
      this.secretDataHold.push($e.secretData[0]);
    } else {
      for (let i = 0; i < this.secretDataHold.length; i++) {
        if (this.secretDataHold[i].answerId === $e.secretData[0].answerId) {
          this.secretDataHold[i].value = $e.secretData[0].value;
          if (this.secretDataHold[i].optionIndex) {
            this.secretDataHold[i].optionIndex = $e.secretData[0].optionIndex;
          }
          return;
        }
      }
      this.secretDataHold.push($e.secretData[0]);
    }
  }

  textCheckboxValuesChangedInProjection($e: string) {
    this.textCheckboxesData = this.textCheckboxesData.map(item =>
      item.answerId === $e ? Object.assign(item, {checked: !item.checked}) : item
    );
  }

  // before submit, get data from my secretData holders (which are custom-bound to DOM) and fill the original data variable
  fillSecretsWithSavedData() {
    this.data.map(item => {
      if (item.questionType === 'SECRET' || item.questionType === 'DROPTEXT') {
        item.answers.map(answer => {
          for (let k = 0; k < this.secretDataHold.length; k++) {
            if (answer.answerId === this.secretDataHold[k].answerId) {
              answer.value = this.secretDataHold[k].value;
            }
          }
          return answer;
        });
      } else if (item.questionType === 'CHECKBOX_TEXT') {
        item.answers.map(answer => {
          let matching = null;
          for (let j = 0; j < this.textCheckboxesData.length; j++) {
            if (answer.answerId === this.textCheckboxesData[j].answerId) {
              matching = this.textCheckboxesData[j];
              break;
            }
          }
          return matching === null ? answer : matching;
        });
      }
      return item;
    });
  }

  // called whenever a mouseup or keyup event occurs in the projection
  // useful for saving the user's progress in the quiz if they click Previous or Next buttons
  saveUserAnswers() {
    this.fillSecretsWithSavedData();
    const arrayOfQuestions = [];
    let i = 0, j = 0;
    let question: any;

    while (i < this.data.length) {
      const questionType = this.data[i].questionType;
      if (questionType === 'SECRET' || questionType === 'RADIO' || questionType === 'CHECKBOX_TEXT' || questionType === 'DROPTEXT') {
        question = {
          questionUuid: this.data[i].uuid,
          type: this.data[i].questionType,
          answers: this.data[i].answers,
          workpad: this.data[i].workpad
        };
        arrayOfQuestions.push(question);
      } else if (this.data[i].questionType === 'CHECKBOX') {
        for (j = 0; j < this.data[i].answers.length; j++) {
          if (this.data[i].answers[j].checked == null) {
            this.data[i].answers[j].checked = false;
          }
        }
        question = {
          questionUuid: this.data[i].uuid,
          type: 'CHECKBOX',
          answers: this.data[i].answers
        };
        arrayOfQuestions.push(question);
      }
      i++;
    }
    this.userAnswers = arrayOfQuestions;
  }

  // submit test
  submitTest() {
    if (!this.isSubmitted) {
      this.isSubmitted = true;
      this.saveUserAnswers();
      this._tutorial.submitQuiz(this.userAnswers).subscribe((data) => {
        this.submit.emit(data);
        this.isSubmitted = false;
       },
        (err) => {
          this.isSubmitted = false;
      });
    }
    window.scroll(0, 0);
    this.quizDiv.nativeElement.scrollTop = '0px';
  }

  workpadChanged() {
    this.data[this.currentQuestion - 1].workpad = this.workpadText;
  }

  // when time runs out, submit the quiz
  submitQuizBecauseOfTime() {
    this.submitTest();
    // navigate to the results page of the quiz
    this._router.navigateByUrl('student/tutorial/' + this.enrollmentUuid);
  }

}
