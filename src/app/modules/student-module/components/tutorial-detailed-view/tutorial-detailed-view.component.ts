import {Component, Input, OnInit} from '@angular/core';
import {TutorialDetailedView} from "../../../../model/tutorial-detailed-view";

@Component({
  selector: 'app-tutorial-detailed-view',
  templateUrl: './tutorial-detailed-view.component.html',
  styleUrls: ['./tutorial-detailed-view.component.css']
})
export class TutorialDetailedViewComponent implements OnInit {

  @Input()
  tutorial: TutorialDetailedView;

  @Input()
  moduleOrder: number;

  @Input()
  tutorialOrder: number;

  constructor() { }

  ngOnInit() {
    console.log(this.tutorial);
  }

}
