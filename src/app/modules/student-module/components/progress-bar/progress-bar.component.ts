import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-progress-bar',
  templateUrl: 'progress-bar.component.html',
  styleUrls: ['progress-bar.component.css']
})
export class ProgressBarComponent {


  private _status: number;
  private _pass: number;
  statusTextPosition: number;
  passTextPosition: number;

  private isIntersectText(width1: number, position1: number, width2: number, position2: number) {
    const right1 = (position1 + (width1 / 2));
    const left1 = (position1 - (width1 / 2));

    const right2 = (position2 + (width2 / 2));
    const left2 = (position2 - (width2 / 2));

    const isIntersected = left1 < right2 && left2 < right1;
    return isIntersected;
  }

  private isIntersectPercent(value1: number, value2: number) {
    return Math.abs(value1 - value2) <= 5;
  }

  private getPositionOfText(value: number, textLength: number): number {
    return (((816 - textLength) * value) / 100) + (textLength / 2);
  }

  @Input()
  set status(value: number) {
    this._status = value;
    this.updateStatusTextPosition();
  }

  get status(): number {
    return this._status;
  }

  public updateStatusTextPosition() {
    const statusTextLength = 180;
    const statusPosition = this._status * 766 / 100 + 20;
    const hasRoomOnRight: boolean = 766 - statusPosition - 20 >= statusTextLength;
    this.statusTextPosition = hasRoomOnRight ? statusPosition + 30: statusPosition - statusTextLength;
  }

  public getPassTextPositionX(): number {
    this.passTextPosition = this.getPositionOfText(this._pass, 140);
    return this.passTextPosition;
  }

  get pass(): number {
    return this._pass;
  }

  @Input()
  set pass(value: number) {
    this._pass = value;
    this.passTextPosition = this.getPositionOfText(this._pass, 140);
  }

}
