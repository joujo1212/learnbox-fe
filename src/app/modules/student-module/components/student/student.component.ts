import {Component, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'app-student',
  templateUrl: 'student.component.html',
  styleUrls: ['student.component.css']
})

export class StudentComponent {
  private chatOpened: any = false;

  public toggleChat(val) {
    this.chatOpened = val;
  }

}
