import {AfterViewInit, Component, Input, ViewChild, OnChanges} from '@angular/core';
import {TutorialDetail} from "../../../../model/tutorial-detail";
import {Submittable} from "../../util/submittable";
import {ProgressBarComponent} from "../progress-bar/progress-bar.component";
import {ProgreesBarCircleComponent} from "../progrees-bar-circle/progrees-bar-circle.component";
import {TutorialModuleStore} from "../../service/tutorial-module-store.service";
import {Endpoints} from "../../../../endpoints";
import {CourseService} from '../../../../services/course.service';
import {CourseStatistic} from '../../../../model/course-statistic';

@Component({
  selector: 'app-text-tutorial',
  templateUrl: './text-tutorial.component.html',
  styleUrls: ['./text-tutorial.component.css']
})
export class TextTutorialComponent implements AfterViewInit, Submittable, OnChanges{
  private avg;
  private total;

  @Input()
  tutorialDetail: TutorialDetail;

  @ViewChild(ProgreesBarCircleComponent)
  private progressCircle: ProgreesBarCircleComponent;

  @ViewChild(ProgressBarComponent)
  private progressBar: ProgressBarComponent;

  constructor(
    private tutorialModuleStore: TutorialModuleStore,
    private courseService: CourseService
  ) { }

  ngOnChanges(changes: any): void {
    this.courseService.getCourseStatistic()
      .then((courseStatistic: CourseStatistic) => {
        this.avg = Math.round(courseStatistic.averageScore);
        this.total = courseStatistic.totalProgress;
      });
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.progressCircle.actualValue = 75;
    }, 1);
  }

  submitTutorial() {
    if (this.tutorialDetail.status === 'ACTIVE') {
      this.tutorialModuleStore.submitTutorial(this);
    } else {
      this.tutorialModuleStore.nextTutorial();
    }
  }

  getEndpoint(): string {
    return Endpoints.submitTextTutorial;
  }

  getSubmitData(): any {
    return {
      enrollmentUuid: this.tutorialDetail.enrollmentUuid
    };
  }
}
