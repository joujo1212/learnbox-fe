import {Component, Input} from '@angular/core';
import {TutorialInfo} from '../../../../model/tutorial-info';

@Component({
  selector: 'app-tutorial-item',
  templateUrl: 'tutorial-item.component.html',
  styleUrls: ['tutorial-item.component.css']
})
export class TutorialItemComponent {

  @Input()
  tutorialInfo: TutorialInfo;

  private _shouldUseModuleQuizLogo: boolean;

  public logoURL: string = '../../../assets/spot-quiz-logo.png';

  get shouldUseModuleQuizLogo() {
    return this._shouldUseModuleQuizLogo;
  }

  @Input()
  set shouldUseModuleQuizLogo(value: boolean) {
    this._shouldUseModuleQuizLogo = value;
    this.logoURL = this._shouldUseModuleQuizLogo ? '../../../assets/module-quiz-logo.png' : '../../../assets/spot-quiz-logo.png';
  }

}
