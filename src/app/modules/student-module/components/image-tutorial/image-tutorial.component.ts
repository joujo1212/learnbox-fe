import {AfterViewInit, Component, Input, OnInit, ViewChild, OnChanges} from '@angular/core';
import {TutorialDetail} from "../../../../model/tutorial-detail";
import {ProgressBarComponent} from "../progress-bar/progress-bar.component";
import {ProgreesBarCircleComponent} from "../progrees-bar-circle/progrees-bar-circle.component";
import {Endpoints} from "../../../../endpoints";
import {TutorialModuleStore} from "../../service/tutorial-module-store.service";
import {DomSanitizer} from "@angular/platform-browser";
import {Submittable} from "../../util/submittable";
import {CourseStatistic} from '../../../../model/course-statistic';
import {CourseService} from '../../../../services/course.service';

@Component({
  selector: 'app-image-tutorial',
  templateUrl: './image-tutorial.component.html',
  styleUrls: ['./image-tutorial.component.css']
})
export class ImageTutorialComponent implements AfterViewInit, Submittable, OnChanges {

  private avg;
  private total;

  @Input()
  public tutorialDetail: TutorialDetail;

  @ViewChild(ProgreesBarCircleComponent)
  private progressCircle: ProgreesBarCircleComponent;

  @ViewChild(ProgressBarComponent)
  private progressBar: ProgressBarComponent;

  constructor(
    private tutorialModuleStore: TutorialModuleStore,
    private _sanitizer: DomSanitizer,
    private courseService: CourseService
  ) { }

  ngOnChanges(changes: any): void {
    this.courseService.getCourseStatistic()
      .then((courseStatistic: CourseStatistic) => {
        this.avg = Math.round(courseStatistic.averageScore);
        this.total = courseStatistic.totalProgress;
      });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.progressCircle.actualValue = 75;
    }, 1);
  }

  submitTutorial() {
    if (this.tutorialDetail.status === 'ACTIVE') {
      this.tutorialModuleStore.submitTutorial(this);
    } else {
      this.tutorialModuleStore.nextTutorial();
    }
  }

  getEndpoint(): string {
    return Endpoints.submitImageTutorial;
  }

  getSubmitData(): any {
    return {
      enrollmentUuid: this.tutorialDetail.enrollmentUuid
    };
  }
}
