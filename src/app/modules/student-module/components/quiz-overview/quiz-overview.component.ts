import {Component, Input, Output, EventEmitter} from '@angular/core';
import {TutorialDetail} from '../../../../model/tutorial-detail';
import {TutorialModuleStore} from '../../service/tutorial-module-store.service';

@Component({
  selector: 'app-quiz-overview',
  templateUrl: './quiz-overview.component.html',
  styleUrls: ['./quiz-overview.component.scss']
})
export class QuizOverviewComponent {

  @Input() tutorialDetail: TutorialDetail;
  @Input('moduleDetail') moduleDetail;

  @Output('emitStart') emitStart = new EventEmitter();

  constructor(private tutorialModuleStore: TutorialModuleStore) {
  }

  next() {
    this.tutorialModuleStore.nextTutorial();
  }

  emitStartQuiz() {
    this.emitStart.emit({});
  }


}
