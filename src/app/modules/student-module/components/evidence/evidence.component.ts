import {Component, Input, OnInit} from '@angular/core';
import {TutorialModuleStore} from '../../service/tutorial-module-store.service';
import {TutorialDetail} from '../../../../model/tutorial-detail';
import {TutorialService} from '../../../../services/tutorial.service';
import {ModuleDetail} from '../../../../model/module-detail';
import {CourseService} from '../../../../services/course.service';
import {CourseStatistic} from '../../../../model/course-statistic';

@Component({
  selector: 'app-evidence-tutorial',
  templateUrl: 'evidence.component.html',
  styleUrls: ['evidence.component.scss']
})
export class EvidenceComponent implements OnInit {

  private avg;
  private total;
  @Input('tutorialDetail') private tutorialDetail: TutorialDetail;
  @Input('moduleDetail') private moduleDetail: ModuleDetail;

  constructor(private tutorialService: TutorialService,
              private tutorialModuleStore: TutorialModuleStore,
              private courseService: CourseService) {
  }

  ngOnInit(): void {
    this.courseService.getCourseStatistic()
      .then((courseStatistic: CourseStatistic) => {
        this.avg = Math.round(courseStatistic.averageScore);
        this.total = courseStatistic.totalProgress;
      });
  }

  fileChange(event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file: File = fileList[0];
      this.tutorialService.uploadEvidenceFile(file)
        .then((fileUuid: string) => {
          this.tutorialService.submitEvidenceTutorial(
            {enrollmentUuid: this.tutorialDetail.enrollmentUuid,
            fileUuid: fileUuid})
            .then(() => {
              this.tutorialModuleStore.getTutorial(this.tutorialDetail.enrollmentUuid);
              this.tutorialModuleStore.getModule(this.tutorialDetail.enrollmentModuleUuid);
            });
        });
    }
  }

  downloadDescription() {
    window.open(
      this.tutorialDetail.desc,
      '_blank'
    );
  }

}
