import {Component, EventEmitter, Input, Output, ViewEncapsulation} from '@angular/core';
import {TutorialDetail} from '../../../../model/tutorial-detail';
import {TutorialModuleStore} from '../../service/tutorial-module-store.service';
import {Submittable} from '../../util/submittable';
import {Endpoints} from '../../../../endpoints';
import {TutorialService} from '../../../../services/tutorial.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-quiz-tutorial',
  templateUrl: 'quiz-tutorial.component.html',
  styleUrls: ['quiz-tutorial.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class QuizTutorialComponent implements Submittable {

  private loading = true;
  public quizData: Array<any>;

  private quizStart = true;
  public isQuizRunning = false;
  public isCalculatorRequired = true;
  @Input('moduleDetail') moduleDetail;

  @Input()
  tutorialDetail: TutorialDetail;

  @Output('quizTutorialEmitter')
  quizTutorialEmitter: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private tutorialModuleStore: TutorialModuleStore,
              private _tutorial: TutorialService,
              private _activatedRoute: ActivatedRoute) {
    console.log(this._activatedRoute.snapshot.url[1].path);
  }

  startQuiz(): void {
    this._tutorial.getQuiz(this._activatedRoute.snapshot.url[1].path).then((res: any) => {
      console.log(this._activatedRoute.snapshot.url[1].path);
      console.log(res.questions);
      this.quizData = res.questions;
      this.loading = false;
      this.isQuizRunning = true;
      this.quizTutorialEmitter.emit(this.isQuizRunning);
      this.quizStart = false;
      this.isCalculatorRequired = res.showCalculator;
    });
  }

  submitTutorial(): void {
    this.tutorialModuleStore.submitTutorial(this);
  }

  getEndpoint(): string {
    return Endpoints.submitQuizTutorial;
  }

  getSubmitData(): any {
    return {
      enrollmentUuid: this.tutorialDetail.enrollmentUuid
    };
  }

  submitQuiz($e) {
    this.tutorialModuleStore.getTutorial(this.tutorialDetail.enrollmentUuid)
      .then(() => {
        this.isQuizRunning = false;
        this.quizTutorialEmitter.emit(this.isQuizRunning);
        this.tutorialModuleStore.getModule(this.tutorialDetail.enrollmentModuleUuid);
      });
  }
}
