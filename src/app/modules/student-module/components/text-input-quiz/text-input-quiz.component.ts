import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-text-input-quiz',
  templateUrl: 'text-input-quiz.component.html',
  styleUrls: ['text-input-quiz.component.css']
})
export class TextInputQuizComponent implements OnInit {

  //only mock because there is no backend yet
  question: any[] = ['a', 'b', 'c', 'd', 'e', 'f'];
  answers: string[] = ['answefsfs 1', 'answer 2', 'answerigs 3'];


  constructor() { }

  ngOnInit() {
  }

}
