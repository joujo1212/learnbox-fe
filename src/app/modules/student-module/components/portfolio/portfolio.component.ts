import {Component, OnInit} from '@angular/core';
import {PortfolioCourse} from '../../../../model/portfolioCourse';
import {PortfolioService} from '../../../../services/portfolio.service';
import {Qualification} from '../../../../model/learning-portfolio-qualification';

@Component({
  selector: 'app-portfolio',
  templateUrl: 'portfolio.component.html',
  styleUrls: ['portfolio.component.css'],
})
export class PortfolioComponent implements OnInit {

  courses: PortfolioCourse[] = [];
  qualifications: Qualification[] = [];

  constructor(private portfolioService: PortfolioService) {
  }

  ngOnInit(): void {
    this.portfolioService.getMyPortfolioCourses().then(response => {
      this.courses = response.json().portfolioCourses as PortfolioCourse[];
      this.qualifications = response.json().qualifications as Qualification[];
    });
  }
}
