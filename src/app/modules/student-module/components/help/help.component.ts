import { NgModule } from "@angular/core";
import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ModuleService} from "../../../../services/module.service";
import { SharedModule } from "../../../../shared/shared.module"

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.css']
})

@NgModule({
  imports: [
    SharedModule
  ]
})

export class HelpComponent implements OnInit {

  category = 'technical';
  messageSent = false;

  constructor(private moduleService: ModuleService) {
  }

  ngOnInit() {

  }
  changeCategory(category){
    this.category = category;
  }
  submitMessage(){
    this.messageSent = true;
  }
}
