import { Component, OnInit } from '@angular/core';

const HEROES = [
  {id: 1, name:'Superman', selected: false},
  {id: 2, name:'Batman', selected: false},
  {id: 5, name:'BatGirl', selected: false},
  {id: 3, name:'Robin', selected: false},
  {id: 4, name:'Flash', selected: false}
];

@Component({
  selector: 'app-checkbox-input',
  templateUrl: './checkbox-input.component.html',
  styleUrls: ['./checkbox-input.component.css']
})

export class CheckboxInputComponent implements OnInit {
public items = HEROES;
  constructor() {
  }

  ngOnInit() {
  }

}
