import {Component, OnInit, ViewChild} from '@angular/core';
import {ModuleDetail} from '../../../../model/module-detail';
import {ProgreesBarCircleComponent} from '../progrees-bar-circle/progrees-bar-circle.component';
import {ModuleService} from '../../../../services/module.service';
import {NonOnlineLearning} from '../../../../model/non-online-learning';
import {CourseService} from '../../../../services/course.service';
import {CourseStatistic} from '../../../../model/course-statistic';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Response} from '@angular/http';
import {CourseType} from '../../../../model/course-type.enum';

@Component({
  selector: 'app-course-overview',
  templateUrl: './course-overview.component.html',
  styleUrls: ['./course-overview.component.css']
})
export class CourseOverviewComponent implements OnInit {

  courseType: CourseType;
  modules: ModuleDetail[];
  overview = true;

  @ViewChild('progressCircle')
  private progressCircle: ProgreesBarCircleComponent;

  nonOnlineLearningRecords: NonOnlineLearning[];
  public courseStatistic: CourseStatistic;

  constructor(private moduleService: ModuleService,
              private courseService: CourseService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.route.params
      .subscribe((params: Params) => {
        const enrollmentUuid: string = params['id'];

        this.moduleService.getModuleFromCourse(enrollmentUuid)
          .then((response: any) => {
            this.modules = response.modules;
            this.courseType = response.courseType;
          })
          .catch((error: Response) => {
            if (error.status === 404) {
              const errorObj = error.json();
              if (errorObj.message === 'ERROR_COURSE_ENROLLMENT_NOT_FOUND') {
                this.router.navigateByUrl('student/portfolio');
              }
            }
          });

        this.courseService.getCourseStatistic(enrollmentUuid)
          .then((courseStatistic: CourseStatistic) => {
            this.courseStatistic = courseStatistic;
          });
      });
  }

  goToOverview() {
    this.overview = true;
  }

  goToDetailedView() {
    this.overview = false;
  }

  getTotalModules(): number {
    return this.modules.length;
  }

  getCompletedModules(): number {
    let completeModules: number = 0;
    for (const module of this.modules) {
      if (module.status === 'PASS') {
        completeModules++;
      }
    }
    return completeModules;
  }

  getTotalQuizzes(): number {
    let totalQuizzes: number = 0;
    for (const module of this.modules) {
      for (const tutorial of module.tutorials) {
        if (tutorial['@type'] === 'QUIZ') {
          totalQuizzes++;
        }
      }
    }
    return totalQuizzes;
  }

  getCompletedQuizzes(): number {
    let completedQuizzes: number = 0;
    for (const module of this.modules) {
      for (const tutorial of module.tutorials) {
        if (tutorial['@type'] === 'QUIZ' && tutorial.status === 'PASS') {
          completedQuizzes++;
        }
      }
    }
    return completedQuizzes;
  }

}
