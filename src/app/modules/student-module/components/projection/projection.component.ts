/**
 * Created by pati on 27/02/2017.
 */
import {
  Component, Input, OnChanges, Renderer, Injector, EventEmitter, HostListener,
  Output, AfterContentChecked, AfterContentInit, ViewChild
} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import set = Reflect.set;
import {ProjectionService} from '../../service/projection.service';

declare const document: any;

@Component({
  selector: 'projection',
  templateUrl: './projection.component.html',
  styleUrls: ['./projection.component.scss']
})
export class ProjectionComponent implements OnChanges, AfterContentChecked, AfterContentInit {

  private secretData: any = []; // helper for secret data
  @Output('secretProjectionChanged') secretProjectionChanged: any = new EventEmitter();  // output secretData helper
  @Output('textCheckboxValuesChanged') textCheckboxValuesChanged: any = new EventEmitter();  // output checkboxText helper
  @Input('htmlData') htmlData: any; // html data to one question - needed everytime
  @Input('secretDataIn') secretDataIn: any; // secret data which fills current question when question changed to secret
   private _dnd1Data: any = []; // to text drop data
  @Input('categoriesData') categoriesData: any = [{name: '', items: []}, {name: '', items: []}]; // dnd categories data
  @Input('checkboxData') checkboxData: any = []; // data for checkboxes
  @Input('radioData') radioData: any = []; // data for radio
  @Input('groupsData') groupsData: any = []; // data for connections
  @Input('categoryNumber') categoryNumber: any = 0; // category number 0 - secret 1 - dnd 2 - categories 3 - checkbox 4 - radio 5 - connections / groups
  @Input('keyUpEventOnly') keyUpEventOnly = false;
  @Input('textCheckboxesData') textCheckboxesData = [];
  @Input('units') units = [];
  private keyupHold = false; // was used for loading but now it has to stay because of stopContentCheck !!!
  private cat1items = []; // helper for categories
  private cat2items = []; // same as cat1
  private loading = true; // loading variable used to loading now it's used also to help stopContentCheck!
  secretHolder = []; // tmp var for removing secrets
  private stopContentCheckMethod: boolean = false; // without this variable it would work but It would not be effective !!it changes 1000+ loops to 1-2.. needed because of DOM data filling :)
  @Output('buttonShow') buttonShow = new EventEmitter();

  @ViewChild('player') player;
  startDragID;
  whichDropToColor: string = '';
  unitsChanged: boolean = true;

  hideIndex: HideHolder[] = [];

  constructor(private _sanitizer: DomSanitizer,
              private _renderer: Renderer,
              private injector: Injector) {
  }

  get dnd1Data(): any {
    return this._dnd1Data;
  }
  @Input('dnd1Data')
  set dnd1Data(value: any) {
    if (value) {
      this._dnd1Data = value;
      this.hideIndex = [];
      let index = 0;
      for (const dndDataItem of this._dnd1Data) {
        this.hideIndex[index] = {
          idInput: null,
          hide: false
        };
        index++;
      }
    } else {
      this.hideIndex = [];
    }
  }

  ngAfterContentInit() {
    const obj = this.player.nativeElement;
    setTimeout(() => {
      this.buttonShow.emit(obj.scrollTop >= (obj.scrollHeight - obj.offsetHeight - 20));
    }, 100);
  }

  scrollEventForButtonChange() {
    const obj = this.player.nativeElement;
    if (obj.scrollTop >= (obj.scrollHeight - obj.offsetHeight - 20)) {
      // console.log('bottom');
      this.buttonShow.emit(true);
    } else {
      this.buttonShow.emit(false);
    }
  }

  // removing wrong dom elements from text + holding data in secrets
  ngAfterContentChecked(): void {
    if (!this.stopContentCheckMethod) {
      let elementsToRemove: any;
      let i = 0;
      if (this.categoryNumber === 0) {
        setTimeout(() => {
          const unitsToEdit = document.querySelectorAll('#lecture_player option');
          if (unitsToEdit.length !== 0) {
            if (unitsToEdit.length !== 0) {
              while (i < unitsToEdit.length) {
                if (unitsToEdit[i].parentNode) {
                  unitsToEdit[i].parentNode.removeChild(unitsToEdit[i]);
                }
                i++;
              }
            }
            const unitsToAddOption = document.querySelectorAll('.units-select');
            unitsToAddOption.forEach(select => {
              const option = document.createElement('option');
              option.text = '';
              option.disabled = true;
              option.selected = true;
              // if (unitsToAddOption[i].length <= 1) {
              select.add(option);
              this.units.forEach(unit => {
                const option = document.createElement('option');
                option.text = unit.sign;
                option.value = unit.value;
                select.add(option);
              });
            });
          }
        }, 100);

        elementsToRemove = document.querySelectorAll('#lecture_player .dragFromBottomToText');
        while (i < elementsToRemove.length) {
          elementsToRemove[i].parentNode.removeChild(elementsToRemove[i]);
          i++;
        }
        // after contentChecked it takes some ms to get DOM ready
        // (up to 10ms maybe even less) removing timeout will mess up secret data change
        setTimeout(() => {
          // this.secretInputsFiller();
          // this.implementUnitsToSecrets();
          // insurance to stop this loop ! do not remove !
          // + first load bug fix .. if you remove this you will destroy users computer :D
          this.secretInputsFiller();
          this.stopContentCheckMethod = true;
        }, 15);

      } else if (this.categoryNumber === 1) {
        // elementsToRemove = document.querySelectorAll('#lecture_player .secretInputFromWord');
        // this.secretHolder = elementsToRemove;
        // while (i < elementsToRemove.length) {
        //   elementsToRemove[i].parentNode.removeChild(elementsToRemove[i]);
        //   i++;
        // }
        // console.log(this.dnd1Data);

        setTimeout(() => {
          this.dndDataInputsFiller();
          // insurance to stop this loop ! do not remove !
          // + first load bug fix .. if you remove this you will destroy users computer :D
          this.stopContentCheckMethod = true;
        }, 15);

        this.stopContentCheckMethod = true;
      } else if (this.categoryNumber === 6) {
        // after contentChecked it takes some ms to get DOM ready
        // (up to 10ms maybe even less) removing timeout will mess up secret data change
        setTimeout(() => {
          this.textCheckboxesFiller();
          // insurance to stop this loop ! do not remove !
          // + first load bug fix .. if you remove this you will destroy users computer :D
          this.stopContentCheckMethod = true;
        }, 15);

      } else {
        elementsToRemove = document.querySelectorAll('#lecture_player .dragFromBottomToText');
        while (i < elementsToRemove.length) {
          elementsToRemove[i].parentNode.removeChild(elementsToRemove[i]);
          i++;
        }
        i = 0;
        elementsToRemove = document.querySelectorAll('#lecture_player .secretInputFromWord');
        while (i < elementsToRemove.length) {
          elementsToRemove[i].parentNode.removeChild(elementsToRemove[i]);
          i++;
        }
        this.stopContentCheckMethod = true;
      }

    }
    this.loading = false;

  }

  dragValue: string = '';
  indexToRemoveAfterDrag: number;
  dragEventID: any;

  dragoverLecture(event) {
    // console.log('over lecture dragged');
    // console.log();
    this.dragEventID = event.target.id;
    // document.getElementById(this.dragEventID).style.background = 'blue!important';
    // console.log('lecture');
  }

  removeElement($event) {
    if ($event.target.className === 'dragFromBottomToText') {
      console.log($event);
      console.log('remove element');

    }
  }

  dragstart(event, index) {
    console.log('dragstart ');
    console.log(event);
    this.dragValue = event.target.textContent;
    console.log('dragstart with value: ' + this.dragValue);
    this.indexToRemoveAfterDrag = index;
  }

  dragendDom() {
    if (this.dragEventID !== 'lecture_player') {
      if (typeof this.dragEventID !== 'undefined' && this.dragEventID !== null) {
        document.getElementById(this.dragEventID).value = this.dragValue;
        for (const item of this.hideIndex) {
          if (item.idInput === this.dragEventID) {
            item.hide = false;
          }
        }
        this.hideIndex[this.indexToRemoveAfterDrag].hide = true;
        this.hideIndex[this.indexToRemoveAfterDrag].idInput = this.dragEventID;
        const secretData = [];
        secretData.push({
          answerId: this.dragEventID,
          value: this.dragValue,
          optionIndex: this.indexToRemoveAfterDrag
        });
        this.secretProjectionChanged.emit({
          secretData: secretData
        });
        this.dragEventID = null;
        this.indexToRemoveAfterDrag = null;
        this.dragValue = null;
      } else {
        this.dragEventID = null;
        this.indexToRemoveAfterDrag = null;
        this.dragValue = null;
      }

    } else {
      this.dragEventID = null;
      this.indexToRemoveAfterDrag = null;
      this.dragValue = null;
    }

  }


  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.scrollEventForButtonChange();
  }

  // binding secrets to variable
  @HostListener('keyup', ['$event.target']) changed($e) {
    // console.log($e.id);
    // console.log($e.value);
    // console.log(this.categoryNumber);
    if (this.categoryNumber === 0) {
      let i = 0;
      const elements = document.querySelectorAll('.secretInputFromWord, .units-select');
      // console.log(elements);
      this.secretData = [];
      // console.log(elements[i].id, $e.id);
      while (i < elements.length) {
        if (elements[i].id === $e.id) {
          this.secretData.push({
            value: $e.value
          });
          break;
        }
        i++;
      }
      // console.log('push data');
      this.secretProjectionChanged.emit({
        secretData: this.secretData
      });
    }
  };

  @HostListener('change', ['$event.target']) changed2($e) {
    // console.log($e.id);
    // console.log($e.value);
    if (this.categoryNumber === 0) {
      let i = 0;
      const elements = document.querySelectorAll('.secretInputFromWord, .units-select');
      // console.log(elements);
      this.secretData = [];
      // console.log(elements[i].id, $e.id);
      while (i < elements.length) {
        if (elements[i].id === $e.id) {
          this.secretData.push({
            answerId: elements[i].id,
            value: $e.value
          });
          break;
        }
        i++;
      }
      // console.log('push data');
      this.secretProjectionChanged.emit({
        secretData: this.secretData
      });
    } else if (this.categoryNumber === 6) {
      this.textCheckboxValuesChanged.emit($e.parentNode.querySelector('.check').id);
    }
  };

  // holding data in secrets
  secretInputsFiller() {
    if (typeof this.secretDataIn !== 'undefined' && this.categoryNumber === 0) {
      let i = 0;
      while (i < this.secretDataIn.length) {
        const id = this.secretDataIn[i].answerId;
        const tmp = document.getElementById(id);
        if (tmp != null) {
          tmp.value = this.secretDataIn[i].value;
          // console.log(tmp);
          // console.log(typeof tmp == "object",i+1, this.secretDataIn.length);
          if (typeof tmp === 'object') {
            this.stopContentCheckMethod = true;
          }
        }
        i++;
      }
    }
  }

  dndDataInputsFiller() {
    if (typeof this.secretDataIn !== 'undefined' && this.categoryNumber === 1) {
      let i = 0;
      while (i < this.secretDataIn.length) {
        const id = this.secretDataIn[i].answerId;
        const inputElement = document.getElementById(id);
        if (inputElement != null) {
          inputElement.value = this.secretDataIn[i].value;
          this.hideIndex[this.secretDataIn[i].optionIndex].hide = true;
          this.hideIndex[this.secretDataIn[i].optionIndex].idInput = this.secretDataIn[i].answerId;
          // console.log(tmp);
          // console.log(typeof tmp == "object",i+1, this.secretDataIn.length);
          if (typeof inputElement === 'object') {
            this.stopContentCheckMethod = true;
          }
        }
        i++;
      }
    }
  }

  textCheckboxesFiller() {
    if (typeof this.textCheckboxesData !== 'undefined' && this.categoryNumber === 6) {
      this.textCheckboxesData.map(item => {
        const tmp = document.getElementById(item.answerId);
        if (tmp !== null) {
          document.getElementById(item.answerId).parentNode.parentNode.querySelector('input').checked = item.checked;
          if (typeof tmp === 'object') {
            this.stopContentCheckMethod = true;
          }
        }
      });
    }
  }

  ngOnChanges(changes: any): void {
    if (typeof changes.keyUpEventOnly !== 'undefined') {
      this.keyupHold = changes.keyUpEventOnly.currentValue;
    }
    if (!this.keyupHold) {
      this.loading = true;
      this.stopContentCheckMethod = false;
    }

  }

  setStartDragContainer($e) {
    this.startDragID = $e.target.parentNode.id;
  }

  // method which will be used on drop categories question
  // TODO pri realnej projekcii naplnit categoriesshuffleddata :)
  transferDataSuccess($event) {
    this.whichDropToColor = '';
    let objToPush;

    if ($event.mouseEvent.target.id === 'cat0') {
      if (this.startDragID !== 'cat0') {
        if (this.startDragID === 'cat1') {
          objToPush = this.deleteItemFromArray($event.dragData.id, this.cat1items);
        } else {
          objToPush = this.deleteItemFromArray($event.dragData.id, this.cat2items);
        }

        this.categoriesData.push(objToPush);
      }
    } else if ($event.mouseEvent.target.id === 'cat1') {
      if (this.startDragID !== 'cat1') {
        if (this.startDragID === 'cat0') {
          objToPush = this.deleteItemFromArray($event.dragData.id, this.categoriesData);
        } else {
          objToPush = this.deleteItemFromArray($event.dragData.id, this.cat2items);
        }
        this.cat1items.push(objToPush);
      }

    } else if ($event.mouseEvent.target.id === 'cat2') {
      if (this.startDragID !== 'cat2') {
        if (this.startDragID === 'cat0') {
          objToPush = this.deleteItemFromArray($event.dragData.id, this.categoriesData);
        } else {
          objToPush = this.deleteItemFromArray($event.dragData.id, this.cat1items);
        }
        this.cat2items.push(objToPush);
      }
    }


    // if ($event.mouseEvent.target.id == 'cat1') {
    //   let sameArrayTest = this.deleteItemFromArray($event.dragData.id, this.cat1items);
    //   if (sameArrayTest != null) {
    //     this.cat1items.push(sameArrayTest);
    //     return;
    //   }
    //   let draggedItem = this.deleteItemFromArray($event.dragData.id, this.categoriesData);
    //   let draggedItem1 = this.deleteItemFromArray($event.dragData.id, this.cat2items);
    //   if (draggedItem != null) {
    //     this.cat1items.push(draggedItem);
    //   } else if (draggedItem1 != null) {
    //     this.cat1items.push(draggedItem1);
    //
    //   } else {
    //     this.cat1items.push(this.deleteItemFromArray($event.dragData.id, this.categoriesData));
    //   }
    // } else {
    //   let sameArrayTest = this.deleteItemFromArray($event.dragData.id, this.cat2items);
    //   if (sameArrayTest != null) {
    //     this.cat2items.push(sameArrayTest);
    //     return;
    //   }
    //   let draggedItem = this.deleteItemFromArray($event.dragData.id, this.categoriesData);
    //   let draggedItem1 = this.deleteItemFromArray($event.dragData.id, this.cat1items);
    //   if (draggedItem != null) {
    //     this.cat2items.push(draggedItem);
    //   } else if (draggedItem1 != null) {
    //     this.cat2items.push(draggedItem1);
    //   } else {
    //     this.cat2items.push(this.deleteItemFromArray($event.dragData.id, this.categoriesData));
    //   }
    //
    // }
    // console.log(this.cat1items, this.cat2items);
  }

  // delete by id
  deleteItemFromArray(id, array) {
    let i = 0;
    while (i < array.length) {
      if (array[i].id === id) {
        const returnValue = array[i];
        array.splice(i, 1);
        return returnValue;
      }
      i++;
    }
    return null;
  }

  // array shuffler
  shuffle(array) {
    let currentIndex = array.length, temporaryValue, randomIndex;
    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  }

  // custom radio buttons logic with data bindning
  changedRadio(radioData, pos) {
    //// console.log((radioData);
    if (pos != null) {
      if (!radioData[pos].checked) {
        radioData[pos].checked = false;
      } else {
        let i = 0, change = true;
        while (i < radioData.length) {
          radioData[i].checked = false;
          i++;
        }
        if (change) {
          radioData[pos].checked = true;
        }
      }

    }

  }

}

export class HideHolder {
  public idInput: string;
  public hide: boolean;
}


