import {Component, ViewChild, AfterViewInit, Input, OnChanges, ElementRef, OnDestroy} from '@angular/core';
import {ProgreesBarCircleComponent} from '../progrees-bar-circle/progrees-bar-circle.component';
import {ProgressBarComponent} from '../progress-bar/progress-bar.component';
import {TutorialModuleStore} from '../../service/tutorial-module-store.service';
import {Submittable} from '../../util/submittable';
import {Endpoints} from '../../../../endpoints';
import {TutorialDetail} from '../../../../model/tutorial-detail';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {ModuleDetail} from '../../../../model/module-detail';
import {CourseService} from '../../../../services/course.service';
import {CourseStatistic} from '../../../../model/course-statistic';

@Component({
  selector: 'app-video-tutorial',
  templateUrl: 'video-tutorial.component.html',
  styleUrls: ['video-tutorial.component.css'],
})
export class VideoTutorialComponent implements AfterViewInit, Submittable, OnDestroy {

  public avg;
  public total;

  private progressData;

  private _tutorialDetail: TutorialDetail;
  private _moduleDetail: ModuleDetail;
  public videoURL: SafeResourceUrl;
  public moduleName: string;
  private playerOrigin = '*';
  private isIFrameInited: boolean;
  private fullVideoUrl = '';
  /**
   * Replay video is realized by change status of tutorial.
   * This change is only on front size, so after video is
   * finished must be change back to PASS status for consistency.
   * @type {boolean}
   */
  private replayed = false;
  /**
   * Variable prevent send more request for submit video,
   * because finished event from Vimeo can be send more times.
   * @type {boolean}
   */
  private finished = false;

  @Input('moduleDetail')
  set moduleDetail(value) {
    if (value !== null) {
      this._moduleDetail = value;
      if (this._moduleDetail.name) {
        this.moduleName = this._moduleDetail.name;
      }
      this.setAutoplayFirstTutorial();
    }
  }
  @Input()
  set tutorialDetail(value: TutorialDetail) {
    this._tutorialDetail = value;
    this.replayed = false;
    this.finished = false;
    this.fullVideoUrl = this.tutorialDetail.urlVideo + '?title=0&byline=0&api=1';

    this.setAutoplayFirstTutorial();

    this.courseService.getCourseStatistic()
      .then((courseStatistic: CourseStatistic) => {
        this.avg = Math.round(courseStatistic.averageScore);
        this.total = courseStatistic.totalProgress;
      });
  }

  @ViewChild(ProgreesBarCircleComponent)
  private progressCircle: ProgreesBarCircleComponent;

  @ViewChild(ProgressBarComponent)
  private progressBar: ProgressBarComponent;

  @ViewChild('iframe') iframe: ElementRef;

  constructor(private tutorialModuleStore: TutorialModuleStore,
              private _sanitizer: DomSanitizer,
              private courseService: CourseService) { }

  ngAfterViewInit() {
    this.isIFrameInited = true;
  }

  submitTutorial() {
    if (this.tutorialDetail.status === 'ACTIVE') {
      this.tutorialModuleStore.submitTutorial(this);
    } else {
      this.tutorialModuleStore.nextTutorial();
    }
  }

  getEndpoint(): string {
    return Endpoints.submitVideoTutorial;
  }

  getSubmitData(): any {
    return {
      enrollmentUuid: this._tutorialDetail.enrollmentUuid
    };
  }

  get tutorialDetail(): TutorialDetail {
    return this._tutorialDetail;
  }

  /**
   * Handle replaying vide - hide overlay, play video again
   */
  replayVideo() {
    this.replayed = true;
    this.tutorialDetail.status = 'ACTIVE';
    this.videoURL = this._sanitizer.bypassSecurityTrustResourceUrl(this.fullVideoUrl + '&autoplay=true');
  }

  // Handle messages received from the player
  onMessageReceived(event) {
    // Handle messages from the vimeo player only
    if (!(/^https?:\/\/player.vimeo.com/).test(event.origin)) {
        return false;
    }
    if (this.playerOrigin === '*') {
        this.playerOrigin = event.origin;
    }

    const data = JSON.parse(event.data);
    if (data.event === 'finish' && !this.finished) {
      if (this.replayed) {
        this._tutorialDetail.status = 'PASS';
        this.replayed = false;
      }
      this.finished = true;
      this.submitTutorial();
    }
  }

  // Helper function for sending a message to the player
  post(action, value) {
    const data = {
      method: action,
      value: ''
    };
    if (value) {
        data.value = value;
    }
    const message = JSON.stringify(data);
    this.iframe.nativeElement.contentWindow.postMessage(message, this.playerOrigin);
  }

  // See full example of handling  Vimeo events: https://codepen.io/bdougherty/pen/FEuDd
  onIframeLoad() {
     if (this.isIFrameInited) {
       setTimeout(() => {
         this.post('addEventListener', 'finish');
         window.addEventListener('message', this.onMessageReceived.bind(this), false);
       }, 500);
     }
   }

  ngOnDestroy() {
    window.removeEventListener('message', this.onMessageReceived);
  }

  private setAutoplayFirstTutorial() {
    if (this._moduleDetail && this._tutorialDetail) {
      // if video tutorial is first in course autoplay video
      if (this._tutorialDetail.order === 1 && this._moduleDetail.order === 1 && this._tutorialDetail.status === 'ACTIVE') {
        this.fullVideoUrl = this.fullVideoUrl + '&autoplay=1';
      }
      this.videoURL = this._sanitizer.bypassSecurityTrustResourceUrl(this.fullVideoUrl);
    }
  }
}
