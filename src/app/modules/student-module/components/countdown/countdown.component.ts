import {Component, Input, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-countdown',
  templateUrl: 'countdown.component.html',
  styleUrls: ['countdown.component.css']
})
export class CountdownComponent {

  @Input() end: any;
  @Output('endedfunc') endedfunc = new EventEmitter();
  @Input() displayString: string = '';
  ended: boolean = false;
  constructor() {
    //while (!this.ended) {
      setInterval(() => this._displayString(), 1000);
    //}
  }

  _displayString() {
    if(this.end >= 0){
      var returnString = '';
      var minutes = Math.floor( this.end / 60);
      var seconds = Math.ceil(this.end % 60);
      if(minutes < 10){
        returnString += ' 0' + minutes;
      }else{
        returnString += ' ' + minutes
      }
      if (seconds < 10){
        returnString += ':0' + seconds;
      }else{
        returnString += ':' + seconds;
      }
      returnString += (minutes === 0 ? ' SEC' : ' MIN');

      this.displayString = returnString;
      this.end -= 1;

    }else if(!this.ended){
      //You can also call a function for an action!
      this.endedfunc.emit();
    }

  }

}
