import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-timer',
  templateUrl: 'timer.component.html',
  styleUrls: ['timer.component.css']
})
export class TimerComponent {
  @Input() displayString: string = '';
  //use a getter for the time to get the total time running
  time: number = 0;
  constructor() {
    setInterval(() => this._displayString(), 1000);
  }

  _displayString() {
      var returnString = '';
      var hours = Math.floor(this.time / 3600);
      var minutes = Math.floor( this.time / 60) % 60;
      var seconds = Math.ceil(this.time % 60);
      if(hours > 0 && hours >= 10){
        returnString += ' ' + hours + ':';
      }else if(hours > 0){
        returnString += ' 0' + hours + ':';
      }
      if(minutes < 10){
        returnString += '0' + minutes;
      }else{
        returnString += '' + minutes
      }
      if (seconds < 10){
        returnString += ':0' + seconds;
      }else{
        returnString += ':' + seconds;
      }
      returnString += ' MIN';

      this.displayString = returnString;
      this.time += 1;



  }

}
