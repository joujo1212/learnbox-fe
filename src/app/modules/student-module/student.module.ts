import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StudentRoutingModule} from './student-routing.module';
import {PortfolioComponent} from './components/portfolio/portfolio.component';
import {HelpComponent} from './components/help/help.component';
import {TutorialComponent} from './components/tutorial/tutorial.component';
import {StudentComponent} from './components/student/student.component';
import {VideoTutorialComponent} from './components/video-tutorial/video-tutorial.component';
import {ProgressBarComponent} from './components/progress-bar/progress-bar.component';
import {ProgreesBarCircleComponent} from './components/progrees-bar-circle/progrees-bar-circle.component';
import {TutorialItemComponent} from './components/tutorial-item/tutorial-item.component';
import {ModuleItemComponent} from './components/module-item/module-item.component';
import {PERFECT_SCROLLBAR_CONFIG} from '../../libs-config/perfectscrollbar.config';
import {PerfectScrollbarModule} from 'angular2-perfect-scrollbar';
import {MaterialModule} from '@angular/material';

import {QuizScreenComponent} from './components/quiz-screen/quiz-screen.component';
import {TextInputQuizComponent} from './components/text-input-quiz/text-input-quiz.component';
import {CountdownComponent} from './components/countdown/countdown.component';
import {TimerComponent} from './components/timer/timer.component';
import {CheckboxInputComponent} from './components/checkbox-input/checkbox-input.component';
import {FormsModule} from '@angular/forms';
import {BorderedInputComponent} from '../../components/bordered-input/bordered-input.component';
import {QuizOverviewComponent} from './components/quiz-overview/quiz-overview.component';
import {QuizTutorialComponent} from './components/quiz-tutorial/quiz-tutorial.component';
import {ProjectionComponent} from './components/projection/projection.component';
import {ProjectionService} from './service/projection.service';
import {DndModule} from 'ng2-dnd';
import {TutorialDetailResolver} from './resolver/tutorial-detail-resolver.service';
import {TutorialModuleStore} from './service/tutorial-module-store.service';
import {ImageTutorialComponent} from './components/image-tutorial/image-tutorial.component';
import {TextTutorialComponent} from './components/text-tutorial/text-tutorial.component';
import {CourseOverviewComponent} from './components/course-overview/course-overview.component';

import {SharedModule} from '../../shared/shared.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {TutorialDetailedViewComponent} from './components/tutorial-detailed-view/tutorial-detailed-view.component';
import {EvidenceComponent} from './components/evidence/evidence.component';
import {ScrollToModule} from 'ng2-scroll-to';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule.forRoot(),
    PerfectScrollbarModule.forRoot(PERFECT_SCROLLBAR_CONFIG),
    StudentRoutingModule,
    FormsModule,
    FlexLayoutModule,
    DndModule.forRoot(),
    SharedModule,
    ScrollToModule.forRoot()
  ],
  declarations: [
    PortfolioComponent,
    ModuleItemComponent,
    TutorialItemComponent,
    ProgreesBarCircleComponent,
    ProgressBarComponent,
    VideoTutorialComponent,
    StudentComponent,
    TutorialComponent,
    QuizScreenComponent,
    TextInputQuizComponent,
    CountdownComponent,
    TimerComponent,
    CheckboxInputComponent,
    BorderedInputComponent, // malo by to ist do spolocneho modulu,
    // BlueButtonComponent,
    QuizOverviewComponent,
    QuizTutorialComponent,
    ImageTutorialComponent,
    TextTutorialComponent,
    ProjectionComponent,
    CourseOverviewComponent,
    EvidenceComponent,
    TutorialDetailedViewComponent,
    HelpComponent
  ],
  providers: [
    ProjectionService,
    TutorialDetailResolver,
    TutorialModuleStore],
  entryComponents: [ProjectionComponent]

})
export class StudentModule {
}
