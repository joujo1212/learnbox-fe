import {Injectable} from '@angular/core';
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {TutorialModuleStore} from '../service/tutorial-module-store.service';
import {TutorialService} from '../../../services/tutorial.service';
import {Response} from '@angular/http';
import {error} from 'selenium-webdriver';
@Injectable()
export class TutorialDetailResolver implements Resolve<boolean> {

  constructor(private tutorialModuleStore: TutorialModuleStore,
              private tutorialService: TutorialService,
              private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    const enrollmentTutorialUuid: string = route.params['id'];
    // console.log("resolve tutorialUuid:", enrollmentTutorialUuid);

    if (enrollmentTutorialUuid === 'active') {
      return this.tutorialService.getEnrollmentUuidOfActualTutorial()
        .then((enrollmentTutorialUuidActive: string) => {
          this.router.navigateByUrl('student/tutorial/' + enrollmentTutorialUuidActive);
          return null;
        })
        .catch((error: Response) => {
          if (error.status === 404) {
            const errorBody = error.json();
            if (errorBody.message === 'ERROR_TUTORIAL_ENROLLMENT_NOT_FOUND' ||
              errorBody.message === 'ERROR_TUTORIAL_NOT_ACTIVE') {
              this.router.navigate(['/student/overview']);
            }
            if (errorBody.message === 'ERROR_COURSE_ENROLLMENT_NOT_FOUND') {
              this.router.navigateByUrl('student/portfolio');
            }
          }
          if (error.status === 412) {
            this.router.navigateByUrl('student/portfolio');
          }
        });
    } else {
      return this.tutorialModuleStore.getTutorial(enrollmentTutorialUuid)
        .then(() => {
          // console.log("Successfully get TutorialDetail with enrollmentUuid [" + enrollmentTutorialUuid + "]");
          return true;
        })
        .catch(error => {
          // console.log("Unable fetch TutorialDetail error happened.", error);
          this.router.navigate(['/page-not-found']);
          return null;
        });
    }


  }

}
