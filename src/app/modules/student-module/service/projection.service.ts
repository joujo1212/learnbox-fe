/**
 * Created by pati on 20/03/2017.
 */
import {Injectable} from '@angular/core';
import {Endpoints} from '../../../endpoints';
import {Response, Http} from '@angular/http';

@Injectable()
export class ProjectionService {

  constructor(private http: Http) {
  }

  getUnits() {
    return this.http.get(Endpoints.getUnits)
      .toPromise()
      .then((response: Response) => response.json()).catch((err) => {
        console.log(err);
      });
  }
}
