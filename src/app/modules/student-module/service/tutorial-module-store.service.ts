import {Injectable} from '@angular/core';
import {TutorialService} from '../../../services/tutorial.service';
import {ModuleService} from '../../../services/module.service';
import {Router} from '@angular/router';
import {Submittable} from '../util/submittable';
import {TutorialDetail} from '../../../model/tutorial-detail';
import {ModuleDetail} from '../../../model/module-detail';
import {TutorialInfo} from '../../../model/tutorial-info';
import {OnBoardService} from '../../../services/on-board.service';
import {Response} from '@angular/http';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class TutorialModuleStore {

  private tutorialSubject: BehaviorSubject<TutorialDetail> = new BehaviorSubject<TutorialDetail>(null);
  private moduleSubject: BehaviorSubject<ModuleDetail> = new BehaviorSubject<ModuleDetail>(null);

  tutorialObservable: Observable<TutorialDetail> = this.tutorialSubject.asObservable();
  moduleObservable: Observable<ModuleDetail> = this.moduleSubject.asObservable();

  constructor(private tutorialService: TutorialService,
              private moduleService: ModuleService,
              private router: Router,
              private onBoardService: OnBoardService) {
  }

  getTutorial(enrollmentTutorialUuid: string) {
    return this.tutorialService
      .getTutorialByUuid(enrollmentTutorialUuid)
      .then((tutorial: TutorialDetail) => {
        this.tutorialSubject.next(tutorial);
        if (this.moduleSubject.getValue() == null ||
          this.moduleSubject.getValue().enrollmentUuid !== tutorial.enrollmentModuleUuid) {
          // TODO: co ked sa nenacita module
          this.getModule(tutorial.enrollmentModuleUuid);
        }
      });
  }

  getModule(enrollmentModuleUuid: string): Promise<any> {
    return this.moduleService
      .getModuleByUuid(enrollmentModuleUuid)
      .then((module: ModuleDetail) => this.moduleSubject.next(module));
  }

  submitTutorial(submittableObject: Submittable): void {
    console.log(submittableObject);
    this.tutorialService
      .submitTutorial(submittableObject)
      .then((module: ModuleDetail) => {
        this.moduleSubject.next(module);
        this.nextActive();
      })
      .catch((error: Response) => {
        if (error.status === 412) {
          const errorMessage = error.json().message;
          if (errorMessage === 'ERROR_TUTORIAL_NOT_ACTIVE' || errorMessage === 'ERROR_COURSE_FINISHED') {
            const moduleActual = this.moduleSubject.getValue();
            this.getModule(moduleActual.enrollmentUuid);
            this.router.navigateByUrl('student/overview');
          } else {
            return Promise.reject(error);
          }
        }
      });
  }

  nextActive(): void {
    const moduleActual = this.moduleSubject.getValue();
    const actualTutorial: TutorialInfo =
      moduleActual.tutorials
        .find((tutorialInfo: TutorialInfo) => tutorialInfo.status === 'ACTIVE');

    if (actualTutorial) {
      this.router.navigateByUrl('student/tutorial/' + actualTutorial.enrollmentUuid);
    } else {
      this.router.navigateByUrl('student/tutorial/' + 'unknown_tutorial');
    }
  }

  nextTutorial(): void {
    const tutorialLast = this.tutorialSubject.getValue();
    const moduleActual = this.moduleSubject.getValue();

    let indexOfNextTutorial = moduleActual.tutorials
        .findIndex(tutorial => tutorial.enrollmentUuid === tutorialLast.enrollmentUuid);

    indexOfNextTutorial++;
    if (moduleActual.tutorials.length > indexOfNextTutorial) {
      const nextTutorial: TutorialInfo = moduleActual.tutorials[indexOfNextTutorial];
      if (nextTutorial.status !== 'UNAVAILABLE') {
        this.router.navigateByUrl('student/tutorial/' + nextTutorial.enrollmentUuid);
      }
    } else {
      this.router.navigateByUrl('student/overview');
    }
  }

}
