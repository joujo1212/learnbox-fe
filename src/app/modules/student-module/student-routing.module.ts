import {Routes, RouterModule} from '@angular/router';
import {StudentComponent} from './components/student/student.component';
import {TutorialComponent} from './components/tutorial/tutorial.component';
import {NgModule} from '@angular/core';
import {PortfolioComponent} from './components/portfolio/portfolio.component';
import {QuizScreenComponent} from './components/quiz-screen/quiz-screen.component';
import {AuthGuard} from '../../services/auth-guard';
import {TutorialDetailResolver} from './resolver/tutorial-detail-resolver.service';
import {CourseOverviewComponent} from './components/course-overview/course-overview.component';
import {EvidenceComponent} from './components/evidence/evidence.component';
import {HelpComponent} from './components/help/help.component';
const studentRoutes: Routes = [
  {
    path: 'student',
    component: StudentComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    children: [
      {
        path: 'tutorial/:id',
        component: TutorialComponent,
        resolve: {
          value: TutorialDetailResolver
        }
      },
      {
        path: 'portfolio',
        component: PortfolioComponent
      },
      {
        path: 'quiz',
        component: QuizScreenComponent
      },
      {
        path: 'overview',
        component: CourseOverviewComponent,
      },
      {
        path: 'overview/:id',
        component: CourseOverviewComponent,
      },
      {
        path: 'evidence',
        component: EvidenceComponent
      },
      {
        path: 'help',
        component: HelpComponent
      }
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(studentRoutes)
  ],
  exports: [
    RouterModule
  ],
})
export class StudentRoutingModule {
}
