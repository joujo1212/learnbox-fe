export interface Submittable {

  getEndpoint(): string;
  getSubmitData(): any;

}
