import {Component, OnInit} from '@angular/core';
import {ClassLearnerProgress} from '../../../../model/class-learner-progress';
import {ClassLearnerProgressService} from '../../../../services/class-learner-progress.service';
import {Router} from '@angular/router';

@Component({
  selector: 'class-learner-progress',
  templateUrl: 'class-learner-progress.component.html',
  styleUrls: ['class-learner-progress.component.css']
})
export class ClassLearnerProgressComponent implements OnInit {
  public classLearnerProgresses: ClassLearnerProgress[];
  public isOpenedClass: boolean[] = [];
  public isFetchingStudents: boolean[] = [];
  public fetching = false;
  public classNameOrder = 'asc';
  public averageProgressOrder = 'none';
  public averageScoreOrder = 'none';

  constructor(private classLearnerProgressService: ClassLearnerProgressService,
              private router: Router) {
  }

  ngOnInit() {
    this.fetching = true;
    this.classLearnerProgressService.getClasses()
      .then(data => {
        this.classLearnerProgresses = data;
        this.classLearnerProgresses.map(classItem => {
          this.isOpenedClass.push(false);
          this.isFetchingStudents.push(false);
        });
        this.fetching = false;
        this.orderByType('classNameOrder');
      })
      .catch((e) => this.fetching = false);
  }

  switchOrder(orderType: string): void {
    for (let i = 0; i < this.isOpenedClass.length; i++) {
      this.isOpenedClass[i] = false;
    }
    switch (orderType) {
      case 'classNameOrder':
        this.classNameOrder = this.classNameOrder === 'asc' ? 'desc' : 'asc';
        this.averageProgressOrder = 'none';
        this.averageScoreOrder = 'none';
        break;
      case 'averageProgressOrder':
        this.averageProgressOrder = this.averageProgressOrder === 'asc' ? 'desc' : 'asc';
        this.classNameOrder = 'none';
        this.averageScoreOrder = 'none';
        break;
      case 'averageScoreOrder':
        this.averageScoreOrder = this.averageScoreOrder === 'asc' ? 'desc' : 'asc';
        this.classNameOrder = 'none';
        this.averageProgressOrder = 'none';
        break;
    }
    this.orderByType(orderType);
  }

  switchOpen(uuidClassLearnerProgress: string, index: number): void {
    if (this.isFetchingStudents[index]) {
      return;
    }
    this.isOpenedClass[index] = !this.isOpenedClass[index];
    if (
      this.isOpenedClass[index] &&
      (
      (this.classLearnerProgresses[index].students && this.classLearnerProgresses[index].students.length === 0) ||
      !this.classLearnerProgresses[index].students)
    ) {
      // fetch students
      this.isFetchingStudents[index] = true;
      this.classLearnerProgressService.getClassStudents(uuidClassLearnerProgress)
        .then(students => {
          this.classLearnerProgresses[index].students = students;
          this.isFetchingStudents[index] = false;
        })
        .catch((e) => this.isFetchingStudents[index] = false);
    }
  }

  orderByType(orderType: string): void {
    let comparator1;
    let comparator2;
    let sortNumber = 1;
    switch (orderType) {
      case 'classNameOrder':
        comparator1 = (a, b) => a.className < b.className;
        comparator2 = (a, b) => a.className > b.className;
        if (this.classNameOrder === 'desc') {
          sortNumber = -1;
        }
        break;
      case 'averageProgressOrder':
        comparator1 = (a, b) => a.averageProgress < b.averageProgress;
        comparator2 = (a, b) => a.averageProgress > b.averageProgress;
        if (this.averageProgressOrder === 'desc') {
          sortNumber = -1;
        }
        break;
      case 'averageScoreOrder':
        comparator1 = (a, b) => a.averageScore < b.averageScore;
        comparator2 = (a, b) => a.averageScore > b.averageScore;
        if (this.averageScoreOrder === 'desc') {
          sortNumber = -1;
        }
        break;
      default:
        return;
    }

    this.classLearnerProgresses.sort((a, b) => {
      if (comparator1(a, b)) {
        return -1 * sortNumber;
      }

      if (comparator2(a, b)) {
        return sortNumber;
      }
      return 0;
    });

  }

  showLearnerProgress(learnerUuid: string) {
    this.router.navigate(['/tutor/progress/learner-progress', learnerUuid]);
  }

}
