import { Component } from '@angular/core';
import { LearnerOverview } from "../../../../model/learner-overview";
import { getLearnerOverview } from "../../../../mocks/learner-admin.mocks";

@Component({
  selector: 'learner-overview',
  templateUrl: 'learner-overview.component.html',
  styleUrls: ['learner-overview.component.css']
})
export class LearnerOverviewComponent{

  public tableType = 'learner-overview';

  public data : LearnerOverview[];

  ngOnInit(){
    this.data = getLearnerOverview();
  }

  requestOptions = {
    name: null,
    dateAdded: null,
    course: null,
    className: null,
  };
}
