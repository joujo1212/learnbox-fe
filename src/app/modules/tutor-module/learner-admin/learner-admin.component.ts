import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'learner-admin',
  templateUrl: 'learner-admin.component.html',
  styleUrls: ['learner-admin.component.css']
})
export class LearnerAdminComponent{

  constructor(
    public router: Router
  ) {
  }
}
