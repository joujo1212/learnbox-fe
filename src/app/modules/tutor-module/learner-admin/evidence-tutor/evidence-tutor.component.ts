/**
 * Created by pati on 16/04/2017.
 */
import {Component, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ActionsRequiredService} from '../../../../services/actionsRequired.service';
import {Endpoints} from '../../../../endpoints';

@Component({
  selector: 'evidence-tutor',
  templateUrl: 'evidence-tutor.component.html',
  styleUrls: ['evidence-tutor.component.scss']
})
export class EvidenceTutorComponent {
  private name: string = 'Paul Player';
  private evidenceName: string = '';
  private tutors: any = ['Matt Everest'];
  private evidence: any = [{
    content: '1.5 Uploaded evidence',
    date: '',
    tutor: '',
    status1: false,
    status2: false,
    fileName: 'machine.mp4',
    file: ''
  }];
  private mock: any = [
    {
      content: 'Paintshop Introduction', date: '', tutor: '', status1: false,
      status2: false,
    },
    {
      content: 'Paintshop Introduction', date: '', tutor: '', status1: false,
      status2: false,
    },
    {
      content: 'Paintshop Introduction', date: '', tutor: '', status1: false,
      status2: false,
    },

  ];

  private notificationUuid: string;
  private evidenceFile: any;
  private videoSource: any;
  private notificationInfo: any = {
    fileInfo: ''
  };

  @ViewChild('downloadHelper') private downloadHelper;

  constructor(private route: ActivatedRoute, private _actions: ActionsRequiredService) {
    this.route.params.subscribe(params => {
      this.notificationUuid = params['notificationUuid'];
      console.log(this.notificationUuid);
      if (typeof this.notificationUuid !== 'undefined') {
        _actions.getNotificationInfo(this.notificationUuid).subscribe(notificationInfo => {
          console.log(notificationInfo);
          this.notificationInfo = notificationInfo;
          this.evidence[0].fileName = this.notificationInfo.fileName;
          this.evidence[0].downloadSource = Endpoints.getEvidenceByID + notificationInfo.fileUuid;
          //   _actions.getEvidence(notificationInfo.fileUuid).then((evidence: any) => {
          //   console.log('ss');
          //   this.evidenceFile = evidence;
          //   console.log(this.evidenceFile);
          //   this.videoSource = evidence._body;
          //
          // });
        });
      }
    });
  }

  submitEvidence(bool, obj) {
    let state = '';
    if (bool) {
      state = 'PASS';
      obj.status2 = false;
    } else {
      state = 'FAIL';
      obj.status1 = false;
    }
    this._actions.submitEvidence(state,
      this.notificationInfo.enrollmentUuid,
      new Date().getTime()).then(data => {});
  }

  retakeEvidence(item) {
    item.status1 = false;
    item.status2 = false;

    this._actions.retakeEvidence(this.notificationInfo.studentUuid, this.notificationInfo.enrollmentUuid).then((data) => {
      console.log(data);
    });
  }
}
