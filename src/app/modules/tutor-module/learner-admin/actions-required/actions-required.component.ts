import {Component, OnInit} from '@angular/core';
import {ActionsRequired} from "../../../../model/actions-required";
import {ActionsRequiredService} from "../../../../services/actionsRequired.service";

@Component({
  selector: 'actions-required',
  templateUrl: 'actions-required.component.html',
  styleUrls: ['actions-required.component.css']
})
export class ActionsRequiredComponent implements OnInit {

  public tableType = 'actions-required';

  public data: any = [];

  private loadingData = true;

  public requestOptions = {
    name: null,
    className: null,
    marking: null,
    status: null,
    issues: null
  };

  constructor(private _actions: ActionsRequiredService) {

  }

  ngOnInit() {
    // TODO finish request on BE and get real data so no mocks will be used
    this._actions.getActionsRequired().subscribe((data) => {
      console.log(data);
      let tmp: any = {}, i = 0;
      while (i < data.length) {
        tmp = {};
        console.log(data[i].studentName);
        tmp.name = data[i].studentName;
        tmp.status = false;
        tmp.className = 'Performing Manufacturing and Operations';
        tmp.marking = 'james_machine_maintenance.mp4';
        tmp.messages = i % 2 === 0 ? 1 : 0;
        tmp.status = Math.random() >= .4;
        tmp.fileUuid = data[i].fileUuid;
        tmp.fileName = data[i].fileName;
        tmp.enrollmentUuid = data[i].enrollmentUuid;
        tmp.userUuid = data[i].studentUuid;
        tmp.notificationType = data[i].notificationType;

        if (Math.random() < .5) {
          tmp.issues = 'Maths Boxed';
          if (Math.random() < .2) {
            tmp.issueSpecial = true;
          }
        } else {
          tmp.issueSpecial = false;

        }

        this.data.push(tmp);
        i++;
      }
      this.loadingData = false;
    });
  }
}
