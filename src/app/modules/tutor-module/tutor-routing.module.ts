import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { TutorComponent } from './tutor/tutor.component';
import { UserDashboardComponent } from '../../shared/user-dashboard/user-dashboard.component';
import { LearnerAdminComponent } from './learner-admin/learner-admin.component';
import { ActionsRequiredComponent } from './learner-admin/actions-required/actions-required.component';
import { LearnerOverviewComponent } from './learner-admin/learner-overview/learner-overview.component';
import { ClassLearnerProgressComponent } from './learner-admin/class-learner-progress/class-learner-progress.component';
import { ClassSetupComponent } from './class-setup/class-setup.component';
import { ProgressComponent } from './progress/progress.component';
import { EvidenceTutorComponent } from './learner-admin/evidence-tutor/evidence-tutor.component';
import { LearnerProgressComponent } from './progress/learner-progress/learner-progress.component';
import { AddLearnerComponent } from '../../shared/add-learner/add-learner.component';
import {ViewResultsComponent} from '../../shared/view-results/view-results-component';
import {AuthGuard} from '../../services/auth-guard';

const tutorRoutes: Routes = [
  {
    path: 'tutor',
    component: TutorComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    children: [
      {
        path: '',
        component: UserDashboardComponent
      },
      {
        path: 'admin',
        component: LearnerAdminComponent,
        children: [
          {
            path: 'actions-required',
            component: ActionsRequiredComponent
          },
          {
            path: 'learner-overview',
            component: LearnerOverviewComponent
          },
          {
            path: 'add-learner',
            component: AddLearnerComponent
          },
          {
            path: 'evidence/:notificationUuid',
            component: EvidenceTutorComponent
          }
        ]
      },
      {
        path: 'progress',
        component: ProgressComponent,
        children: [
          {
            path: 'class-learner-progress',
            component: ClassLearnerProgressComponent
          },
          {
            path: 'learner-progress/:learner',
            component: LearnerProgressComponent
          },
          {
            path: 'view-results/:user/:enrollment',
            component: ViewResultsComponent
          }
        ]
      },
      // {
      //   path: 'chat/:learner',
      //   component: ChatComponent
      // },
      {
        path: 'class-setup',
        component: ClassSetupComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(tutorRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class TutorRoutingModule {
}
