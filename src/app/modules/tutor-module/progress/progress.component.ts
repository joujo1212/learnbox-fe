/**
 * Created by pati on 13/04/2017.
 */
import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'progress-component',
  templateUrl: './progress.component.html',
  styleUrls: ['./progress.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProgressComponent implements OnInit {
  constructor( public router: Router) {
  }

  ngOnInit() {
  }

}
