import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LearnerProgressPortfolio } from '../../../../model/learner-progress-portfolio';
import {ClassLearnerProgressService} from '../../../../services/class-learner-progress.service';


@Component({
  selector: 'learner-progress',
  templateUrl: 'learner-progress.component.html',
  styleUrls: ['learner-progress.component.css']
})
export class LearnerProgressComponent implements OnInit {

  public learnerProfile: LearnerProgressPortfolio;

  constructor(private route: ActivatedRoute,
              private classLearnerProgressService: ClassLearnerProgressService) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      const learnerUuid = params['learner'];
      this.classLearnerProgressService.getLearnerProfile(learnerUuid)
        .then((learnerProfile: LearnerProgressPortfolio) => this.learnerProfile = learnerProfile);
    });
  }


}
