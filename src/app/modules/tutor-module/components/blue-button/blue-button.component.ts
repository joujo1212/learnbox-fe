import { Component, OnChanges, Input, Output } from '@angular/core';

@Component({
  selector: 'blue-button',
  templateUrl: 'blue-button.component.html',
  styleUrls: ['blue-button.component.css']
})
export class BlueButtonComponent implements OnChanges {
  buttonT = '';

  @Input()
  get buttonTitle() {
    return this.buttonT;
  }

  set buttonTitle(title) {
    this.buttonT = title;
  }

  constructor() {
  }

  ngOnChanges() {
  }

}
