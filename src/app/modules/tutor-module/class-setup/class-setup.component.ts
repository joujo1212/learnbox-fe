import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router'
import { LearnerList } from "../../../model/learner-list";
import { ClassSetup } from "../../../model/class-setup";

import { Overlay, overlayConfigFactory } from 'angular2-modal';
import { Modal, BSModalContext } from 'angular2-modal/plugins/bootstrap';
import { CustomModalContext, SuccessModal } from '../../../shared/modals/success-modal/success-modal.component';

@Component({
  selector: 'class-setup',
  templateUrl: 'class-setup.component.html',
  styleUrls: ['class-setup.component.css']
})
export class ClassSetupComponent implements OnInit{

  public learnerList: LearnerList[] = [];
  public classSetUp: ClassSetup = new ClassSetup(null, null, null);
  public tutors = ["M K EVEREST"];
  public classes: string[] = [];
  public selectedClass;
  public selection: boolean = false;

  constructor(
    public router: Router,
    overlay: Overlay,
    vcRef: ViewContainerRef,
    public modal: Modal
  ) {
    overlay.defaultViewContainer = vcRef;
  }

  ngOnInit() {

  }

  createClass(){
    this.classes.push(this.classSetUp.name);
    console.log(this.classes);

    let learner1 = new LearnerList();
    learner1.name = this.classSetUp.student;
    learner1.dateAdded = new Date().getDate() + ' / ' + new Date().getMonth() + ' / ' + new Date().getFullYear();
    learner1.selected = false;
    this.learnerList.push(learner1);

    this.classSetUp = new ClassSetup(null, null, null);
    let context = new CustomModalContext();
    context.title = 'Success!';
    context.body = 'Class was succesfully created!';
    return this.modal.open(SuccessModal, overlayConfigFactory(context, BSModalContext));
  }

  addLearningToClass(){
    this.selection = false;
    this.selectedClass = null;
    let context = new CustomModalContext();
    context.title = 'Success!';
    context.body = 'English course was succesfully added to this class!';
    return this.modal.open(SuccessModal, overlayConfigFactory(context, BSModalContext));
  }

}
