import {Component} from '@angular/core';
import {Router} from '@angular/router'

@Component({
  selector: 'tutor-module',
  templateUrl: 'tutor.component.html',
  styleUrls: ['tutor.component.css']
})
export class TutorComponent {

  private chatOpened: any = false;

  constructor(public router: Router) {
  };

  toggleChat(val) {
    this.chatOpened = val;
  }
}
