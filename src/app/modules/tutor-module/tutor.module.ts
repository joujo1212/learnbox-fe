import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TutorRoutingModule } from './tutor-routing.module';
import { TutorComponent } from './tutor/tutor.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LearnerAdminComponent } from './learner-admin/learner-admin.component';
import { ActionsRequiredComponent } from './learner-admin/actions-required/actions-required.component';
import { LearnerOverviewComponent } from './learner-admin/learner-overview/learner-overview.component';
import { ClassLearnerProgressComponent } from './learner-admin/class-learner-progress/class-learner-progress.component';
import { ClassSetupComponent } from './class-setup/class-setup.component';

import { ChartsModule } from 'ng2-charts';
import { SharedModule } from '../../shared/shared.module';
import { ModalModule } from 'angular2-modal';
import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';
import {ProgressComponent} from './progress/progress.component';
import {DndModule} from 'ng2-dnd';
import {LearnerProgressComponent} from './progress/learner-progress/learner-progress.component';
import { StompService } from 'ng2-stomp-service';
import { EvidenceTutorComponent } from './learner-admin/evidence-tutor/evidence-tutor.component';
// import { BlueButtonComponent } from 'app/modules/tutor-module/components/blue-button/blue-button.component';
import {ActionsRequiredService} from '../../services/actionsRequired.service';
import { TutorService } from '../../services/tutor.service';
import { OnBoardService } from '../../services/on-board.service';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule.forRoot(),
    TutorRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    ChartsModule,
    SharedModule,
    ModalModule.forRoot(),
    BootstrapModalModule

  ],
  declarations: [
    TutorComponent,
    LearnerAdminComponent,
    ActionsRequiredComponent,
    LearnerOverviewComponent,
    ClassLearnerProgressComponent,
    ClassSetupComponent,
    ClassLearnerProgressComponent,
    ProgressComponent,
    LearnerProgressComponent,
    EvidenceTutorComponent
  ],
  providers: [StompService, ActionsRequiredService, TutorService, OnBoardService]

})
export class TutorModule {}
