import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-progress-bar-tutor-examiner',
  templateUrl: 'progress-bar-tutor-examiner.component.html',
  styleUrls: ['progress-bar-tutor-examiner.component.css']
})
export class ProgressBarTutorExaminerComponent implements OnInit {


  private _status: number;
  private _pass: number;
  statusTextPosition: number;
  passTextPosition: number;

  constructor() {
  }

  ngOnInit() {
  }


  private isIntersectText(width1: number, position1: number, width2: number, position2: number) {
    let right1 = (position1 + (width1 / 2));
    let left1 = (position1 - (width1 / 2));

    let right2 = (position2 + (width2 / 2));
    let left2 = (position2 - (width2 / 2));

    let isIntersected = left1 < right2 && left2 < right1;
    return isIntersected;
  }

  private isIntersectPercent(value1: number, value2: number) {
    return Math.abs(value1 - value2) <= 5;
  }

  private getPositionOfText(value: number, textLength: number): number {
    return (((816 - textLength) * value) / 100) + (textLength / 2);
  }

  @Input()
  set status(value: number) {

    this._status = value;
    this.statusTextPosition = this.getPositionOfText(this._status, 180);
  }

  get status(): number {
    return this._status;
  }

  get pass(): number {
    return this._pass;
  }

  @Input()
  set pass(value: number) {

    this._pass = value;
    this.passTextPosition = this.getPositionOfText(this._pass, 140);
  }

}
