import {Component, Input, OnInit} from '@angular/core';
import {DashboardItemData} from "../../model/dashboard-item-data";
import {Router} from "@angular/router";
import {MdSidenav} from "@angular/material";
import {OnBoardService} from "../../services/on-board.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  @Input()
  nav: MdSidenav;

  /**
   * Type of dashboard, either 'student' or 'tutor
   */
  @Input()
  type: String;

  dashboardItemsData: {[id: string]: DashboardItemData} = {};

  constructor(private router: Router,
              private onBoardService: OnBoardService) {
    //home
    this.dashboardItemsData["home"] = new DashboardItemData();
    this.dashboardItemsData["home"].icon = "home";
    this.dashboardItemsData["home"].text = "home";
    this.dashboardItemsData["home"].redirectLink = "/student/tutorial/active";

    //course overview
    this.dashboardItemsData["courseOverview"] = new DashboardItemData();
    this.dashboardItemsData["courseOverview"].icon = "list";
    this.dashboardItemsData["courseOverview"].text = "course overview";
    this.dashboardItemsData["courseOverview"].redirectLink = "/student/overview";

    //learning portfolio
    this.dashboardItemsData["learningPortfolio"] = new DashboardItemData();
    this.dashboardItemsData["learningPortfolio"].icon = "data_usage";
    this.dashboardItemsData["learningPortfolio"].text = "learning portfolio";
    this.dashboardItemsData["learningPortfolio"].redirectLink = "/student/portfolio";

    //help
    this.dashboardItemsData["help"] = new DashboardItemData();
    this.dashboardItemsData["help"].icon = "help";
    this.dashboardItemsData["help"].text = "help";
    this.dashboardItemsData["help"].redirectLink = "/student/help";

    //logout
    this.dashboardItemsData["logout"] = new DashboardItemData();
    this.dashboardItemsData["logout"].icon = "exit_to_app";
    this.dashboardItemsData["logout"].text = "logout";
  }

  ngOnInit() {
  }

  redirectTo(dashboardItemData: DashboardItemData): void {
    this.router.navigate([dashboardItemData.redirectLink])
      .then(() => {
        this.closeDashboard();
      });
  }

  closeDashboard(): void {
    this.nav.close();
  }

  logoutUser() {
    this.onBoardService.logout();
  }


}
