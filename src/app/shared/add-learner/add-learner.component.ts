import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {AddUser} from '../../model/add-user';
import {TutorService} from '../../services/tutor.service';
import {Modal} from 'angular2-modal/plugins/bootstrap';
import {Router} from '@angular/router';
import {OnBoardService} from '../../services/on-board.service';
import {UserService} from '../../services/user.service';
import {ModalService} from '../../services/modal.service';
import {ClassService} from '../../services/class.service';

@Component({
  selector: 'add-learner',
  templateUrl: 'add-learner.component.html',
  styleUrls: ['add-learner.component.css']
})

export class AddLearnerComponent implements OnInit {
  public tableType = 'add-user';
  public selectedRole = 'STUDENT';
  public addLearner = new AddUser();
  public newLearners: AddUser[] = [];

  @ViewChild('fileInput')
  fileInput: any;

  public addMultiple = false;
  public multiple: any = {};
  public userRole: string;

  constructor(
    private _userService: UserService,
    private _tutorService: TutorService,
    private _onBoardService: OnBoardService,
    private _modalService: ModalService,
    private _classService: ClassService,
    private vcRef: ViewContainerRef,
    public modal: Modal,
    private router: Router
  ) {};

  ngOnInit(): void {
    this.multiple.className = '';
    this.multiple.learners = [];
    this.showUsers(this.selectedRole);
    this.userRole = this._onBoardService.getUserRole();
  }

  /**
   * Add single student
   * @param event of submit of form
   */
  inviteStudent(event: Event) {
    event.preventDefault();
    const pojo = [{
      email: this.addLearner.email,
      name: this.addLearner.name,
      birthday: this.addLearner.birthday,
      genderType: this.addLearner.genderType,
      company: this.addLearner.company,
      street: this.addLearner.street,
      city: this.addLearner.city,
      zipCode: this.addLearner.zipCode,
      phoneNumber: this.addLearner.phoneNumber
    }];
    this._userService.inviteStudent(pojo).then(
      response => {
        this._modalService.showSimpleModal('Success!', 'Learner was successfully invited!', 'success', this.vcRef);
      },
      error => {
        this._modalService.showSimpleModal('Error!', 'There has been an error, please try again!', 'error', this.vcRef);
      }
    );

    // reset form
    this.addLearner = new AddUser();
  }

  /**
   * Add multiple students by file
   * @param event of upload with file
   */
  inviteMultipleStudents(event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file: File = fileList[0];
      this._tutorService.getUsersByFile(file).then(
        response => {
          this.addMultiple = true;
          this.multiple.learners = response;
          // reset input
          this.fileInput.nativeElement.value = '';
        },
        error => {
          this._modalService.showSimpleModal('Error!', 'There has been an error, please try again.', 'error', this.vcRef);
          // reset input
          this.fileInput.nativeElement.value = '';
        }
      );
    }
  }

  /**
   * Creates students from multiple file upload, then creates class from them
   */
  saveAndContinue() {
    this._userService.inviteStudent(this.multiple.learners).then(
      (response: any) => {
        if (this.multiple.className !== '') {
          const studentUuids = [];
          const tutorUuid = this._onBoardService.getUserUuid();
          for (let i = 0; i < response.length; i++) {
            studentUuids.push(response[i].uuid);
          }
          const body = {
            name: this.multiple.className,
            studentUuids: studentUuids,
            tutorUuid: tutorUuid
          };
          this._classService.createClass(body).then(
            classResponse => {
              this._modalService.showSimpleModal('Success!', 'Learners were successfully invited and added to class '
                + this.multiple.className + '!', 'success', this.vcRef);
            },
            error => {
              this._modalService.showSimpleModal('Error!', 'There has been an error, please try again.', 'error', this.vcRef);
            }
          );
        } else {
          this._modalService.showSimpleModal('Success!', 'Learners were successfully invited!', 'success', this.vcRef);
        }
      },
      error => {
        this._modalService.showSimpleModal('Error!', 'There has been an error, please try again.', 'error', this.vcRef);
      }
    );
  }

  /**
   * Load users by filter, used in right list with all existing users
   * @param type of users, EXAMINER or TUTOR or STUDENT
   */
  showUsers(type) {
    const pojo = {
      roles: [type],
      pageRequestPojo: { page: 0}};
    this._userService.getUsersByFilter(pojo).then(
      response => {
        this.newLearners = response.content;
      },
      error => {
        console.log(error);
      }
    );
  }

  /**
   * When changed tab in right list with all existing users, load other users
   * @param event
   */
  onTabClick(event) {
    if (event.index === 0) {
      this.selectedRole = 'EXAMINER';
      this.showUsers(this.selectedRole);
    }
    if (event.index === 1) {
      this.selectedRole = 'TUTOR';
      this.showUsers(this.selectedRole);
    }
    if (event.index === 2) {
      this.selectedRole = 'STUDENT';
      this.showUsers(this.selectedRole);
    }
  };
}
