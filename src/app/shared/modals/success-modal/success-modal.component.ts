import { Component } from '@angular/core';

import { DialogRef, ModalComponent } from 'angular2-modal';
import { BSModalContext } from 'angular2-modal/plugins/bootstrap';

export class CustomModalContext extends BSModalContext {
  public title: string;
  public body: string;
}

@Component({
  selector: 'success-modal-content',
  templateUrl: 'success-modal.component.html',
  styleUrls: ['success-modal.component.css']
})
export class SuccessModal implements ModalComponent<CustomModalContext> {
  context: CustomModalContext;

  constructor(public dialog: DialogRef<CustomModalContext>) {
    this.context = dialog.context;
  }

  closeModal() {
    this.dialog.close();
  }
}
