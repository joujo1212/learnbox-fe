import {Component, Input} from '@angular/core';

@Component({
  selector: 'black-full-width-button',
  templateUrl: './black-full-width-button.component.html',
  styleUrls: ['./black-full-width-button.component.css']
})
export class BlackFullWidthButtonComponent {
  @Input()
  text: String;

  @Input()
  height: number;
}
