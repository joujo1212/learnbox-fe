/**
 * Created by pati on 21/04/2017.
 */
import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'loading',
  templateUrl: 'loading.component.html',
  styleUrls: ['loading.component.scss']
})
export class LoadingComponent {

  @Input('state')
  private state: boolean;

}
