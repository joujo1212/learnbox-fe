/**
 * Created by pati on 14/04/2017.
 */
import {Component, OnInit, Input, OnChanges, DoCheck, Output, EventEmitter} from '@angular/core';
import {Http} from "@angular/http";
import {StompService} from 'ng2-stomp-service';
import {JwtHelper} from "angular2-jwt";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {ActivatedRoute} from '@angular/router';
import {ChatService} from "../../services/chat.service";
import {Endpoints} from '../../endpoints';


declare var SockJS: any;

@Component({
  selector: 'chat',
  templateUrl: 'chat.component.html',
  styleUrls: ['chat.component.scss']
})
export class ChatComponent implements OnInit {
  private text: string = '';
  private contacts = [];
  private messages = [
    // {text: 'Hello, how are you doing?', type: 'tutor', time: '01:17'},
    // {text: 'Hello, great!', type: 'learner', time: '01:18'},
    // {text: 'If you need help with anything, don\'t hesitate and feel free to ask me!', type: 'tutor', time: '01:19'},
  ];
  private wsConf: any = {
    host: Endpoints.chatWS,
    // host: 'http://192.168.43.77:8000/chat',
    recTimeout: 30000,
    debug: true,
    headers: {
      Authorization: '',
      to: null
    },
    heartbeatIn: 0,
    heartbeatOut: 0
  };
  learner: string;
  private sub: any;
  public jwtHelper: JwtHelper = new JwtHelper();
  public uuid;
  public jwtToken;
  private studentName = 'Alex Humphries';
  private tutorName = 'Matt Everes';
  private searchText = '';
  @Input('shown') shown;
  @Input('isStudent') isStudent;
  @Output('closeChatEmitter') closeChatEmitter = new EventEmitter;
  private onlineIndicator: any = true;
  private whoToChat: string;

  private chatRooms;
  private activeRoomUuid = '';

  constructor(private http: Http,
              private stomp: StompService,
              private route: ActivatedRoute,
              private _chat: ChatService) {
    // this.jwtToken = localStorage.getItem('currentUser');
    // this.jwtToken = JSON.parse(this.jwtToken).token;
    // this.uuid = this.jwtHelper.decodeToken(this.jwtToken).uuid;

    this.jwtToken = localStorage.getItem('access_token');
    this.uuid = localStorage.getItem('uuid');

    if (this.jwtToken) {
      this.wsConf.headers.Authorization = 'Bearer ' + this.jwtToken;
      this.wsConf.headers.to = 3;
      this.stomp.configure(this.wsConf);

      this._chat.getChatroomInfo(0).subscribe(data => {
        console.log(data);
        this.chatRooms = data;
        this.turnChatroomsToContacts(data);
      });
      this.connectSocket();
    }
  }

  connectSocket() {
    this.stomp.startConnect().then(() => {
    }).catch((ex) => {
      console.error(ex);
    });
  }

  startSubscription(chatroomUuid) {
    this.activeRoomUuid = chatroomUuid;
    this.stomp.subscribe('chat.' + chatroomUuid, e => {
      // console.log(e);
      this.addMessageFromSubscribe(e);
    }, this.wsConf.headers);
  }

  turnChatroomsToContacts(chatRooms) {
    let i = 0;

    while (i < chatRooms.length) {
      const tempContact = {
        name: 'Matt - BE missing name',
        active: false,
        online: true,
        unreadCount: chatRooms[i].unreadMessages,
        chatRoomUuid: chatRooms[i].chatRoomUuid,
        users: chatRooms[i].users
      };
      this.contacts.push(tempContact);
      i++;
    }
  }

  selectContact(contact) {
    let i = 0;
    while (i < this.contacts.length) {
      this.contacts[i].active = false;
      i++;
    }
    contact.active = true;
    this.onlineIndicator = contact.online;
    this.whoToChat = contact.name;
    this.activeRoomUuid = contact.chatRoomUuid;
    this.startSubscription(contact.chatRoomUuid);
  }

  ngOnInit() {
    // MOCK !!!
    // if (this.isStudent) {
    //   this.contacts = [{
    //     name: 'Matt Everest',
    //     active: true,
    //     online: false,
    //     unreadCount: 0
    //   }];
    // } else {
    //   this
    //     .
    //     contacts = [{
    //     name: 'Alex Humphries',
    //     active: true,
    //     online: true,
    //     unreadCount: 0
    //   }];
    // }
    // this.whoToChat = this.contacts[0].name;
    // this.onlineIndicator = this.contacts[0].online;
    // END MOCK
  }


  getInitials(val) {
    const studentNameSplitted = val.split(' ');
    return studentNameSplitted[0].substring(0, 1) + studentNameSplitted[1].substring(0, 1);
  }


  closeChat() {
    this.closeChatEmitter.emit();
  }

  addMessageFromSubscribe(e) {
    console.log(e);
    if (e.author !== this.uuid && this.activeRoomUuid === e.chatRoomUuid) {
      const time = new Date(e.timestamp);
      const myTime = time.getHours() + ':' + time.getMinutes();

        const message = {
          text: e.body,
          type: 'learner',
          time: myTime
        };
        this.messages.push(message);

    }
  }

  /**
   * Send message
   */
  addMessage() {
    const time = new Date();
    const myTime = time.getHours() + ':' + time.getMinutes();
    const message = {
      text: this.text,
      type: 'tutor', // tutor means me (doesnt matter if student or not!)
      time: myTime
    };
    this.messages.push(message);

    const stompMessage = {
      author: this.uuid,
      chatRoomUuid: this.activeRoomUuid,
      body: this.text,
      timestamp: new Date().getTime()
    };

    this.stomp.send('/chat/message', stompMessage, this.wsConf.headers);
    this.text = '';

  }
}
