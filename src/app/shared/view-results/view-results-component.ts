import {Component, trigger, state, style, transition, animate, OnInit} from '@angular/core';
import {TutorialService} from '../../services/tutorial.service';
import { ActivatedRoute } from '@angular/router';
import {ActionsRequiredService} from '../../services/actionsRequired.service';

@Component({
  selector: 'view-results-component',
  templateUrl: 'view-results-component.html',
  styleUrls: ['view-results-component.scss'],
  animations: [
    trigger('OpacityAnimation', [
      state('void', style({
        opacity: 0
      })),
      state('*', style({
        opacity: 1
      })),
      transition('void => *', animate('300ms ease-in')),
    ])
  ]
})
export class ViewResultsComponent implements OnInit {
  private quizName: string = 'MATHS BOXED | Quiz 01';
  private quizzes = {passMark: 0, learnerScore: 0};
  private pass: number = 0;
  private score: number = 0;
  private loadingItems: boolean = true;

  private userUuid: string;
  private enrollmentUuid: string;
  private sub: any;

  constructor(
    private _tutorialService: TutorialService,
    private route: ActivatedRoute,
    private _actions: ActionsRequiredService
  ) {
    this.sub = this.route.params.subscribe(params => {
      this.enrollmentUuid = params['enrollment'];
      this.userUuid = params['user'];
    });
  }

  ngOnInit() {
    // load results of student according to enrollmentUuid and userUuid
    this._tutorialService.viewResultsForTutorial(this.userUuid, this.enrollmentUuid).then(
      (res) => {
        this.quizzes = res;
        this.pass = this.quizzes.passMark;
        this.score = this.quizzes.learnerScore;
        this.loadingItems = false;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  reSubmit() {
    this._actions.retakeEvidence(this.userUuid, this.enrollmentUuid).then((data) => {
      console.log(data);
    });
  }
}
