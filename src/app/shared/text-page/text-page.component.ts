import { Component, Input, OnInit, ViewChild } from '@angular/core';
import {MdSidenav} from '@angular/material';
import {OnBoardService} from '../../services/on-board.service';

@Component({
  selector: 'generic-text-page',
  templateUrl: 'text-page.component.html',
  styleUrls: ['text-page.component.scss']
})
export class TextPageComponent implements OnInit {

  @Input() json = null;
  @Input() innerHTML = null;
  @Input() masterTitle = '';
  private entries: Array<[string, string]>;
  private chatOpened: any = false;
  @ViewChild('sidenav') sidenav: MdSidenav;

  constructor(private _onboardService: OnBoardService) {}

  ngOnInit() {
    this.entries = new Array<[string, string]>();

    if (this.json !== null) {
      for (const item of this.json) {
        if (item === null) { continue; }
        const itemType = item.split(' ')[0];
        switch (itemType) {
          case 'MASTER':
            this.masterTitle = item.substring(7, item.length);
            this.entries.push(['MASTER', this.masterTitle]);
            break;
          case 'SUBTITLE':
            this.entries.push(['SUBTITLE', item.substring(9, item.length)]);
            break;
          case 'TEXT':
            this.entries.push(['TEXT', item.substring(5, item.length)]);
            break;
          case 'LINK':
            this.entries.push(['LINK', item.substring(5, item.length)]);
            break;
        }
      }
    }
  }

  getMasterTitle(): string {
    return this.masterTitle;
  }

  getEntries(): Array<[string, string]> {
    return this.entries;
  }

  getLinkURL(str: string): string {
    return str.split(' ')[0];
  }

  getLinkText(str: string): string {
    return str.split(' ')[1];
  }

  getUserType(): string {
    if (this._onboardService.getUserRole() === null || typeof this._onboardService.getUserRole() === 'undefined') {
      return '';
    }

    let type = this._onboardService.getUserRole().toLowerCase();
    if (type.indexOf('super') !== -1) { type = 'superUser'; }
    return type;
  }

  public toggleChat(val) {
    this.chatOpened = val;
  }
}
