import {Component, Input, OnInit, OnChanges, SimpleChanges, SimpleChange} from '@angular/core';
import {Router} from '@angular/router';
import {TutorService} from '../../services/tutor.service';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'infinity-scroll-table',
  templateUrl: './infinity-scroll-table.component.html',
  styleUrls: ['./infinity-scroll-table.component.css']
})
export class InfinityScrollTableComponent implements OnInit, OnChanges {

  @Input()
  tableType: String;

  @Input()
  role: String;

  @Input()
  requestOptions: any;

  @Input()
  tableData: any;

  @Input()
  loadingState: boolean;

  private pageNumber = 0;

  constructor(
    private _router: Router,
    private _userService: UserService
  ) {}

  ngOnInit() {
    console.log(this);
  }

  ngOnChanges(changes: SimpleChanges) {
    // if type of shown users changes, reset page number for scrolling
    if (changes.hasOwnProperty('role')) {
      this.pageNumber = 0;
    }
  }

  sortOptions(type) {
    if (this.requestOptions[type] == null) {
      this.requestOptions[type] = 'desc';
    } else if (this.requestOptions[type] === 'desc') {
      this.requestOptions[type] = 'asc';
    } else if (this.requestOptions[type] === 'asc') {
      this.requestOptions[type] = 'desc';
    }
  }

  onScroll() {
    if (this.tableType === 'add-learner') {
      this.pageNumber++;
      const pojo = {
        roles: [this.role],
        pageRequestPojo: { page: this.pageNumber}};
        this._userService.getUsersByFilter(pojo).then(
          response => {
            this.tableData = this.tableData.concat(response.content);
          },
          error => {
            console.log(error);
          }
        );
    } else {
      this.tableData = this.tableData.concat(this.tableData);
    }
  }

  routing(route, data) {
    if (route === 'chat') {
      this._router.navigate(['/tutor/chat', data]);
    } else if (route === 'view_results') {
      this._router.navigate(['/tutor/progress/view-results', data.userUuid, data.enrollmentUuid]);
    } else if (route === 'evidence') {
      this._router.navigate(['/tutor/admin/evidence', data]);
    }
  }

}
