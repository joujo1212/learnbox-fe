import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'black-button',
  templateUrl: './black-button.component.html',
  styleUrls: ['./black-button.component.css']
})
export class BlackButtonComponent implements OnInit {

  @Input()
  text: String;

  @Input()
  icon: String;

  @Input()
  active: boolean;

  @Input()
  class_: String;

  constructor(
  ) {
  }

  ngOnInit() {

  }


}
