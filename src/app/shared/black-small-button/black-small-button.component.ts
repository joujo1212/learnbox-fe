import {Component, Input} from '@angular/core';

@Component({
  selector: 'black-small-button',
  templateUrl: './black-small-button.component.html',
  styleUrls: ['./black-small-button.component.css']
})
export class BlackSmallButtonComponent {
  @Input()
  text: String;

  @Input()
  height: number = 40;

  @Input()
  width: number = 180;

  @Input()
  type: string = 'button';
}
