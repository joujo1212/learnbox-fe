import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '@angular/material';
import {FlexLayoutModule} from '@angular/flex-layout';
import {InfiniteScrollModule} from 'angular2-infinite-scroll';
import {ChartsModule} from 'ng2-charts';

import {TopBarComponent} from './top-bar/top-bar.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {DashboardItemComponent} from './dashboard-item/dashboard-item.component';
import {BlackButtonComponent} from './black-button/black-button.component';
import {InfinityScrollTableComponent} from './infinity-scroll-table/infinity-scroll-table.component';
import {ProgressBarComponent} from './progress-bar/progress-bar.component';
import {BlackSmallButtonComponent} from './black-small-button/black-small-button.component';
import {LearningPortfolioComponent} from './learning-portfolio/learning-portfolio.component';
import {LoadingComponent} from './loading/loading.component';
import {ChatComponent} from './chat/chat.component';
import {FormsModule} from '@angular/forms';
import {ViewResultsComponent} from './view-results/view-results-component';
import {TutorExaminerProjectionComponent} from './tutor-examiner-projection/tutor-examiner-projection.component';
import {ProgressBarTutorExaminerComponent} from './progress-bar-tutor-examiner/progress-bar-tutor-examiner.component';
import {BlueButtonComponent} from './blue-button/blue-button.component';
import {DndModule} from 'ng2-dnd';
import {ChatService} from '../services/chat.service';
import {ModalService} from '../services/modal.service';
import {ClassService} from '../services/class.service';
import {UserDashboardComponent} from './user-dashboard/user-dashboard.component';
import {CalculatorComponent} from './calculator/calculator.component';
import {GreyContainerComponent} from './grey-container/grey-container.component';
import {CourseLogoComponent} from './course-logo/course-logo.component';
import {ThumbPipe} from './_pipes/thumb.pipe';
import {AddLearnerComponent} from './add-learner/add-learner.component';
import {ImageUploadModule} from 'ng2-imageupload';
import {TextPageComponent} from './text-page/text-page.component';
import {BlackFullWidthButtonComponent} from './black-full-width-button/black-full-width-button.component';
import {SuccessModal} from './modals/success-modal/success-modal.component';
import {ErrorModal} from './modals/error-modal/error-modal.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule.forRoot(),
    FlexLayoutModule,
    InfiniteScrollModule,
    FormsModule,
    DndModule.forRoot(),
    ChartsModule,
    ImageUploadModule
  ],
  declarations: [
    TopBarComponent,
    DashboardComponent,
    DashboardItemComponent,
    BlackButtonComponent,
    InfinityScrollTableComponent,
    ProgressBarComponent,
    BlackSmallButtonComponent,
    LearningPortfolioComponent,
    LoadingComponent,
    ChatComponent,
    UserDashboardComponent,
    ViewResultsComponent,
    TutorExaminerProjectionComponent,
    ProgressBarTutorExaminerComponent,
    BlueButtonComponent,
    AddLearnerComponent,
    BlackFullWidthButtonComponent,
    CalculatorComponent,
    GreyContainerComponent,
    CourseLogoComponent,
    ThumbPipe,
    AddLearnerComponent,
    TextPageComponent,
    SuccessModal,
    ErrorModal
  ],
  providers: [ChatService, ModalService, ClassService],
  exports: [
    TopBarComponent,
    DashboardComponent,
    DashboardItemComponent,
    BlackButtonComponent,
    InfinityScrollTableComponent,
    ProgressBarComponent,
    BlackSmallButtonComponent,
    LearningPortfolioComponent,
    LoadingComponent,
    ChatComponent,
    ViewResultsComponent,
    TutorExaminerProjectionComponent,
    ProgressBarTutorExaminerComponent,
    BlueButtonComponent,
    UserDashboardComponent,
    CalculatorComponent,
    GreyContainerComponent,
    CourseLogoComponent,
    ThumbPipe,
    AddLearnerComponent,
    TextPageComponent,
    BlackFullWidthButtonComponent,
    SuccessModal,
    ErrorModal
  ],
  entryComponents: [ SuccessModal, ErrorModal ]
})
export class SharedModule {
}
