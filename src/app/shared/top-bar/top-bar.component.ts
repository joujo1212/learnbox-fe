import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {MdSidenav} from '@angular/material';
import {Router} from '@angular/router';
import {CourseLogo} from '../../model/course-logo';
import {CourseService} from '../../services/course.service';
import {OnBoardService} from '../../services/on-board.service';

@Component({
  selector: 'app-top-bar',
  templateUrl: 'top-bar.component.html',
  styleUrls: ['top-bar.component.scss']
})
export class TopBarComponent implements OnInit {

  private static CONDENSED_MENU_BREAKPOINT = 650;

  @Input()
  nav: MdSidenav;

  /**
   * Type of top-bar, either 'student' or 'tutor
   */
  @Input()
  type: String;

  /**
   * Event emitter to trigger open on chat
   * @type {EventEmitter}
   */
  @Output()
  chatOpenEmitter: any = new EventEmitter();

  public courseLogo: CourseLogo;

  constructor(private router: Router,
              private onBoardService: OnBoardService,
              private courseService: CourseService) {
  }

  ngOnInit(): void {
    if (this.type === 'student') {
      this.courseService.getCourseLogo()
        .then((courseLogo: CourseLogo) => {
          this.courseLogo = courseLogo;
        });
    }
  }

  openChat() {
    this.chatOpenEmitter.emit();
  }

  /**
   * Method redirect user base on type
   * after click on LearnBox logo
   */
  redirectToUrl() {
    switch (this.type) {
      case 'tutor': {
        this.router.navigateByUrl('/tutor');
        break;
      }
      case 'student': {
        this.router.navigateByUrl('/student/tutorial/active');
        break;
      }
      case 'superUser': {
        this.router.navigateByUrl('/super-user');
        break;
      }
      case 'examiner': {
        this.router.navigateByUrl('/examiner');
        break;
      }
      default: {
        this.router.navigateByUrl('/');
        break;
      }
    }
  }

  shouldCondenseMenu(): boolean {
    return window.innerWidth <= TopBarComponent.CONDENSED_MENU_BREAKPOINT;
  }

  shouldShowLearnboxLogo(): boolean {
    if (this.type !== 'student' || typeof this.courseLogo === 'undefined') {
      return true;
    } else {
      return !this.shouldCondenseMenu();
    }
  }

  shouldShowCourseLogo(): boolean {
    return this.type === 'student' && typeof this.courseLogo !== 'undefined';
  }

  shouldShowMessages(): boolean {
    return this.type === 'student' || this.type === 'tutor';
  }

  shouldShowMenu(): boolean {
    return this.type === 'student';
  }

  shouldShowLogout(): boolean {
    return this.type.length > 0 && this.type !== 'student';
  }

  public logout() {
    this.onBoardService.logout();
  }
}
