import {Component, Input, OnInit} from '@angular/core';
import {DashboardItemData} from "../../model/dashboard-item-data";

@Component({
  selector: 'app-dashboard-item',
  templateUrl: './dashboard-item.component.html',
  styleUrls: ['./dashboard-item.component.css']
})
export class DashboardItemComponent implements OnInit {

  @Input()
  dashboardItemData: DashboardItemData;

  constructor() { }

  ngOnInit() {
  }

}
