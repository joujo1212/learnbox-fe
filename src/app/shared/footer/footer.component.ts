import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'global-footer',
  templateUrl: 'footer.component.html',
  styleUrls: ['footer.component.scss']
})
export class FooterComponent implements OnInit {

  public fontSize;

  ngOnInit() {
    this.updateFontSize();
  }

  updateFontSize() {
    if (window.innerWidth <= 360) {
      this.fontSize = '9px';
    } else if (window.innerWidth <= 450) {
      this.fontSize = '11px';
    } else {
      this.fontSize = '12px';
    }
  }

}
