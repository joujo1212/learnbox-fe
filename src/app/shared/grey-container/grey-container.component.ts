import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-grey-container',
  templateUrl: './grey-container.component.html',
  styleUrls: ['./grey-container.component.css']
})
export class GreyContainerComponent implements OnInit {

  /**
   * Background of container, it can be GREEN or nothing for default grey color
   */
  @Input()
  color: string;

  /**
   * Custom style like object
   */
  @Input()
  style: {};

  ngOnInit() {}

}
