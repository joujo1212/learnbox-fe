/**
 * Created by pati on 27/02/2017.
 */
import {
  Component, Input, OnChanges, AfterContentChecked
} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

declare let document: any;

@Component({
  selector: 'tutor-examiner-projection',
  templateUrl: 'tutor-examiner-projection.component.html',
  styleUrls: ['tutor-examiner-projection.component.scss']
})
export class TutorExaminerProjectionComponent implements OnChanges, AfterContentChecked {

  @Input('htmlData') htmlData: any;
  @Input('secretDataIn') secretDataIn: any;
  @Input('dnd1Data') dndData: any = [];
  @Input('categoriesData') categoriesData: any = [{name: '', items: []}, {name: '', items: []}];
  @Input('checkboxData') checkboxData: any = [];
  @Input('radioData') radioData: any = [];
  @Input('groupsData') groupsData: any = [];
  @Input('categoryNumber') categoryNumber: any = 0;
  @Input('keyUpEventOnly') keyUpEventOnly = false;
  private keyupHold = false;
  private stopContentCheckMethod: boolean = false;
  private secretData: any = [];
  private cat1items = [];
  private cat2items = [];
  private loading = true;
  counter = 0;
  secretHolder = [];

  constructor(private _sanitizer: DomSanitizer) {
  }

  /**
   * handling secret evaluation
   */
  ngAfterContentChecked(): void {
    if (!this.stopContentCheckMethod) {
      let nullCheck = this.secretInputsFiller();
      let elementsToRemove: any,
        i = 0;
      if (this.categoryNumber == 0) {
        let elementsToDisable = document.querySelectorAll('#lecture_player1 .secretInputFromWord');
        while (i < elementsToDisable.length) {
          elementsToDisable[i].disabled = true;
          let j = 0;
          while (j < this.secretDataIn.length) {
            if (this.secretDataIn[j].answerId == elementsToDisable[i].id) {
              if (this.secretDataIn[j].correct) {
                elementsToDisable[i].style.background = '#8bc34a';
              } else {
                elementsToDisable[i].style.background = '#ff584d';
              }
            }
            j++;
          }
          i++;
        }
      }
      // loading starting in onChanges
    }
    this.loading = false;
  }

  /**
   * when true is found make everything true ! just in radio answer case
   * @param data
   * @param i
   * @returns {boolean}
   */
  radioFix(data, i) {
    let condition: boolean = data[i].checked == data[i].correct;
    let j = 0;
    if (condition) {
      while (j < data.length) {
        if (i != j) {
          data[j].correct = true;
        }
        j++;
      }
    }
    return condition;
  }

  /**
   * filling data into secrets
   */
  secretInputsFiller() {
    // console.log(this.secretDataIn);
    if (typeof this.secretDataIn != 'undefined' && this.categoryNumber == 0) {
      let i = 0;
      while (i < this.secretDataIn.length) {
        let id = this.secretDataIn[i].answerId;
        let tmp = document.getElementById(id);

        if (tmp != null) {
          tmp.value = this.secretDataIn[i].value;
          // // console.log(typeof tmp == "object",i+1, this.secretDataIn.length);
          if (i + 1 == this.secretDataIn.length && typeof tmp == "object") {
            this.stopContentCheckMethod = true;
          }
        } else {

        }
        i++;
      }
    }
  }

  /**
   *
   * @param changes
   */
  ngOnChanges(changes: any): void {
    if (typeof changes.keyUpEventOnly != 'undefined') {
      this.keyupHold = changes.keyUpEventOnly.currentValue;
    }
    if (!this.keyupHold) {
      this.loading = true;
      this.stopContentCheckMethod = false;
    }
    this.htmlData = this._sanitizer.bypassSecurityTrustHtml(this.htmlData);
  }

  // TODO pri realnej projekcii naplnit categoriesshuffleddata :)
  /**
   * maybe this one will be removed soon
   * @param $event
   */
  transferDataSuccess($event) {
    if ($event.mouseEvent.target.id == 'cat1') {

      let sameArrayTest = this.deleteItemFromArray($event.dragData.id, this.cat1items);
      if (sameArrayTest != null) {
        this.cat1items.push(sameArrayTest);
        return;
      }
      let draggedItem = this.deleteItemFromArray($event.dragData.id, this.categoriesData[0].items);
      let draggedItem1 = this.deleteItemFromArray($event.dragData.id, this.cat2items);
      if (draggedItem != null) {
        this.cat1items.push(draggedItem);
      } else if (draggedItem1 != null) {
        this.cat1items.push(draggedItem1);

      } else {
        this.cat1items.push(this.deleteItemFromArray($event.dragData.id, this.categoriesData[1].items));
      }
    } else {
      let sameArrayTest = this.deleteItemFromArray($event.dragData.id, this.cat2items);
      if (sameArrayTest != null) {
        this.cat2items.push(sameArrayTest);
        return;
      }
      let draggedItem = this.deleteItemFromArray($event.dragData.id, this.categoriesData[0].items);
      let draggedItem1 = this.deleteItemFromArray($event.dragData.id, this.cat1items);
      if (draggedItem != null) {
        this.cat2items.push(draggedItem);
      } else if (draggedItem1 != null) {
        this.cat2items.push(draggedItem1);
      } else {
        this.cat2items.push(this.deleteItemFromArray($event.dragData.id, this.categoriesData[1].items));
      }
    }
  }

  /**
   *
   * @param id
   * @param array
   * @returns {any}
   */
  deleteItemFromArray(id, array) {
    let i = 0;
    while (i < array.length) {
      if (array[i].id == id) {
        let returnValue = array[i];
        array.splice(i, 1);
        return returnValue;
      }
      i++;
    }
    return null;
  }

  /**
   * array shuffler
   * @param array
   * @returns {any}
   */
  shuffle(array) {
    let currentIndex = array.length, temporaryValue, randomIndex;
    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  }

  /**
   * maybe this one will be removed soon too
   * @param radioData
   * @param pos
   */
  changedRadio(radioData, pos) {
    if (pos != null) {
      if (!radioData[pos].checked) {
        radioData[pos].checked = false;
      } else {
        let i = 0, change = true;
        while (i < radioData.length) {
          radioData[i].checked = false;
          i++;
        }
        if (change) {
          radioData[pos].checked = true;
        }
      }
    }
  }
}
