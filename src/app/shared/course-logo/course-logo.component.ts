import { Component, Input } from '@angular/core';
import { CourseLogo } from '../../model/course-logo';

@Component({
  selector: 'app-course-logo',
  templateUrl: 'course-logo.component.html',
  styleUrls: ['course-logo.component.css']
})
export class CourseLogoComponent {

  /**
   * Course's course-logo object for store picture.
   * If picture is not available then text is used.
   * If text is not available nothing is show.
   */
  @Input()
  public courseLogo: CourseLogo;
}
