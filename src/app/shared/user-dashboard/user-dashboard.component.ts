import { Component, OnInit } from '@angular/core';
import { WeeklySummary } from '../../model/weekly-summary';
import { Router } from '@angular/router';

@Component({
  selector: 'user-dashboard',
  templateUrl: 'user-dashboard.component.html',
  styleUrls: ['user-dashboard.component.css']
})
export class UserDashboardComponent implements OnInit {

  weeklySummary: WeeklySummary = new WeeklySummary();
  learnersNotifications: any[] = [];

  // config for charts
  public barChartOptions: any = {
    defaultFontFamily: '"Chivo"',
    scaleShowVerticalLines: false,
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true,
          maxTicksLimit: 10,
          stepSize: 10,
          fontColor: '#fff',
          fontFamily: '"Chivo"'
        },
        barPercentage: 1.5,
      }],
      xAxes: [{
        categoryPercentage: 1.0,
        barPercentage: 0.6,
        ticks: {
          fontSize: 10,
          fontColor: '#fff',
          fontFamily: '"Chivo"'
        }
      }]
    },
    legend: {
      display: false
    }
  };

  // fake data for charts
  public topChartLabels: string[] = ['CLASS 01', 'CLASS 02', 'CLASS 03', 'CLASS 04'];
  public topChartData: any[] = [{data: [65, 30, 45, 30], label: 'Top 4'}];
  public topChartColors: Array<any> = [{backgroundColor: 'rgba(139, 195, 74, 1)'}];

  public bottomChartLabels: string[] = ['CLASS 01', 'CLASS 02', 'CLASS 03', 'CLASS 04'];
  public bottomChartData: any[] = [{data: [65, 30, 45, 30], label: 'Bottom 4'}];
  public bottomChartColors: Array<any> = [{backgroundColor: 'rgba(255, 88, 77, 1)'}];

  constructor(
    private router: Router
  ) {}

  ngOnInit() {
    this.weeklySummary.activeUsers = 32;
    this.weeklySummary.tutorialsCompleted = 240;
    this.weeklySummary.averageScore = 84;
    this.weeklySummary.coursesCompleted = 7;
    this.learnersNotifications.push({message: 'Learner Alert Message', name: 'Paul Player'});
    this.learnersNotifications.push({message: 'Matt Everest Submitted Evidence', time: 1490874793000});
  }

  redirectToActions() {
    this.router.navigate(['/tutor/admin/actions-required']);
  }
}
