import {Component, Input} from '@angular/core';
import {Router} from '@angular/router';
import {Qualification} from '../../model/learning-portfolio-qualification';
import {PortfolioCourse} from '../../model/portfolioCourse';
import {TutorialService} from '../../services/tutorial.service';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'learning-portfolio',
  templateUrl: './learning-portfolio.component.html',
  styleUrls: ['./learning-portfolio.component.css'],
  animations: [
    trigger('shrinkOut', [
      state('in', style({height: '*'})),
      transition('* => void', [
        style({height: '*'}),
        animate(250, style({height: 0, overflow: 'hidden'}))
      ])
    ]),
    trigger('shrinkIn', [
      state('out', style({height: '*'})),
      transition('void => *', [
        style({height: '0'}),
        animate(250, style({height: '*', overflow: 'hidden'}))
      ])
    ])
  ]
})
export class LearningPortfolioComponent {

  private _courses: PortfolioCourse[];

  public isOpenCourse: boolean[] = [];

  private _qualifications: Qualification[];

  public isOpenQualification: boolean[] = [];

  @Input()
  type: string;

  get courses(): PortfolioCourse[] {
    return this._courses;
  }

  @Input()
  set courses(value: PortfolioCourse[]) {
    this._courses = value;
    this._courses.map(course => this.isOpenCourse.push(false));
  }

  get qualifications(): Qualification[] {
    return this._qualifications;
  }

  @Input()
  set qualifications(value: Qualification[]) {
    this._qualifications = value;
    this._qualifications.map(qualification => this.isOpenQualification.push(false));
  }

  constructor(private router: Router,
              private tutorialService: TutorialService) {
  }

  redirectToCourseOverview(course: PortfolioCourse) {
    if (course.status === 'ACTIVE') {
      this.router.navigateByUrl('student/overview');
    } else {
      this.router.navigateByUrl('student/overview/' + course.enrollmentUuid);
    }
  }

  resetCourse(course: PortfolioCourse) {
    this.tutorialService.resetCourseEnrollment()
      .then(() => {
        course.status = 'ACTIVE';
        this.router.navigate(['/student/tutorial/active']);
      });
  }

  closeCourse(index: number) {
    this.isOpenCourse[index] = false;
  }

  openCouse(index: number) {
    this.isOpenCourse[index] = true;
  }

  closeQualification(index: number) {
    this.isOpenQualification[index] = false;
  }

  openQualification(index: number) {
    this.isOpenQualification[index] = true;
  }

}
