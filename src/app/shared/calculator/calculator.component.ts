/**
 * Created by Trevor on 15/05/2017.
 */
import {Component, Input, ViewChild, ElementRef, Renderer, OnInit, OnDestroy} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'calculator',
  templateUrl: 'calculator.component.html',
  styleUrls: ['calculator.component.scss']
})

export class CalculatorComponent implements OnInit, OnDestroy {

  // variables to allow drag-and-drop of the calculator
  @ViewChild('calccontainer') calcContainer: any;
  private xoff: number = 0;
  private yoff: number = 0;
  private xdiff: number = 0;
  private ydiff: number = 0;
  public topPos: string = '0px';
  public leftPos: string = '0px';
  private dragListen;
  private dropListen;
  private isDragging: boolean = false;
  // variables used for the calculator's computations
  public output: number = 0;
  private currentOperation = this.operation('', '');
  private midOperation: boolean = false;
  private goToNext: boolean = false;

  @Input('close') close = function(): void { return; };

  constructor(private http: Http, private route: ActivatedRoute,
              public elementRef: ElementRef, public renderer: Renderer) {
  }

  ngOnInit() {
    // capture these listeners in variables so they can be removed later
    this.dragListen = this.renderer.listen(this.calcContainer.nativeElement, 'dragstart', (event) => {
      this.dragStart(event);
    });
    this.dropListen = this.renderer.listen(this.calcContainer.nativeElement, 'dragend', (event) => {
      this.drop(event);
    });
    this.centerCalculator();
  }

  ngOnDestroy() {
    // calling these methods effectively removes the listeners
    this.dragListen();
    this.dropListen();
  }

  centerCalculator() {
    const rect = this.calcContainer.nativeElement.getBoundingClientRect();
    this.topPos = (window.innerHeight / 2.0 - rect.height / 2.0) + 'px';
    this.leftPos = (window.innerWidth / 2.0 - rect.width / 2.0) + 'px';
  }

  private pxToNum(pixelString: string): number {
    const index = pixelString.indexOf('px');
    if (index === -1) { return parseFloat(pixelString); }
    return parseFloat(pixelString.substring(0, index));
  }

  dragStart(event) {
    const rect = this.calcContainer.nativeElement.getBoundingClientRect();
    this.xoff = event.clientX;
    this.yoff = event.clientY;
    this.xdiff = event.clientX - this.pxToNum(this.leftPos);
    this.ydiff = this.pxToNum(this.topPos) + rect.height - event.clientY;
  }

  drop(event) {
    this.xoff = event.clientX - this.xoff;
    this.yoff = event.clientY - this.yoff;
    this.leftPos = (this.pxToNum(this.leftPos) + this.xoff + this.xdiff) + 'px';
    this.topPos = (this.pxToNum(this.topPos) + this.yoff - this.ydiff) + 'px';
  }

  clear() {
    this.currentOperation = this.operation('', '');
    this.midOperation = false;
    this.goToNext = false;
    this.output = 0;
  }

  operation(curr, op) {
    return function(next): number {
      return +(global.eval(curr + op + next).toFixed(6));
    };
  }

  outputValue(curr, next) {
    if (next === '.' && curr.toString().indexOf('.') > -1) {
      // Prevent multiple decimal points
      return curr;
    } else if (next === '.') {
      // Keep number if next input is a decimal point
      return curr + next;
    } else if (curr.toString() === '0' || this.goToNext) {
      // Print next input if current output is a 0 or an operation has been canceled
      this.goToNext = false;
      return next;
    } else {
      return curr + next;
    }
  }

  updateValue(text) {
    // cap the length of the number in digits
    if (this.output.toString().length < 11 || (this.output.toString().length === 11 && this.midOperation)) {
      this.output = this.outputValue(this.output, String(text));
    }
  }

  performOp(text) {
    if (text === 'AC') {
      this.clear();
    } else if (text === '+/-') {
      this.output *= -1;
    } else if (text === '%') {
      this.output *= 0.01;
    } else if (text === '=') {
      if (this.midOperation) {
        this.output = this.currentOperation(this.output);
        this.midOperation = false;
        this.goToNext = true;
      }
    } else {
        // else the operation is either *, /, +, or -
        if (text === 'X') {
          text = '*';
        }
        if (!this.midOperation) {
          this.currentOperation = this.operation(this.output, text);
          this.midOperation = true;
        } else {
          this.output = this.currentOperation(this.output);
          this.currentOperation = this.operation(this.output, text);
        }
        this.goToNext = true;
      }
    }
  }

