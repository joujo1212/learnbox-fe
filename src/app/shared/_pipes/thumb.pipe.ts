import { Pipe, PipeTransform } from '@angular/core';
import { Endpoints } from '../../endpoints';

@Pipe({ name: 'thumb' })
export class ThumbPipe implements PipeTransform {
  /**
   * Transform relative URL to absolute URL with postfix _S1|2|3|4
   * @param input
   * @param size Sizes:
   *              2: 250x250
   *              3: 500x500
   *              4: 700x-
   *              5: 1200x720
   * @returns {string} absolute URL of image, e.g. http://domain.com/img/img_1234_S1.jpg
   */
  transform(input: string = '', size: number) {
    const substring1 = input.substring(0, input.indexOf('.'));
    const substring2 = input.substring(input.indexOf('.'));
    return Endpoints.imageBase + substring1 + '_S'
      + size + substring2;
  }
}
