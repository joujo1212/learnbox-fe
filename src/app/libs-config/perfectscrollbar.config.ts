import {PerfectScrollbarConfigInterface} from "angular2-perfect-scrollbar";
export const PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  useBothWheelAxes: true, //wheel scroll horizontally
  suppressScrollY: true //only horizontal scrolling
};
