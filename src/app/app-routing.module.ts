import {Routes, RouterModule} from "@angular/router";
import {IndexComponent} from "./components/index/index.component";
import {NgModule} from "@angular/core";
import {PageNotFoundComponent} from "./components/page-not-found/page-not-found.component";


const appRoutes: Routes = [
  {path: 'index', component: IndexComponent},
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'page-not-found', component: PageNotFoundComponent},
  {path: '**', redirectTo: 'page-not-found', pathMatch: 'full'},
];
@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes
    )
  ],
  exports: [
    RouterModule
  ],
})
export class AppRoutingModule { }
