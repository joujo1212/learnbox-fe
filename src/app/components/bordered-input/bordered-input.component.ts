import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'bordered-input',
  templateUrl: 'bordered-input.component.html',
  styleUrls: ['bordered-input.component.css']
})
export class BorderedInputComponent implements OnInit {
  _placeholder: string = '';
  _inputType = '';

  constructor() {
  }

  @Input()
  get placeholder() {
    return this._placeholder;
  }

  set placeholder(placeholder) {
    this._placeholder = placeholder;
  }

  @Input()
  get inputType() {
    return this._inputType;
  }

  set inputType(inputType) {
    this._inputType = inputType;
  }

  @Output()
  nameChange: EventEmitter<String> = new EventEmitter<String>();

  @Input()
  name: string;

  ngOnInit() {
  }

}
