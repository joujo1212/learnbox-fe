import {Component, OnInit} from '@angular/core';
import {Router}            from '@angular/router';

import {OnBoardService}         from  '../../services/on-board.service';
import {UserService}          from '../../services/user.service';

import {FileUploader}      from 'ng2-file-upload/ng2-file-upload';

const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})


export class IndexComponent implements OnInit {
  public uploader: FileUploader = new FileUploader({url: URL});
  items: Array<any> = [];
  message = '';
  messageStudent = '';
  messageStudents = '';

  constructor(private router: Router,
              private userService: UserService,
              private onBoardService: OnBoardService) {
  }

  ngOnInit() {
    this.userService.getUsers().subscribe(
      response => {
        for (let i = 0; i < response.length; i++) {
          response[i].jsonData = JSON.parse(response[i].jsonData);
        }
        this.items = response;
        // this.router.navigate(['/tutorial/tutorialId']);
      }, (err) => {
        if (err === 'UserNotFinishedRegistration') {
          this.router.navigate(['/finishRegistration']);
        }
        else {
          console.log('Error with user`s data');
        }
      }
    );
  }

  logout(): void {
    this.onBoardService.logout();
  }

  changePswd(oldPswd, newPswd, newPswdAgain): void {
    if (newPswd === newPswdAgain) {
      this.message = 'Password successfully changed!';
      this.onBoardService.changePassword(oldPswd, newPswd).subscribe(response => {
        }, (err) => {
          if (err === 'Error') {
            this.message = 'Problem with changing password!';
          }
        }
      );
    }
    else {
      this.message = 'Passwords are not the same!';
      console.log('A zas to menim');
    }
  }

  inviteStudent(mail): void {
    if(mail){
      this.messageStudent = 'Student successfully invited!';
      this.onBoardService.inviteStudent(mail).subscribe(response => {
        }
        // , (err) => {
        //   this.messageStudent = 'Problem with inviting student!';
        // }
      );
    }else {
      this.messageStudent = 'Please enter a mail address'
    }

  }

  uploadStudents(): void {
    console.log('Fileee: ', this.uploader.queue[0].file);
    this.messageStudents = 'Student successfully invited!';
    this.onBoardService.inviteStudentsByFile(this.uploader.queue[0].file).subscribe(response => {
      }
      // , (err) => {
      //   this.messageStudents = 'Problem with inveting student!';
      // }
    );
  }

}
