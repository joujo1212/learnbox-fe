export type TutorialType =
  "VIDEO" |
  "IMAGE" |
  "TEXT" |
  "QUIZ" |
  "EVIDENCE" |
  "NON_ONLINE"

