import {PortfolioCourse} from './portfolioCourse';
import {Qualification} from './learning-portfolio-qualification';
export class LearnerProgressPortfolio {
  user: {
    uuid: string;
    name: string;
    company: string;
    email: string;
    created: number;
    genderType: string;
    birthDate: string;
    street: string;
    city: string;
    postCode: string;
    phoneNumber: string;
  };
  portfolioCourses: PortfolioCourse[];
  qualifications: Qualification[];

}
