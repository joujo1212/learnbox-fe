import { CourseType } from './course-type.enum';
import { ModuleDetailedView } from './module-detailed-view';

export class CourseSetup {
  uuid: string;
  courseTitle: string;
  courseType: CourseType = CourseType.BASIC;
  courseLogo: string;
  courseLogoText: string;
  courseNumber: number;
  qualificationMappingDocument: string;
  qualificationCode: number;
  userUuid: string; // tutor uuid
  courseDate: number;
  courseUpdated: number;
  draft = false;
  enable = false;
  modules: ModuleDetailedView[] = [];
}
