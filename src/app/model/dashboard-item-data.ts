export class DashboardItemData {
  get text(): string {
    return this._text;
  }

  set text(value: string) {
    this._text = value.toUpperCase();
  }

  icon: string;
  private _text: string;
  redirectLink: string;
}
