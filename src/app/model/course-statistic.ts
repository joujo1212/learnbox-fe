export class CourseStatistic {
  averageScore: number;
  totalProgress: number;
}
