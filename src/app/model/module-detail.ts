import {TutorialInfo} from "./tutorial-info";
import {ModuleTutorialState} from "./module-tutorial-state";
export class ModuleDetail {

  enrollmentUuid: string;
  name: string;
  order: number;
  status: ModuleTutorialState;
  tutorials: TutorialInfo[];
  totalTutorials: number;

  // TODO add fields like from BE

}
