export class PortfolioCourse {
  enrollmentUuid: string;
  courseName: string;
  startTime: number;
  progress: number;
  tutorName: string;
  status: string;
}
