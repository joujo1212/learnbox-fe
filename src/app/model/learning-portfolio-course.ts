export class LearningPortfolioCourse {
  courseName: string;
  startTime: string;
  progress: number;
  tutorName: string;
  status: string;
}
