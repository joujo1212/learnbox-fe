import {TutorialType} from './tutorial-type';
import {ModuleTutorialState} from './module-tutorial-state';

export class TutorialDetail {
  enrollmentUuid: string;
  name: string;
  '@type': TutorialType;
  order: number;
  status: ModuleTutorialState;
  enrollmentModuleUuid: string;
  desc: string;

  // video tutorial
  urlVideo: string;

  // image tutorial
  urlImage: string;

  // text tutorial
  textContent: string;

  // quiz tutorial
  timeLimit: number; // in second
  maxAttempts: number;
  usedAttempts: number;
  totalScore: number;
  earnedScore: number;
  totalQuestions: number;
  correctQuestions: number;
  isFirstAttempt: boolean;
  attempts: number;

  // evidence tutorial
  uploaded: boolean;
  fileName: string;

}
