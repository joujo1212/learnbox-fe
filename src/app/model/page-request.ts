export class PageRequest {

  page: number;
  pageSize: number;

  subPage: number;
  subPageSize: number;

  courseUuid: string;
  enrollmentUuid: string;


}
