export class QualificationData {
  content:string;
  date: number;
  tutor: string;
  grade: string;
}
