import {TutorialType} from "./tutorial-type";
import {ModuleTutorialState} from "./module-tutorial-state";
export class TutorialDetailedView {

  /**
   * Available types of tutorial: VIDEO | EVIDENCE | QUIZ.
   */
  type: TutorialType;

  /**
   * Date and status is in all types of tutorial.
   * Status can be ACTIVE | UNAVAILABLE | PASS | FAIL.
   */
  date: number;
  status: ModuleTutorialState;
  name: string;
  /**
   * VIDEO|EVIDENCE tutorial properties.
   */
  tutor: string;

  /**
   * VIDEO tutorial properties.
   */
  thumbnailUrl: string;
  /**
   * EVIDENCE tutorial properties.
   */
  fileName: string;

  /**
   * QUIZ tutorial properties.
   */
  time: number;
  attempts: number;
  earnScorePercent: number;
  passMarkPercent: number;



}
