export class NonOnlineLearning {
  title: string;
  date: number;
  verified: boolean;
  tutor: string;
  status: boolean;
}
