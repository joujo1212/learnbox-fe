import {TutorialDetailedView} from './tutorial-detailed-view';

/**
 * Used in course overview only
 */
export class ModuleDetailedView {

  /**
   * Name of module.
   */
  name: string;

  /**
   * List of tutorials.
   */
  tutorials: TutorialDetailedView[] = [];
}
