import {LearnerProgress} from './learner-progress';
export class ClassLearnerProgress {
  uuidClassLearnerProgress: string;
  className: string;
  averageProgress: number;
  averageScore: number;
  students: LearnerProgress[];
}
