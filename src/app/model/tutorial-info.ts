import {TutorialType} from "./tutorial-type";
import {ModuleTutorialState} from "./module-tutorial-state";
export class TutorialInfo {
  enrollmentUuid: string;
  name: string;
  tutorialOrder: number;
  enrollmentModuleUuid: string;
  thumbnailUrl: string;
  status: ModuleTutorialState;
  "@type": TutorialType;

  //quiz tutorial
  totalScore: number;
  earnedScore: number;
  isFirstAttempt: boolean;

}
