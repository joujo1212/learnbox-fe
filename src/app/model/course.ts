import {ModuleDetailedView} from "./module-detailed-view";
export class Course {

  enrollmentUuid: string;
  startTime: number;
  endTime: number;
  status: string;
  studentUuid: string;
  courseUuid: string;
  tutorUuid: string;
  doneModules: number;

  uuid: string;
  name: string;
  desc: string;
  modules: ModuleDetailedView[];
  totalModules: number;

}
