export class Qualification {
  qualificationUuid: string;
  qualificationTitle: string;
  verified: boolean;
  tutorUuid: string;
  studentUuid: string;
  tutorName: string;
  grade: string;
  dateReceived: number;
}
