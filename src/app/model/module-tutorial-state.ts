export type ModuleTutorialState =
  "ACTIVE" |
  "UNAVAILABLE" |
  "PASS" |
  "FAIL"

