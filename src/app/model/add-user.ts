/**
 * Used when creating learners, tutors and examiners,
 * learner - whole object
 * tutors & examiners - whole object except for gender and birthday
 */
export class AddUser {
  name: string;
  company: string;
  birthday: string;
  genderType: string;
  street: string;
  city: string;
  zipCode: number;
  email: string;
  phoneNumber: number;
}

