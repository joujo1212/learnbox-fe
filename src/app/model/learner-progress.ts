export class LearnerProgress {
  learnerUuid: string;
  name: string;
  averageProgress: number;
  averageScore: number;
  uuidClassLearnerProgress: string;
}
