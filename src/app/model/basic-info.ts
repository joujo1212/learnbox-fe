export interface BasicInfo {
  company: string;
  email: string;
  name: string;
  roles: string[];
  uuid: string;
}
