export class Tutorial {

  enrollmentUuid: string;
  startTime: number;
  endTime: number;
  status: string;
  enrollmentModuleUuid: string;
  tutorialUuid: string;
  "@type": string;

  uuid: string;
  name: string;
  desc: string;
  order: number;
  moduleUuid: string;
  thumbnailUrl: string;
  url: string;
  disabled: boolean;

}
