export default class CourseBasic {
  uuid: string;
  courseTitle: string;
  courseLogo: string;
  courseLogoText: string;
  enable = false;
  since: string;
}
