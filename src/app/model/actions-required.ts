export class ActionsRequired {
  messages: number;
  name: string;
  className: string;
  marking: string;
  status: boolean;
  issues: string;
  issueSpecial: boolean;
  fileUuid: string;
  fileName: string;
  notificationType: string;

}
