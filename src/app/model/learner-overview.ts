export class LearnerOverview {
  name: string;
  dateAdded: string;
  course: string;
  className: string;
  messages: number;
}
