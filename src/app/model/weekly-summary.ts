export class WeeklySummary {
  activeUsers: number;
  tutorialsCompleted: number;
  averageScore: number;
  coursesCompleted: number;
}
