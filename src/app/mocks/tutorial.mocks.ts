import {Tutorial} from "../model/tutorial";
import {ModuleTutorialState} from "../model/module-tutorial-state";
import {TutorialType} from "../model/tutorial-type";
import {TutorialInfo} from "../model/tutorial-info";
import {TutorialDetail} from "../model/tutorial-detail";
import {TutorialDetailedView} from "../model/tutorial-detailed-view";


export let useMock = false;

export let getTutorials = function(tutorialsCount: number, moduleUuid: string, enrollmentModuleUuid: string): Tutorial[] {
  let tutorials: Tutorial[] = [];

  for (let _i = 0; _i < tutorialsCount; _i++) {
    let tutorial: Tutorial = {
      enrollmentUuid: "enrollmentTutorialUuid" + _i + enrollmentModuleUuid,
      startTime: 0,
      endTime: 100,
      status: "FAIL",
      enrollmentModuleUuid: enrollmentModuleUuid,
      tutorialUuid: "tutorialUuid" + _i + moduleUuid,
      "@type": "VIDEO",
      uuid: "uuid" + _i,
      name: "Tutorial name " + _i,
      desc: "tutorial desc " + _i,
      order: _i,
      moduleUuid: moduleUuid,
      thumbnailUrl: "../../../assets/m-u-l-t-i-p-l-i-c-a-t-i-o-n-c-o-m-p-l-e-t-e.jpg",
      url: null,
      disabled: false
    };

    // console.log(tutorial.tutorialUuid);

    if(!(_i % 2 == 0)) {
      tutorial["@type"] = "QUIZ";
      tutorial.name = _i + " QUIZ"
    }

    if( Math.random()<.5 ){
      tutorial.status = "PASS"
    }
    tutorials.push(tutorial);

    if(_i > 5) {
      tutorial.disabled = true;
    }
  }
  return tutorials;
};

export let isFirst = true;

export let getTutorialInfos = function(tutorialInfoCount: number, enrollmentModuleUuid: string):TutorialInfo[] {
  let tutorialInfos: TutorialInfo[] = [];

  for(let _i = 0; _i < tutorialInfoCount; _i++){
    let tutorialInfo: TutorialInfo = {
      enrollmentUuid: "enrollmentTutorialUuid" + _i + enrollmentModuleUuid,
      name: _i + " VIDEO",
      tutorialOrder: _i,
      enrollmentModuleUuid: enrollmentModuleUuid,
      thumbnailUrl: "../../../assets/m-u-l-t-i-p-l-i-c-a-t-i-o-n-c-o-m-p-l-e-t-e.jpg",
      status: "UNAVAILABLE",
      "@type": "VIDEO",
      totalScore: null,
      earnedScore: null,
      isFirstAttempt: null
    };

    if(_i % 4 == 1) {
      tutorialInfo["@type"] = "QUIZ";
      tutorialInfo.name = _i + " QUIZ";
      tutorialInfo.earnedScore = 0;
      tutorialInfo.totalScore = 20;
      tutorialInfo.isFirstAttempt = true;
    }

    if(_i % 4 == 2) {
      tutorialInfo["@type"] = "IMAGE";
      tutorialInfo.name = _i + " IMAGE";
    }

    if(_i % 4 == 3) {
      tutorialInfo["@type"] = "TEXT";
      tutorialInfo.name = _i + " TEXT";
    }

    if(isFirst){
      tutorialInfo.status = "ACTIVE";
    }

    tutorialInfos.push(tutorialInfo);

  }

  return tutorialInfos;

};

export let getTutorialDetail = function(tutorialInfo: TutorialInfo): TutorialDetail{
  let tutorialDetail: TutorialDetail = new TutorialDetail();
  tutorialDetail.enrollmentUuid = tutorialInfo.enrollmentUuid;
  tutorialDetail.name = tutorialInfo.name;
  tutorialDetail["@type"] = tutorialInfo["@type"];
  tutorialDetail.status = tutorialInfo.status;
  tutorialDetail.enrollmentModuleUuid = tutorialInfo.enrollmentModuleUuid;

  if(tutorialDetail["@type"] === "VIDEO") {
    tutorialDetail.urlVideo = videoURLs[tutorialInfo.tutorialOrder];
  }

  if(tutorialDetail["@type"] === "QUIZ") {
    tutorialDetail.timeLimit = 150;
    tutorialDetail.maxAttempts = 3;
    tutorialDetail.usedAttempts = 0;
    tutorialDetail.totalScore = tutorialInfo.totalScore;
    tutorialDetail.earnedScore = tutorialInfo.earnedScore;
  }

  if(tutorialDetail["@type"] === "TEXT") {
    tutorialDetail.textContent = textRandom.join(" ");
  }

  if(tutorialDetail["@type"] === "IMAGE") {
    tutorialDetail.urlImage = "http://placehold.it/1920x1080";
  }
  return tutorialDetail;
};

export let getTutorialsDetailedView = function(count: number): TutorialDetailedView[] {
  let tutorials: TutorialDetailedView[] = [];
  for( let i = 0; i < count; i++) {
    let tutorial: TutorialDetailedView = new TutorialDetailedView();

    switch (i % 4) {
      case 0:
      case 1: tutorial.type = "VIDEO"; break;
      case 2: tutorial.type = "EVIDENCE"; break;
      case 3: tutorial.type = "QUIZ"; break;
    }

    tutorial.date = 1485023431000; //Fri, 21 Jan 2017 18:30:31 GMT

    switch(tutorial.type){
      case "VIDEO": tutorial.status = "PASS"; break;
      case "EVIDENCE": tutorial.status = "FAIL"; break;
      case "QUIZ": tutorial.status = "FAIL"; break;
    }

    tutorial.name = tutorialDetailedViewTitles[i % 4];
    tutorial.tutor = "M K EVEREST";
    tutorial.thumbnailUrl = "../../../assets/m-u-l-t-i-p-l-i-c-a-t-i-o-n-c-o-m-p-l-e-t-e-dw.jpg";
    tutorial.fileName = "james_robinson_machine_maintenance";
    tutorial.time = null;
    tutorial.attempts = 2;
    tutorial.earnScorePercent = 50;
    tutorial.passMarkPercent = 75;

    tutorials.push(tutorial);
  }
  return tutorials;
};

export let videoURLs = [
  "https://player.vimeo.com/video/193381596",
  "https://player.vimeo.com/video/192417650",
  "https://player.vimeo.com/video/193884721",
  "https://player.vimeo.com/video/193086875",
  "https://player.vimeo.com/video/193419910",
  "https://player.vimeo.com/video/193677663",
  "https://player.vimeo.com/video/192537995",
  "https://player.vimeo.com/video/193729267",
  "https://player.vimeo.com/video/193562415",
  "https://player.vimeo.com/video/192733922",
  "https://player.vimeo.com/video/171794276",
  "https://player.vimeo.com/video/167516546",
  "https://player.vimeo.com/video/67767865",
  "https://player.vimeo.com/video/193381596",
  "https://player.vimeo.com/video/192417650",
  "https://player.vimeo.com/video/193884721",
  "https://player.vimeo.com/video/193086875",
  "https://player.vimeo.com/video/193419910",
  "https://player.vimeo.com/video/193677663",
  "https://player.vimeo.com/video/192537995",
  "https://player.vimeo.com/video/193729267",
  "https://player.vimeo.com/video/193562415",
  "https://player.vimeo.com/video/192733922",
  "https://player.vimeo.com/video/171794276",
  "https://player.vimeo.com/video/167516546",
  "https://player.vimeo.com/video/67767865",
  "https://player.vimeo.com/video/193381596",
  "https://player.vimeo.com/video/192417650",
  "https://player.vimeo.com/video/193884721",
  "https://player.vimeo.com/video/193086875",
  "https://player.vimeo.com/video/193419910",
  "https://player.vimeo.com/video/193677663",
  "https://player.vimeo.com/video/192537995",
  "https://player.vimeo.com/video/193729267",
  "https://player.vimeo.com/video/193562415",
  "https://player.vimeo.com/video/192733922",
  "https://player.vimeo.com/video/171794276",
  "https://player.vimeo.com/video/167516546",
  "https://player.vimeo.com/video/67767865",
  "https://player.vimeo.com/video/193381596",
  "https://player.vimeo.com/video/192417650",
  "https://player.vimeo.com/video/193884721",
  "https://player.vimeo.com/video/193086875",
  "https://player.vimeo.com/video/193419910",
  "https://player.vimeo.com/video/193677663",
  "https://player.vimeo.com/video/192537995",
  "https://player.vimeo.com/video/193729267",
  "https://player.vimeo.com/video/193562415",
  "https://player.vimeo.com/video/192733922",
  "https://player.vimeo.com/video/171794276",
  "https://player.vimeo.com/video/167516546",
  "https://player.vimeo.com/video/67767865",
];

export let textRandom = [
  `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce a libero neque. Aliquam quis neque et urna tincidunt mattis at id urna. Etiam mattis nibh odio, eget accumsan urna rutrum ac. Etiam non purus nec nulla dignissim interdum in eget magna. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse neque arcu, aliquam quis volutpat at, ultricies commodo felis. Duis sit amet sodales nibh, vel aliquet justo. Cras rhoncus, diam et pulvinar sollicitudin, neque nisl varius enim, sit amet molestie enim metus non urna. Donec condimentum metus eget gravida ultricies. Maecenas dapibus ut metus in malesuada.
  Ut ullamcorper orci consequat facilisis tincidunt. Aliquam sit amet erat ligula. Donec a dignissim felis, eu finibus sapien. Sed viverra enim eget posuere vulputate. Vestibulum a lobortis purus. Praesent vel vestibulum velit, at iaculis dolor. Pellentesque sed pretium ipsum. Aliquam eget lorem id nulla venenatis lacinia.
  Interdum et malesuada fames ac ante ipsum primis in faucibus. Quisque nec enim sed lacus ullamcorper facilisis. Donec lobortis nisi vel velit porttitor, id pulvinar lectus rhoncus. Integer scelerisque diam magna, ut laoreet orci egestas pellentesque. Nulla porttitor mauris id sapien blandit, pellentesque faucibus erat tristique. Ut sed laoreet sem, a suscipit turpis. Nulla facilisi. Nunc viverra pretium tellus, nec vulputate mi maximus vel. Etiam a orci erat.
  Nunc consectetur rhoncus orci tincidunt elementum. Nunc commodo vulputate odio id lacinia. Phasellus venenatis metus erat, eget pretium leo eleifend non. Vivamus vestibulum pretium nisl a pulvinar. Cras vel mi ut dui suscipit malesuada. Donec vel odio dolor. Aenean nec dapibus lorem, quis tempus orci. Donec ultricies ultrices ipsum at condimentum. Sed vitae ornare est. Vivamus sed nunc commodo ligula mattis varius et eget mi. Ut sem lacus, consequat vitae interdum id, dapibus rhoncus purus.
  Pellentesque metus nulla, ultricies sit amet venenatis sit amet, pharetra quis nunc. Curabitur condimentum tristique suscipit. Nam non ligula tincidunt, pellentesque arcu et, pretium ipsum. Aenean malesuada tristique sem id consectetur. Donec ultrices dignissim scelerisque. Sed viverra enim et velit aliquam, vel tempor augue commodo. Suspendisse eget metus tempor, semper sapien eget, elementum nulla. Nulla sed nunc molestie, venenatis odio sit amet, congue odio. Fusce viverra pretium elit et vestibulum.`
];

export let tutorialDetailedViewTitles = [
  "Risk assessment",
  "Divide & Conquer",
  "Machine Maintenance",
  "Health & Safely"
];


