import { ActionsRequired } from "../model/actions-required";
import { ClassLearnerProgress } from "../model/class-learner-progress";
import { LearnerProgress } from "../model/learner-progress";
import { LearnerOverview } from "../model/learner-overview";

export let getActionsRequired = function() : ActionsRequired[]{
  let actions: ActionsRequired[] = [];

  for(let i = 0; i < 4; i++){
    let action = new ActionsRequired();
    let studentNames: string[] = ["Alex Humphries", "Paul Player", "Emma Richards", "Frances Perkins", "Sean Rushdon"];
    action.messages = i % 2 == 0 ? 1 : 0;
    action.name = studentNames[i];
    action.className = 'Performing Manufacturing and Operations';
    action.marking = 'james_machine_maintenance.mp4';
    action.status = Math.random() >= .4;

    if(Math.random()<.5){
      action.issues = 'Maths Boxed';
      if(Math.random()<.2)
        action.issueSpecial = true;
    } else
      action.issueSpecial = false;

    actions.push(action);
  }

  return actions;
};

export let getClassLearnerProgress = function(): ClassLearnerProgress[]{
  let progress: ClassLearnerProgress[] = [];
  let studentNames: string[] = ["Alex Humphries", "Paul Player", "Emma Richards", "Frances Perkins", "Sean Rushdon"];

  const classes: string[] = [
    'Performing Manufacturing and Operations',
    'Learn Ethical Hacking From Scratch',
    'Microsoft Excel - Data Analysis with Excel Pivot Tables',
    'Machine Learning A-Z™: Hands-On Python & R In Data Science',
    'Constructing a Complete FileMaker 16 Contact Manager'
  ];

  for(let i = 0; i < 5; i++){
    let item = new ClassLearnerProgress();
    item.uuidClassLearnerProgress = 'uuid' + i;
    item.className = classes[i];
    item.averageProgress = Math.floor(Math.random() * 100);
    item.averageScore = Math.floor(Math.random() * 100);
    item.students = [];

    for(let j = 0; j < 5; j++){
      let student = new LearnerProgress();
      student.name = studentNames[j];
      if (j==1){
        student.averageProgress = 20;
      }else{
        student.averageProgress = Math.floor(Math.random() * 100);
      }
      student.averageScore = Math.floor(Math.random() * 100);
      student.uuidClassLearnerProgress = item.uuidClassLearnerProgress;
      student.learnerUuid = '33b3d3da-3f96-4e1e-b677-cfee6580184d';
      item.students.push(student);
    }

    progress.push(item);
  }

  return progress;
};

export let getLearnerOverview = function() : LearnerOverview[]{
  let overview: LearnerOverview[] = [];
  let studentNames: string[] = ["Alex Humphries", "Paul Player", "Emma Richards", "Frances Perkins", "Sean Rushdon"];

  for(let i = 0; i < 5; i++){
    let item = new LearnerOverview();
    item.name = studentNames[i];
    item.dateAdded = '22/06/2017';
    item.course = i % 2 == 0 ? 'maths' : 'english';
    item.className = 'Performing Manufacturing and Operations';
    item.messages = i % 2 == 0 ? 1 : 0;

    overview.push(item);
  }

  return overview;
};
