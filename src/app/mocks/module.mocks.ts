import {getTutorials, getTutorialInfos, getTutorialsDetailedView} from './tutorial.mocks';
import {ModuleDetail} from '../model/module-detail';
import {ModuleDetailedView} from '../model/module-detailed-view';


export let getModuleDetails = function(moduleDetailsCount: number): ModuleDetail[]{
  const moduleDetails: ModuleDetail[] = [];

  for (let _i = 0; _i < moduleDetailsCount; _i++) {
    const moduleDetail: ModuleDetail = {
      enrollmentUuid: 'enrollmentModuleUuid' + _i,
      name: 'Module Name ' + _i,
      tutorials: getTutorialInfos(26, 'enrollmentModuleUuid' + _i),
      status: 'ACTIVE',
      totalTutorials: 26,
      order: _i,
    };

    moduleDetails.push(moduleDetail);
  }

  return moduleDetails;
};

export const MODULE_DETAILS = getModuleDetails(3);

export let getModulesDetailedView = function(count: number): ModuleDetailedView[] {
  const modules: ModuleDetailedView[] = [];
  for (let i = 0; i < count; i++) {
    const module: ModuleDetailedView = {
      name: 'Divide & Conquer',
      tutorials: getTutorialsDetailedView(4)
    };
    modules.push(module);
  }

  return modules;
};
