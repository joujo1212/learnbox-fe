import {NonOnlineLearning} from "../model/non-online-learning";
export let getNonOnlineLearning = function (count: number): NonOnlineLearning[] {
  let nonOnlineLearningDatas: NonOnlineLearning[] = [];

  nonOnlineLearningDatas.push(
    {
      title: "T14 Certificate of Higher Education in Mathematical Sciences",
      date: 1056281153000,
      verified: true,
      tutor: "M K EVEREST",
      status: true
    }, {
      title: "Diploma of Higher Education in Computing and IT",
      date: 1154777153000,
      verified: null,
      tutor: "N/A",
      status: null
    }, {
      title: "Certificate of Higher Education in Language Studies",
      date: 1154777153000,
      verified: true,
      tutor: "M K EVEREST",
      status: true
    }
  );

  return nonOnlineLearningDatas;
};
